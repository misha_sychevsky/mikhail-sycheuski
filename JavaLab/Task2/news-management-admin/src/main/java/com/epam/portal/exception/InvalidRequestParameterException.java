package com.epam.portal.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Custom exception to handle results of server parameters' validation in ajax query.
 * Return HTTP STATUS CODE 400 that will be handled by ajax on the client's side.
 */
@ResponseStatus(value= HttpStatus.BAD_REQUEST, reason="Invalid request parameters.")
public class InvalidRequestParameterException extends RuntimeException {

    public InvalidRequestParameterException(String message) {

        super(message);
    }

    public InvalidRequestParameterException() {

        super();
    }
}
