package com.epam.portal.utils;

/**
 * This class contains all the constants used by the controllers.
 */
public final class Constants {

    /*
    REQUEST PARAMETERS
     */
    public static final String PARAMETER_NAVIGATION = "navigation";
    public static final String PARAMETER_SELECTED_AUTHOR = "selected_author";
    public static final String PARAMETER_SELECTED_TAGS = "selected_tags";
    public static final String PARAMETER_NEWS_ID = "news_id";
    public static final String PARAMETER_NEWS_INDEX = "news_index";
    public static final String PARAMETER_COMMENT_TEXT = "comment_text";
    public static final String PARAMETER_COMMENT_ID = "comment_id";
    public static final String PARAMETER_NEWS_IDS = "news_ids";
    public static final String PARAMETER_TITLE = "title";
    public static final String PARAMETER_DATE = "date";
    public static final String PARAMETER_SHORT_TEXT = "short_text";
    public static final String PARAMETER_FULL_TEXT = "full_text";
    public static final String PARAMETER_TAG_NAME = "tag_name";
    public static final String PARAMETER_TAG_ID = "tag_id";
    public static final String PARAMETER_AUTHOR_NAME = "author_name";
    public static final String PARAMETER_AUTHOR_ID = "author_id";
    public static final String PARAMETER_TIME_ZONE = "time_zone";
    public static final String PARAMETER_REQUEST_TYPE = "request_type";
    /*
    REQUEST / SESSION ATTRIBUTES
     */
    public static final String ATTRIBUTE_PAGE = "current_page";
    public static final String ATTRIBUTE_LAST_NEWS_PAGE = "lastNewsPage";
    public static final String ATTRIBUTE_NEWS_MESSAGE_PAGE = "newsMessages";
    public static final String ATTRIBUTE_NEWS = "news";
    public static final String ATTRIBUTE_TAGS = "tags";
    public static final String ATTRIBUTE_AUTHORS = "authors";
    public static final String ATTRIBUTE_SEARCH_CRITERIA = "searchCriteria";
    public static final String ATTRIBUTE_NEWS_MESSAGE = "newsMessage";
    public static final String ATTRIBUTE_CURRENT_NEWS_MESSAGE = "currentNewsMessage";
    public static final String ATTRIBUTE_NEWS_INDEX = "newsIndex";
    public static final String ATTRIBUTE_EDIT_FLAG = "editFlag";
    public static final String ATTRIBUTE_ERROR = "error";
    public static final String ATTRIBUTE_CHECKED_AUTHOR = "checkedAuthor";
    public static final String ATTRIBUTE_CHECKED_TAGS = "checkedTags";
    public static final String ATTRIBUTE_ACTIVE_SIDEBAR = "activeSidebar";
    /*
    PAGINATION CONSTANTS
     */
    public static final int ITEMS_COUNT_ON_PAGE = 3;
    public static final int FIRST_PAGE = 1;
    public static final int FIRST_NEWS_MESSAGE_ON_PAGE = 1;
    public static final String NAVIGATION_NEXT = "next";
    public static final String NAVIGATION_PREV = "prev";
    /*
    SIDEBAR ACTIVE FLAGS
     */
    public static final String NEWS_LIST_ACTIVE = "news-list";
    public static final String ADD_NEWS_ACTIVE = "add-news";
    public static final String MANAGE_AUTHORS_ACTIVE = "manage-authors";
    public static final String MANAGE_TAGS_ACTIVE = "manage-tags";
    /*
    VIEWS
     */
    public static final String VIEW_NEWS = "show-news";
    public static final String VIEW_NEWS_PAGE = "show-news-page";
    public static final String VIEW_NEWS_MESSAGE = "show-news-message";
    public static final String VIEW_NEWS_MESSAGE_PAGE = "show-news-message-page";
    public static final String VIEW_SERVER_ERROR = "internal-server-error";
    public static final String VIEW_ADD_NEWS = "add-news";
    public static final String VIEW_EDIT_NEWS = "edit-news";
    public static final String VIEW_MANAGE_TAGS = "manage-tags";
    public static final String VIEW_MANAGE_AUTHORS = "manage-authors";
    /*
    REDIRECTS
    */
    public static final String REDIRECT_MANAGE_NEWS = "redirect:/show-add-news-page";
    public static final String REDIRECT_MANAGE_TAGS = "redirect:/show-tag-management-page";
    public static final String REDIRECT_MANAGE_AUTHORS = "redirect:/show-author-management-page";
    public static final String REDIRECT_EDIT_NEWS = "redirect:/show-edit-news?news_id=";
}
