package com.epam.portal.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.epam.portal.exception.InvalidRequestParameterException;
import com.epam.portal.exception.PageNotFoundException;
import com.epam.portal.exception.ServerHandlingException;
import com.epam.portal.model.Comment;
import com.epam.portal.model.NewsMessage;
import com.epam.portal.model.SearchCriteria;
import com.epam.portal.service.CommentService;
import com.epam.portal.service.NewsMessageService;
import com.epam.portal.service.exception.ServiceException;
import com.epam.portal.utils.validation.Validator;

import javax.servlet.http.HttpSession;
import java.util.Date;

import static com.epam.portal.utils.Constants.*;

/**
 * Controller for handling requests from the single news message page.
 * This controller is responsible for showing single news message pages, posting and deleting comments.
 */
@Controller
public class NewsMessageController {

    public static Logger LOGGER = Logger.getLogger(NewsMessageController.class);

    /**
     * This object represents service for the news message entity.
     */
    @Autowired
    private NewsMessageService newsMessageService;

    /**
     * This object represents service for the comment entity.
     */
    @Autowired
    private CommentService commentService;

    /**
     * Validator object for the validation of request parameters.
     */
    @Autowired
    private Validator validator;

    /**
     * This method handles the ajax request for the next or previous news message page.
     * Setup current page in the session.
     * If there is the last page returns the exception with HTTP STATUS CODE 404.
     * If the server error was occurred returns the exception with HTTP STATUS CODE 500.
     *
     * @param navigation - can be "next" or "prev"
     * @param timeZone - client's time zone
     * @param session
     * @param model
     * @return view with the news message page
     * @throws PageNotFoundException
     * @throws ServerHandlingException
     */
    @RequestMapping("/show-news-message-page")
    public String showNewsMessagePage(@RequestParam(value = PARAMETER_NAVIGATION, required = true) String navigation,
                                      @RequestParam(value = PARAMETER_TIME_ZONE, required = true) String timeZone,
                                      HttpSession session, Model model)throws PageNotFoundException, ServerHandlingException {

        try {
            Long currentNewsMessageId = (Long) session.getAttribute(ATTRIBUTE_CURRENT_NEWS_MESSAGE);
            int currentPage = (int) session.getAttribute(ATTRIBUTE_PAGE);
            int newsMessageIndex = (int) session.getAttribute(ATTRIBUTE_NEWS_INDEX);
            SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute(ATTRIBUTE_SEARCH_CRITERIA);

            NewsMessage newsMessage;
            if (navigation.equals(NAVIGATION_NEXT)) {
                newsMessage = newsMessageService.getNextNewsMessage(searchCriteria, currentNewsMessageId);
                if(newsMessage != null) {
                    if(newsMessageIndex == ITEMS_COUNT_ON_PAGE) {
                        session.setAttribute(ATTRIBUTE_NEWS_INDEX, FIRST_NEWS_MESSAGE_ON_PAGE);
                        session.setAttribute(ATTRIBUTE_PAGE, ++currentPage);
                    }
                    else {
                        session.setAttribute(ATTRIBUTE_NEWS_INDEX, ++newsMessageIndex);
                    }
                    session.setAttribute(ATTRIBUTE_CURRENT_NEWS_MESSAGE, newsMessage.getNews().getNewsId());

                    model.addAttribute(ATTRIBUTE_NEWS_MESSAGE, newsMessage);
                }
                else {
                    throw new PageNotFoundException();
                }
            }

            if (navigation.equals(NAVIGATION_PREV)) {
                newsMessage = newsMessageService.getPrevNewsMessage(searchCriteria, currentNewsMessageId);
                if(newsMessage != null) {
                    if(newsMessageIndex == FIRST_NEWS_MESSAGE_ON_PAGE) {
                        session.setAttribute(ATTRIBUTE_NEWS_INDEX, ITEMS_COUNT_ON_PAGE);
                        session.setAttribute(ATTRIBUTE_PAGE, --currentPage);
                    }
                    else {
                        session.setAttribute(ATTRIBUTE_NEWS_INDEX, --newsMessageIndex);
                    }
                    session.setAttribute(ATTRIBUTE_CURRENT_NEWS_MESSAGE, newsMessage.getNews().getNewsId());

                    model.addAttribute(ATTRIBUTE_NEWS_MESSAGE, newsMessage);
                }
                else {
                    throw new PageNotFoundException();
                }
            }
        }
        catch (ServiceException e) {
            LOGGER.error(e);
            throw new ServerHandlingException();
        }

        return VIEW_NEWS_MESSAGE_PAGE;
    }

    /**
     * This method handles the ajax request for comment posting.
     * Check if the comment is valid and add it to the database..
     * In otherwise returns the exception with HTTP STATUS CODE 400.
     * If the server error was occurred returns the exception with HTTP STATUS CODE 500.
     *
     * @param commentText
     * @param session
     * @return - HTTP STATUS CODE OK and id of the added comment
     * @throws ServerHandlingException
     * @throws InvalidRequestParameterException
     */
    @RequestMapping("/post-comment")
    public ResponseEntity<String> postComment(@RequestParam(value = PARAMETER_COMMENT_TEXT, required = true) String commentText,
                            HttpSession session) throws ServerHandlingException, InvalidRequestParameterException {

        Long commentId = 1L;
        try {
            Long currentNewsMessageId = (Long) session.getAttribute(ATTRIBUTE_CURRENT_NEWS_MESSAGE);

            if(validator.isCommentValid(commentText)) {
                Comment comment = new Comment();
                comment.setNewsId(currentNewsMessageId);
                comment.setCreationDate(new Date());
                comment.setCommentText(commentText);

                commentId = commentService.addComment(comment);
            }
            else {
                throw new InvalidRequestParameterException();
            }
        }
        catch (ServiceException e) {
            LOGGER.error(e);
            throw new ServerHandlingException();
        }

        return new ResponseEntity<>(commentId.toString(), HttpStatus.OK);
    }

    /**
     * This method handles the ajax request for comment deleting.
     * If the server error was occurred returns the exception with HTTP STATUS CODE 500.
     *
     * @param commentId
     * @return - HTTP STATUS OK
     * @throws ServerHandlingException
     */
    @RequestMapping("/delete-comment")
    public ResponseEntity postComment(@RequestParam(value = PARAMETER_COMMENT_ID, required = true) Long commentId)
            throws ServerHandlingException {

        try {
            Comment comment = new Comment();
            comment.setCommentId(commentId);
            commentService.deleteComment(comment);
        }
        catch (ServiceException e) {
            LOGGER.error(e);
            throw new ServerHandlingException();
        }

        return new ResponseEntity(HttpStatus.OK);
    }
}
