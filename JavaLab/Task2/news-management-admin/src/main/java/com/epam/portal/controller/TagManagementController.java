package com.epam.portal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.epam.portal.model.Tag;
import com.epam.portal.service.TagService;
import com.epam.portal.service.exception.ServiceException;
import com.epam.portal.utils.validation.Validator;

import static com.epam.portal.utils.Constants.*;

/**
 * Controller for handling requests from the tag management page.
 * Provides handlers for adding, editing and deleting tags operations.
 */
@Controller
public class TagManagementController {

    /**
     * This object represents service for the tag entity.
     */
    @Autowired
    private TagService tagService;

    /**
     * Validator object for the validation of request parameters.
     */
    @Autowired
    private Validator validator;

    /**
     * Handles tag adding.
     * Creates a new tag entity to add it to the database using service layer.
     *
     * @param tagName
     * @param redirect
     * @return
     * @throws ServiceException
     */
    @RequestMapping(value = "/add-tag", method = RequestMethod.POST)
    public String addTag(@RequestParam(value = PARAMETER_TAG_NAME, required = true) String tagName,
                         RedirectAttributes redirect) throws ServiceException {

        Tag tag = new Tag();
        tag.setName(tagName);

        if(validator.isTagValid(tag)) {
            redirect.addFlashAttribute(ATTRIBUTE_ERROR, false);
            tagService.addTag(tag);
        }
        else {
            redirect.addFlashAttribute(ATTRIBUTE_ERROR, true);
        }

        return REDIRECT_MANAGE_TAGS;
    }

    /**
     * Handles tag editing.
     * Parses tag id and ceates a new tag entity to update it in the database using service layer.
     * In otherwise add redirect flash attribute to show error message on the page.
     *
     * @param tagName
     * @param tagIdString
     * @param redirect
     * @return
     * @throws ServiceException
     */
    @RequestMapping(value = "/edit-tag", method = RequestMethod.POST)
    public String editTag(@RequestParam(value = PARAMETER_TAG_NAME, required = true) String tagName,
                          @RequestParam(value = PARAMETER_TAG_ID, required = true) String tagIdString,
                          RedirectAttributes redirect) throws ServiceException {

        Long tagId = validator.parseId(tagIdString);

        if(tagId != null) {
            Tag tag = new Tag();
            tag.setName(tagName);
            tag.setTagId(tagId);

            if(validator.isTagValid(tag)) {
                redirect.addFlashAttribute(ATTRIBUTE_ERROR, false);
                tagService.updateTag(tag);
            }
            else {
                redirect.addFlashAttribute(ATTRIBUTE_ERROR, true);
            }
        }

        return REDIRECT_MANAGE_TAGS;
    }

    /**
     * Handles tag deleting.
     * Parses tag id and delete tag with such id if it is valid.
     * In otherwise add redirect flash attribute to show error message on the page.
     *
     * @param tagIdString
     * @param redirect
     * @return
     * @throws ServiceException
     */
    @RequestMapping(value = "/delete-tag", method = RequestMethod.POST)
    public String deleteTag(@RequestParam(value = PARAMETER_TAG_ID, required = true) String tagIdString,
                            RedirectAttributes redirect) throws ServiceException {

        Long tagId = validator.parseId(tagIdString);
        if(tagId != null) {
            Tag tag = new Tag();
            tag.setTagId(tagId);
            tagService.deleteTag(tag);

            redirect.addFlashAttribute(ATTRIBUTE_ERROR, false);
        }

        return REDIRECT_MANAGE_TAGS;
    }
}
