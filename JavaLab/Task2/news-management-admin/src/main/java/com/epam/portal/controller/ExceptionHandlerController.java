package com.epam.portal.controller;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import com.epam.portal.service.exception.ServiceException;

import static com.epam.portal.utils.Constants.VIEW_SERVER_ERROR;

/**
 * The main exception handler of the application.
 * Return the error page, if <code>ServiceException</code> has been occurred in some of controllers.
 */
@ControllerAdvice
public class ExceptionHandlerController {

    public static Logger LOGGER = Logger.getLogger(ExceptionHandlerController.class);

    /**
     * Writes the exception to the LOG and returns the error page.
     *
     * @param e
     * @return error view tile
     */
    @ExceptionHandler(ServiceException.class)
    public String serviceExceptionHandler(ServiceException e) {

        LOGGER.error(e);
        return VIEW_SERVER_ERROR;
    }
}
