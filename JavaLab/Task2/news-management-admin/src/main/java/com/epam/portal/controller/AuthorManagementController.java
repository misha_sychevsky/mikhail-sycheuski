package com.epam.portal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.epam.portal.model.Author;
import com.epam.portal.service.AuthorService;
import com.epam.portal.service.exception.ServiceException;
import com.epam.portal.utils.validation.Validator;

import static com.epam.portal.utils.Constants.*;

/**
 * Controller for handling requests from the author management page.
 * Provides handlers for adding, editing and setting author expired operations.
 */
@Controller
public class AuthorManagementController {

    /**
     * Validator object for the validation of request parameters.
     */
    @Autowired
    private Validator validator;

    /**
     * This object represents service for the author entity.
     */
    @Autowired
    private AuthorService authorService;

    /**
     * Handles author adding.
     * Creates and adds a new author entity to the database using service layer.
     *
     * @param authorName
     * @param redirect
     * @return
     * @throws ServiceException
     */
    @RequestMapping(value = "/add-author", method = RequestMethod.POST)
    public String addTag(@RequestParam(value = PARAMETER_AUTHOR_NAME, required = true) String authorName,
                         RedirectAttributes redirect) throws ServiceException {

        Author author = new Author();
        author.setName(authorName);

        if(validator.isAuthorValid(author)) {
            redirect.addFlashAttribute(ATTRIBUTE_ERROR, false);
            authorService.addAuthor(author);
        }
        else {
            redirect.addFlashAttribute(ATTRIBUTE_ERROR, true);
        }

        return REDIRECT_MANAGE_AUTHORS;
    }

    /**
     * Handles author editing.
     * Parses author id and creates an author entity to update it in the database using service layer.
     * In otherwise add redirect flash attribute to show error message on the page.
     *
     * @param authorName
     * @param authorIdString
     * @param redirect
     * @return
     * @throws ServiceException
     */
    @RequestMapping(value = "/edit-author", method = RequestMethod.POST)
    public String editTag(@RequestParam(value = PARAMETER_AUTHOR_NAME, required = true) String authorName,
                          @RequestParam(value = PARAMETER_AUTHOR_ID, required = true) String authorIdString,
                          RedirectAttributes redirect) throws ServiceException {

        Long authorId = validator.parseId(authorIdString);

        if(authorId != null) {
            Author author = new Author();
            author.setAuthorId(authorId);
            author.setName(authorName);

            if(validator.isAuthorValid(author)) {
                redirect.addFlashAttribute(ATTRIBUTE_ERROR, false);
                authorService.editAuthor(author);
            }
            else {
                redirect.addFlashAttribute(ATTRIBUTE_ERROR, true);
            }
        }

        return REDIRECT_MANAGE_AUTHORS;
    }

    /**
     * Method to set author expire.
     * Parses author id and set him expired in the database.
     * In otherwise add redirect flash attribute to show error message on the page.
     * Expired authors are not displayed in the add/edit author page and add/edit news page.
     *
     * @param authorIdString
     * @param redirect
     * @return
     * @throws ServiceException
     */
    @RequestMapping(value = "/expire-author", method = RequestMethod.POST)
    public String deleteTag(@RequestParam(value = PARAMETER_AUTHOR_ID, required = true) String authorIdString,
                            RedirectAttributes redirect) throws ServiceException {

        Long authorId = validator.parseId(authorIdString);
        if(authorId != null) {
            authorService.setAuthorExpired(authorId);

            redirect.addFlashAttribute(ATTRIBUTE_ERROR, false);
        }

        return REDIRECT_MANAGE_AUTHORS;
    }
}
