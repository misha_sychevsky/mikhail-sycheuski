package com.epam.portal.utils.validation;

import org.springframework.stereotype.Component;
import com.epam.portal.model.Author;
import com.epam.portal.model.News;
import com.epam.portal.model.NewsMessage;
import com.epam.portal.model.Tag;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Custom validation class.
 * Provides some methods to validate request parameters on the server side.
 */
@Component
public class Validator {

    /**
     * Method for comment validation. Check comment text.
     *
     * @param commentText
     * @return
     */
    public boolean isCommentValid(String commentText) {

        return commentText != null && !commentText.isEmpty() && commentText.length() < 100;
    }

    /**
     * Method for author entity validation. Check author's name.
     *
     * @param author
     * @return <code>true</code> if the author name is not empty and there are no more then 30 characters,
     * otherwise return <code>false</code>
     */
    public boolean isAuthorValid(Author author) {

        return !author.getName().isEmpty() && !(author.getName().length() > 30);
    }

    /**
     * Method for tag entity validation. Check tag name.
     *
     * @param tag <code>true</code> if the tag name is not empty and there are no more then 30 characters,
     * otherwise return <code>false</code>
     * @return
     */
    public boolean isTagValid(Tag tag) {

        return !tag.getName().isEmpty() && !(tag.getName().length() > 30);
    }

    /**
     * Method for news message entity validation. Check author id, and all of the news fields.
     *
     * @param newsMessage
     * @return <code>boolean</code> result of validation
     */
    public boolean isNewsMessageValid(NewsMessage newsMessage) {

        boolean valid = true;

        if(newsMessage.getAuthor().getAuthorId() == null) {
            valid = false;
        }

        News news = newsMessage.getNews();
        if(news.getTitle().isEmpty() || news.getTitle().length() > 30) {
            valid = false;
        }
        if(news.getModificationDate() == null) {
            valid = false;
        }
        if(news.getShortText().isEmpty() || news.getShortText().length() > 100) {
            valid = false;
        }
        if(news.getFullText().isEmpty() || news.getFullText().length() > 2000) {
            valid = false;
        }

        return valid;
    }

    /**
     * Parse the date in format dd#MM#yyyy where # - is one of delimiters '/', '.', '-'.
     * If the string doesn't represent date in such format - return <code>null</code>.
     *
     * @param dateString - string with date
     * @return
     */
    public Date parseDate(String dateString) {

        SimpleDateFormat formatter = new SimpleDateFormat("dd MM yyyy");
        dateString = dateString.replaceAll("[\\/.-]", " ");

        Date date;
        try {
            date = formatter.parse(dateString);
        }
        catch (ParseException e) {
            date = null;
        }

        return date;
    }

    /**
     * This method parse tag ids from string to the <code>Long</code> and put it into the <code>List</code>.
     *
     * @param idsString
     * @return <code>List</code> with tag ids in <code>Long</code> type
     */
    public List<Long> parseIds(String idsString) {

        List<Long> ids = new ArrayList<>();
        List<String> idsList = Arrays.asList(idsString.replaceAll("id", "").split("-"));
        for(String idString : idsList) {
            Long id = parseId(idString);
            if(id != null) {
                ids.add(id);
            }
        }
        return ids;
    }

    /**
     * This method parse tag idString from string to <code>Long</code>.
     *
     * @param longString
     * @return tag idString <code>Long</code> type
     */
    public Long parseId(String longString) {

        try {
            return Long.parseLong(longString);
        }
        catch (NumberFormatException e) {
            return null;
        }
    }
}
