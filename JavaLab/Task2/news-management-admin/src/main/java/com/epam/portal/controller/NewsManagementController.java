package com.epam.portal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.epam.portal.model.Author;
import com.epam.portal.model.News;
import com.epam.portal.model.NewsMessage;
import com.epam.portal.model.Tag;
import com.epam.portal.service.NewsMessageService;
import com.epam.portal.service.exception.ServiceException;
import com.epam.portal.utils.validation.Validator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.epam.portal.utils.Constants.*;

/**
 * Controller for handling requests from the news management pages (add/edit).
 * Provides handlers for adding, editing and setting author expired operations.
 */
@Controller
public class NewsManagementController {

    /**
     * This object represents service for the news message entity.
     */
    @Autowired
    private NewsMessageService newsMessageService;

    /**
     * Validator object for the validation of request parameters.
     */
    @Autowired
    private Validator validator;

    /**
     * Handles news message adding.
     * Creates and adds a new news message entity to the database using service layer if all parameters are valid.
     * In otherwise add redirect flash attribute to show error message on the page.
     *
     * @param authorIdString - string with author id for parsing
     * @param tagIdsString - string with tag ids for parsing
     * @param title
     * @param date
     * @param shortText
     * @param fullText
     * @param redirect
     * @return
     * @throws ServiceException
     */
    @RequestMapping(value = "/add-news", method = RequestMethod.POST)
    public String addNews(@RequestParam(value = PARAMETER_SELECTED_AUTHOR, required = true) String authorIdString,
                          @RequestParam(value = PARAMETER_SELECTED_TAGS, required = false) String tagIdsString,
                          @RequestParam(value = PARAMETER_TITLE, required = true) String title,
                          @RequestParam(value = PARAMETER_DATE, required = true) String date,
                          @RequestParam(value = PARAMETER_SHORT_TEXT, required = true) String shortText,
                          @RequestParam(value = PARAMETER_FULL_TEXT, required = true) String fullText,
                          RedirectAttributes redirect) throws ServiceException {

        News news = new News();
        news.setTitle(title);
        news.setCreationDate(validator.parseDate(date));
        news.setModificationDate(validator.parseDate(date));
        news.setShortText(shortText);
        news.setFullText(fullText);
        Author author = new Author();
        author.setAuthorId(validator.parseId(authorIdString));
        List<Tag> tags = new ArrayList<>();
        for(Long id : validator.parseIds(tagIdsString)) {
            Tag tag = new Tag();
            tag.setTagId(id);
            tags.add(tag);
        }

        NewsMessage newsMessage = new NewsMessage();
        newsMessage.setAuthor(author);
        newsMessage.setTags(tags);
        newsMessage.setNews(news);

        if(validator.isNewsMessageValid(newsMessage)) {
            redirect.addFlashAttribute(ATTRIBUTE_ERROR, false);
            newsMessageService.saveNewsMessage(newsMessage);
        }
        else {
            redirect.addFlashAttribute(ATTRIBUTE_ERROR, true);
        }

        return REDIRECT_MANAGE_NEWS;
    }

    /**
     * Handles news message editing.
     * Creates a new news message entity to update it in the database using service layer if all parameters are valid.
     * In otherwise add redirect flash attribute to show error message on the page.
     *
     * @param authorIdString - string with author id for parsing
     * @param tagIdsString - string with tag ids for parsing
     * @param title
     * @param newsId
     * @param shortText
     * @param fullText
     * @param redirect
     * @return
     * @throws ServiceException
     */
    @RequestMapping(value = "/edit-news", method = RequestMethod.POST)
    public String editNews(@RequestParam(value = PARAMETER_SELECTED_AUTHOR, required = true) String authorIdString,
                           @RequestParam(value = PARAMETER_SELECTED_TAGS, required = false) String tagIdsString,
                           @RequestParam(value = PARAMETER_TITLE, required = true) String title,
                           @RequestParam(value = PARAMETER_NEWS_ID, required = true) Long newsId,
                           @RequestParam(value = PARAMETER_SHORT_TEXT, required = true) String shortText,
                           @RequestParam(value = PARAMETER_FULL_TEXT, required = true) String fullText,
                           RedirectAttributes redirect) throws ServiceException {

        News news = new News();
        news.setNewsId(newsId);
        news.setTitle(title);
        news.setModificationDate(new Date());
        news.setShortText(shortText);
        news.setFullText(fullText);
        Author author = new Author();
        author.setAuthorId(validator.parseId(authorIdString));
        List<Tag> tags = new ArrayList<>();
        for(Long id : validator.parseIds(tagIdsString)) {
            Tag tag = new Tag();
            tag.setTagId(id);
            tags.add(tag);
        }

        NewsMessage newsMessage = new NewsMessage();
        newsMessage.setAuthor(author);
        newsMessage.setTags(tags);
        newsMessage.setNews(news);

        if(validator.isNewsMessageValid(newsMessage)) {
            redirect.addFlashAttribute(ATTRIBUTE_ERROR, false);
            newsMessageService.updateNewsMessage(newsMessage);
        }
        else {
            redirect.addFlashAttribute(ATTRIBUTE_ERROR, true);
        }

        return REDIRECT_EDIT_NEWS + newsId;
    }
}
