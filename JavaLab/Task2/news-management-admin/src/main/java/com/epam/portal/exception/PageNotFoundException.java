package com.epam.portal.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Custom exception to handle empty server response  in ajax query.
 * Return HTTP STATUS CODE 404 that will be handled by ajax on the client's side.
 */
@ResponseStatus(value= HttpStatus.NOT_FOUND, reason="No page.")
public class PageNotFoundException extends RuntimeException {

    public PageNotFoundException(String message) {

        super(message);
    }

    public PageNotFoundException() {

        super();
    }
}
