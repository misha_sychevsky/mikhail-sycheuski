package com.epam.portal.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Security controller.
 * Returns the authorization page.
 */
@Controller
public class AuthorizationSecurityController {

    /**
     * Returns the login page.
     *
     * @return name of the view tile for login page
     */
    @RequestMapping("/login")
    public String login() {

        return "login";
    }
}
