package com.epam.portal.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.epam.portal.exception.InvalidRequestParameterException;
import com.epam.portal.exception.PageNotFoundException;
import com.epam.portal.exception.ServerHandlingException;
import com.epam.portal.model.*;
import com.epam.portal.service.AuthorService;
import com.epam.portal.service.NewsMessageService;
import com.epam.portal.service.NewsService;
import com.epam.portal.service.TagService;
import com.epam.portal.service.exception.ServiceException;
import com.epam.portal.utils.validation.Validator;

import javax.servlet.http.HttpSession;
import java.util.List;

import static com.epam.portal.utils.Constants.*;

/**
 * Controller for handling requests from the main page (news list).
 * This controller is responsible for showing news pages, showing single news , showing news editing page,
 * filtering of the news, resetting it and deleting of news.
 */
@Controller
public class NewsListController {

    public static Logger LOGGER = Logger.getLogger(NewsListController.class);

    /**
     * This object represents service for the news message entity.
     */
    @Autowired
    private NewsMessageService newsMessageService;

    /**
     * This object represents service for the author entity.
     */
    @Autowired
    private AuthorService authorService;

    /**
     * This object represents service for the news entity.
     */
    @Autowired
    private NewsService newsService;

    /**
     * This object represents service for the tag entity.
     */
    @Autowired
    private TagService tagService;

    /**
     * Validator object for the validation of request parameters.
     */
    @Autowired
    private Validator validator;

    /**
     * Shows the first news page after the authorization or shows the certain news page when user exits news message page.
     *
     * @param model
     * @param session
     * @return name of the view tile
     * @throws ServiceException
     */
    @RequestMapping(value = "/show-news", method = RequestMethod.GET)
    public String showNews(Model model, HttpSession session) throws ServiceException{

        SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute(ATTRIBUTE_SEARCH_CRITERIA);
        Integer page = (Integer) session.getAttribute(ATTRIBUTE_PAGE);

        if(page == null) {
            page = FIRST_PAGE;
        }
        int start = (page - 1) * ITEMS_COUNT_ON_PAGE + 1;

        makeNewsModel(start, searchCriteria, model);

        session.setAttribute(ATTRIBUTE_PAGE, page);

        return VIEW_NEWS;
    }

    /**
     * This method handles the ajax request for the next or previous news page.
     * Setup current page in the session.
     * If there is the last page returns the exception with HTTP STATUS CODE 404.
     * If the server error was occurred returns the exception with HTTP STATUS CODE 500.
     *
     * @param navigation - can be "next" or "prev"
     * @param model
     * @param session
     * @return view with next/previous news items
     * @throws ServerHandlingException
     * @throws PageNotFoundException
     */
    @RequestMapping(value = "/show-news-page", method = RequestMethod.GET)
    public String showNewsPage(@RequestParam(value = PARAMETER_NAVIGATION, required = true) String navigation,
                                Model model, HttpSession session) throws PageNotFoundException, ServerHandlingException{

        try {
            SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute(ATTRIBUTE_SEARCH_CRITERIA);

            if(navigation.equals(NAVIGATION_NEXT)) {
                List<NewsMessage> newsMessages = null;
                if(session.getAttribute(ATTRIBUTE_LAST_NEWS_PAGE) != true) {
                    int page = (int) session.getAttribute(ATTRIBUTE_PAGE);
                    int start = page == FIRST_PAGE ? ITEMS_COUNT_ON_PAGE + 1 : page * ITEMS_COUNT_ON_PAGE + 1;
                    int count = start + ITEMS_COUNT_ON_PAGE;

                    newsMessages = newsMessageService.getNewsMessagePage(searchCriteria, start, count);
                    session.setAttribute(ATTRIBUTE_PAGE, ++page);

                    if (newsMessages.size() == 0) {
                        session.setAttribute(ATTRIBUTE_PAGE, --page);
                        session.setAttribute(ATTRIBUTE_LAST_NEWS_PAGE, true);

                        throw new PageNotFoundException();
                    }
                }
                else {
                    throw new PageNotFoundException();
                }

                model.addAttribute(ATTRIBUTE_NEWS_MESSAGE_PAGE, newsMessages);
            }
            if(navigation.equals(NAVIGATION_PREV)) {
                int page = (int) session.getAttribute(ATTRIBUTE_PAGE);
                if(page != FIRST_PAGE) {
                    int start = (page == 2 ? 1 : (page - 2) * ITEMS_COUNT_ON_PAGE + 1);
                    int count = start + ITEMS_COUNT_ON_PAGE;

                    List<NewsMessage> newsMessages = newsMessageService.getNewsMessagePage(searchCriteria, start, count);
                    session.setAttribute(ATTRIBUTE_PAGE, --page);
                    model.addAttribute(ATTRIBUTE_NEWS_MESSAGE_PAGE, newsMessages);

                    if(session.getAttribute(ATTRIBUTE_LAST_NEWS_PAGE) == true)
                    {
                        session.setAttribute(ATTRIBUTE_LAST_NEWS_PAGE, false);
                    }
                }
                else {
                    throw new PageNotFoundException();
                }
            }
        }
        catch (ServiceException e) {
            LOGGER.error(e);
            throw new ServerHandlingException();
        }

        return VIEW_NEWS_PAGE;
    }

    /**
     * This method do filtering of news by the search criteria parameters from user.
     *
     * @param authorIdString - string with author id for parsing
     * @param tagIdsString - string with tag ids for parsing
     * @param model
     * @param session
     * @return name of the view tile
     * @throws ServiceException
     */
    @RequestMapping(value = "/filter-news", method = RequestMethod.GET)
    public String filterNews(@RequestParam(value = PARAMETER_SELECTED_AUTHOR, required = true) String authorIdString,
                             @RequestParam(value = PARAMETER_SELECTED_TAGS, required = true) String tagIdsString,
                             Model model, HttpSession session) throws ServiceException {

        Long authorId = validator.parseId(authorIdString);
        List<Long> tagIds = validator.parseIds(tagIdsString);

        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setAuthorId(authorId);
        searchCriteria.setTagIds(tagIds);

        int start = FIRST_PAGE;

        makeNewsModel(start, searchCriteria, model);

        model.addAttribute(ATTRIBUTE_CHECKED_AUTHOR, authorIdString);
        model.addAttribute(ATTRIBUTE_CHECKED_TAGS, tagIdsString);

        session.setAttribute(ATTRIBUTE_SEARCH_CRITERIA, searchCriteria);
        session.setAttribute(ATTRIBUTE_PAGE, FIRST_PAGE);
        session.setAttribute(ATTRIBUTE_LAST_NEWS_PAGE, false);

        return VIEW_NEWS;
    }

    /**
     * This method reset the search criteria and return the first news page.
     *
     * @param model
     * @param session
     * @return name of the view tile
     * @throws ServiceException
     */
    @RequestMapping(value = "/reset-filter", method = RequestMethod.GET)
    public String resetFilter(Model model, HttpSession session) throws ServiceException {

        session.removeAttribute(ATTRIBUTE_SEARCH_CRITERIA);
        makeNewsModel(FIRST_PAGE, null, model);
        session.setAttribute(ATTRIBUTE_PAGE, FIRST_PAGE);

        return VIEW_NEWS;
    }

    /**
     * This method shows the single news message with its comments.
     *
     * @param newsId - id of the new to show
     * @param newsIndex - news index on the page (in range 1-newsCountOnPage)
     * @param timeZone - client's time zone
     * @param session
     * @param model
     * @return name of the view tile
     * @throws ServiceException
     */
    @RequestMapping(value = "/show-news-message", method = RequestMethod.GET)
    public String showNewsMessage(@RequestParam(value = PARAMETER_NEWS_ID, required = true) Long newsId,
                                  @RequestParam(value = PARAMETER_NEWS_INDEX, required = true) int newsIndex,
                                  @RequestParam(value = PARAMETER_TIME_ZONE, required = true) String timeZone,
                                  HttpSession session, Model model) throws ServiceException {

        NewsMessage newsMessage = newsMessageService.viewSingleNewsMessage(newsId);

        session.setAttribute(ATTRIBUTE_NEWS_INDEX, newsIndex);
        session.setAttribute(ATTRIBUTE_CURRENT_NEWS_MESSAGE, newsId);

        model.addAttribute(ATTRIBUTE_NEWS_MESSAGE, newsMessage);

        return VIEW_NEWS_MESSAGE;
    }

    /**
     * This method return the page for editing of selected news item.
     *
     * @param newsId - id of the news to edit
     * @param model
     * @return name of the view tile
     * @throws ServiceException
     */
    @RequestMapping(value = "/show-edit-news", method = RequestMethod.GET)
    public String editNews(@RequestParam(value = PARAMETER_NEWS_ID, required = true) Long newsId,
                           Model model) throws ServiceException {

        NewsMessage newsMessage = newsMessageService.viewSingleNewsMessage(newsId);
        List<Tag> tags = tagService.viewTheListOfTags();
        List<Author> authors = authorService.viewTheListOfAuthors();

        model.addAttribute(ATTRIBUTE_EDIT_FLAG, true);
        model.addAttribute(ATTRIBUTE_TAGS, tags);
        model.addAttribute(ATTRIBUTE_AUTHORS, authors);
        model.addAttribute(ATTRIBUTE_NEWS, newsMessage.getNews());
        model.addAttribute(ATTRIBUTE_CHECKED_AUTHOR, newsMessage.getAuthor());
        model.addAttribute(ATTRIBUTE_CHECKED_TAGS, newsMessage.getTags());

        return VIEW_EDIT_NEWS;
    }

    /**
     * This method deletes all of the selected news.
     * Return page with offset news or exception with HTTP STATUS CODE 400 if there were invalid parameters.
     * If the server error was occurred returns the exception with HTTP STATUS CODE 500.
     *
     * @param newsIds - string with news ids witch must be deleted.
     * @param model
     * @param session
     * @return the view with offset news
     * @throws ServerHandlingException
     * @throws InvalidRequestParameterException
     */
    @RequestMapping(value = "/delete-news", method = RequestMethod.POST)
    public String deleteNews(@RequestParam(value = PARAMETER_NEWS_IDS, required = true) String newsIds,
                             Model model, HttpSession session) throws InvalidRequestParameterException, ServerHandlingException{

        try {
            List<Long> newsIdsList = validator.parseIds(newsIds);
            if(!newsIdsList.isEmpty()) {
                for(Long newsId : newsIdsList) {
                    News news = new News();
                    news.setNewsId(newsId);
                    newsService.deleteNews(news);
                }

                int start = ((Integer) session.getAttribute(ATTRIBUTE_PAGE) - 1) * ITEMS_COUNT_ON_PAGE + 1;
                SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute(ATTRIBUTE_SEARCH_CRITERIA);
                makeNewsModel(start, searchCriteria, model);
            }
            else {
                throw new InvalidRequestParameterException();
            }
        }
        catch (ServiceException e) {
            throw new ServerHandlingException();
        }

        return VIEW_NEWS_PAGE;
    }

    /**
     * The the main mature information and add it into the model.
     *
     * @param start - start news id on this page
     * @param searchCriteria
     * @param model
     * @throws ServiceException
     */
    private void makeNewsModel(int start, SearchCriteria searchCriteria, Model model) throws ServiceException{

        int count = start + ITEMS_COUNT_ON_PAGE;

        List<NewsMessage> newsMessages = newsMessageService.getNewsMessagePage(searchCriteria, start, count);
        List<Tag> tags = tagService.viewTheListOfTags();
        List<Author> authors = authorService.viewTheListOfAuthors();

        model.addAttribute(ATTRIBUTE_TAGS, tags);
        model.addAttribute(ATTRIBUTE_AUTHORS, authors);
        model.addAttribute(ATTRIBUTE_NEWS_MESSAGE_PAGE, newsMessages);
        model.addAttribute(ATTRIBUTE_ACTIVE_SIDEBAR, NEWS_LIST_ACTIVE);
    }
}
