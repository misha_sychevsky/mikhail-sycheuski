package com.epam.portal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.epam.portal.model.Author;
import com.epam.portal.model.NewsMessage;
import com.epam.portal.model.Tag;
import com.epam.portal.service.AuthorService;
import com.epam.portal.service.NewsMessageService;
import com.epam.portal.service.TagService;
import com.epam.portal.service.exception.ServiceException;

import javax.servlet.http.HttpSession;
import java.util.List;

import static com.epam.portal.utils.Constants.*;

/**
 * Controller for handling requests from the menu section on each page.
 * Can forward to the "news list" page,  "add news" page, "tag management" page and "author management" page.
 */
@Controller
public class MenuController {

    /**
     * This object represents service for the news message entity.
     */
    @Autowired
    private NewsMessageService newsMessageService;

    /**
     * This object represents service for the author entity.
     */
    @Autowired
    private AuthorService authorService;

    /**
     * This object represents service for the tag entity.
     */
    @Autowired
    private TagService tagService;

    /**
     * This method returns the page with list of all news.
     *
     * @param model
     * @param session
     * @return name of the view tile
     * @throws ServiceException
     */
    @RequestMapping(value = "/show-news-list-page", method = RequestMethod.GET)
    public String showNewsList(Model model, HttpSession session) throws ServiceException{

        int start = FIRST_PAGE;
        int count = start + ITEMS_COUNT_ON_PAGE;

        List<NewsMessage> newsMessages = newsMessageService.getNewsMessagePage(null, start, count);
        List<Tag> tags = tagService.viewTheListOfTags();
        List<Author> authors = authorService.viewTheListOfAuthors();

        model.addAttribute(ATTRIBUTE_TAGS, tags);
        model.addAttribute(ATTRIBUTE_AUTHORS, authors);
        model.addAttribute(ATTRIBUTE_NEWS_MESSAGE_PAGE, newsMessages);
        model.addAttribute(ATTRIBUTE_ACTIVE_SIDEBAR, NEWS_LIST_ACTIVE);

        session.setAttribute(ATTRIBUTE_PAGE, FIRST_PAGE);
        session.setAttribute(ATTRIBUTE_LAST_NEWS_PAGE, false);

        return VIEW_NEWS;
    }

    /**
     * This method returns the news adding page.
     *
     * @param model
     * @return name of the view tile
     * @throws ServiceException
     */
    @RequestMapping(value = "/show-add-news-page", method = RequestMethod.GET)
    public String showAddNewsPage(Model model) throws ServiceException{

        List<Tag> tags = tagService.viewTheListOfTags();
        List<Author> authors = authorService.viewTheListOfAuthors();

        model.addAttribute(ATTRIBUTE_EDIT_FLAG, false);
        model.addAttribute(ATTRIBUTE_TAGS, tags);
        model.addAttribute(ATTRIBUTE_AUTHORS, authors);
        model.addAttribute(ATTRIBUTE_ACTIVE_SIDEBAR, ADD_NEWS_ACTIVE);

        return VIEW_ADD_NEWS;
    }

    /**
     * This method returns the page for tag managing (add/edit).
     *
     * @param model
     * @return name of the view tile
     * @throws ServiceException
     */
    @RequestMapping(value = "/show-tag-management-page", method = RequestMethod.GET)
    public String showTagManagementPage(Model model) throws ServiceException{

        List<Tag> tags = tagService.viewTheListOfTags();

        model.addAttribute(ATTRIBUTE_TAGS, tags);
        model.addAttribute(ATTRIBUTE_ACTIVE_SIDEBAR, MANAGE_TAGS_ACTIVE);

        return VIEW_MANAGE_TAGS;
    }

    /**
     * This method returns the page for author managing (add/edit).
     *
     * @param model
     * @return name of the view tile
     * @throws ServiceException
     */
    @RequestMapping(value = "/show-author-management-page", method = RequestMethod.GET)
    public String showAuthorManagementPage(Model model) throws ServiceException{

        List<Author> authors = authorService.viewTheListOfAuthors();

        model.addAttribute(ATTRIBUTE_AUTHORS, authors);
        model.addAttribute(ATTRIBUTE_ACTIVE_SIDEBAR, MANAGE_AUTHORS_ACTIVE);

        return VIEW_MANAGE_AUTHORS;
    }
}
