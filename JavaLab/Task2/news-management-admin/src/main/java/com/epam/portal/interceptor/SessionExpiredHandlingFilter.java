package com.epam.portal.interceptor;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.portal.utils.Constants.PARAMETER_REQUEST_TYPE;

/**
 * This filter is necessary for checking if the session is expired fo ajax queries.
 * It runs before the spring security filter chain.
 */
public class SessionExpiredHandlingFilter implements Filter {


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        String requestType = request.getParameter(PARAMETER_REQUEST_TYPE);

        boolean flag = true;
        if(request.getSession(false) == null) {
            if(requestType != null && requestType.equals("ajax")) {
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                flag = false;
            }
        }
        if(flag) {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {

    }
}
