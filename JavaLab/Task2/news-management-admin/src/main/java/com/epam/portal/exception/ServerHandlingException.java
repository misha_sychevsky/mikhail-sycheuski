package com.epam.portal.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Custom exception to handle <code>ServiceException</code> in ajax query.
 * Return HTTP STATUS CODE 500 that will be handled by ajax on the client's side.
 */
@ResponseStatus(value= HttpStatus.INTERNAL_SERVER_ERROR, reason="Can't handle request.")
public class ServerHandlingException extends RuntimeException {

    public ServerHandlingException(String message) {

        super(message);
    }

    public ServerHandlingException() {

        super();
    }
}
