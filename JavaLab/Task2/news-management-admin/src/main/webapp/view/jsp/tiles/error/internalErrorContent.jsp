<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="news-container">
  <p class="error-text">
    <spring:message code="view.error.server.code"/>
  </p>
  <p class="error-text error-text-message">
    <spring:message code="view.error.server.text"/>
    <br>
    <spring:message code="view.error.sorry"/>
  </p>
</div>
