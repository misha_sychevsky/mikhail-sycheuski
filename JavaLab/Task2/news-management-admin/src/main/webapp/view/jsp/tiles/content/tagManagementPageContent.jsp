<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:if test="${error != null}">
    <div class="result-div" id="result-container">
        <div class="result-window">
            <c:if test="${error == false}">
                <img class="success-img" src="view/img/success.png">
                <p class="failed-text ">
                    <spring:message code="result.window.success"/>
                </p>
                <p class="failed-text">
                    <spring:message code="result.window.success.operation.message"/>
                </p>
            </c:if>
            <c:if test="${error == true}">
                <img class="success-img" src="view/img/error.png">
                <p class="failed-text ">
                    <spring:message code="result.window.error"/>
                </p>
                <p class="failed-text">
                    <spring:message code="result.window.error.message"/>
                </p>
            </c:if>
        </div>
    </div>
</c:if>
<div class="edit-container">
    <form id="add-tag-form" action="add-tag" method="post">
        <div class="tag-box add-tag">
            <label class="text-label" for="tag"><spring:message code="view.manage.tag.add"/></label>
            <input type="text" name="tag_name" id="tag" class="text-input tag-name"/>
            <input type="submit" name="save" value="<spring:message code="view.button.save"/>" class="button save-tag-button">
            <span class="hidden invalid-field"><spring:message code="manage.tag.name.error"/></span>
        </div>
    </form>
    <form id="delete-tag-form" action="delete-tag" method="post">
        <input type="hidden" name="tag_id" value="">
    </form>
    <form id="edit-tag-form"  action="edit-tag" method="post">
        <input type="hidden" name="tag_name" value="">
        <input type="hidden" name="tag_id" value="">
    </form>
    <div class="edit-tags">
        <c:forEach items="${tags}" var="tag">
            <div class="tag-box">
                <input type="hidden" name="tag_id" value="${tag.tagId}">
                <label class="text-label dark-label" for="tag-${tag.tagId}"><spring:message code="view.name.tag"/> </label>
                <input type="text" name="tag_name" id="tag-${tag.tagId}" class="text-input tag-name" value="${tag.name}" disabled>
                <div class="button edit-button first-edit-button edit"><spring:message code="view.news.href.edit"/></div>
                <div class="tag-actions hidden">
                    <input type="button" name="update" value="<spring:message code="view.button.update"/>" class="button edit-button first-edit-button">
                    <input type="button" name="delete" value="<spring:message code="view.button.delete"/>" class="button edit-button">
                    <div class="button edit-button cancel"><spring:message code="view.button.cancel"/></div>
                </div>
                <span class="hidden invalid-field"><spring:message code="manage.tag.name.error"/></span>
            </div>
        </c:forEach>
    </div>
</div>
