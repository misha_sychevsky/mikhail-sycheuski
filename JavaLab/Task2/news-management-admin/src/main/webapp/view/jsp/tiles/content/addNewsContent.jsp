<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib  uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="add-news-container">
  <form id="news-form" action="add-news" method="post">
    <div class="criteria-container clear-background">
        <div id="dd" class="wrapper-dropdown-2 margin-left fix-wrapper-dropdown-width" tabindex="1">
          <p class="select-title">
            <spring:message code="view.dropdown.author"/>
          </p>
          <input type="hidden" name="selected_author" value="">
          <ul class="dropdown-select">
            <c:forEach items="${authors}" var="author">
              <c:if test="${author.expired == null}">
                <li id="${author.authorId}">
                  <label class="radio-label"><c:out value="${author.name}"/></label>
                </li>
              </c:if>
            </c:forEach>
          </ul>
        </div>
        <dl class="dropdown fix-width-dropdown">
          <dt>
            <a style="width: 188px" href="#">
            <span class="hida">
              <spring:message code="view.dropdown.tags"/>
            </span>
            </a>
          </dt>
          <input type="hidden" name="selected_tags" value=""/>
          <dd>
            <div class="mutliSelect">
              <ul style="width: 218px">
                <div class="checkbox">
                  <c:forEach items="${tags}" var="tag">
                    <li>
                      <input id="id${tag.tagId}" type="checkbox" value="${tag.name}" />
                      <label class="checkbox-label" for="id${tag.tagId}"><c:out value="${tag.name}"/></label>
                    </li>
                  </c:forEach>
                </div>
              </ul>
            </div>
          </dd>
        </dl>
    </div>
    <c:if test="${error != null}">
      <div class="result-div" id="result-container">
        <div class="result-window">
          <c:if test="${error == false}">
            <img class="success-img" src="view/img/success.png">
            <p class="failed-text ">
              <spring:message code="result.window.success"/>
            </p>
            <p class="failed-text">
              <spring:message code="result.window.success.news.add.message"/>
            </p>
          </c:if>
          <c:if test="${error == true}">
            <img class="success-img" src="view/img/error.png">
            <p class="failed-text ">
              <spring:message code="result.window.error"/>
            </p>
            <p class="failed-text">
              <spring:message code="result.window.error.message"/>
            </p>
          </c:if>
        </div>
      </div>
    </c:if>
    <div class="field-row">
      <label class="text-label" for="title"><spring:message code="view.manage.news.title"/></label>
      <input type="text" id="title" class="text-input" name="title" value="">
      <span class="hidden invalid-field"><spring:message code="manage.news.error.message.title"/></span>
    </div>
    <div class="field-row">
      <label class="text-label" for="date"><spring:message code="view.manage.news.date"/></label>
      <input type="text" id="date" class="text-input" name="date" placeholder="dd#mm#yyyy" value="">
      <span class="hidden invalid-field"><spring:message code="manage.news.error.message.date"/></span>
    </div>
    <div class="field-row">
      <label class="text-label" for="brief"><spring:message code="view.manage.news.brief"/></label>
      <textarea id="brief" class="" name="short_text"><c:out value=""/></textarea>
      <span class="hidden invalid-field"><spring:message code="manage.news.error.message.brief"/></span>
    </div>
    <div class="field-row">
      <label class="text-label" for="content"><spring:message code="view.manage.news.content"/></label>
      <textarea id="content" class="" name="full_text"><c:out value=""/></textarea>
      <span class="hidden invalid-field"><spring:message code="manage.news.error.message.content"/></span>
    </div>
    <div class="clear-background align-right">
      <input type="submit" name="Save" value="<spring:message code="view.button.save"/>" class="button save-button">
    </div>
  </form>
</div>
