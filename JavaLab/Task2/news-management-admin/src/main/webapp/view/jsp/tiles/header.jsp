<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<div class="header-container">
  <img src="view/img/newspaper.png" class="news-img">
  <a class="page-title" href="reset-filter">
    <tiles:getAsString name="title"/>
  </a>
  <div class="right-header-part">
    <sec:authorize access="hasRole('ADMIN')">
      <form action="logout" method="post" class="logout">
        <input id="logout" type="submit" value="">
      </form>
    </sec:authorize>
    <sec:authorize access="permitAll">
      <div class="localization">
        <input type="hidden" name="locale" value="${pageContext.response.locale}">
        <c:set var="parameters" value="${requestScope['javax.servlet.forward.query_string']}"/>
        <c:if test="${fn:contains(requestScope['javax.servlet.forward.query_string'], 'locale=ru')}">
          <c:set var="parameters" value="${fn:replace(requestScope['javax.servlet.forward.query_string'], 'locale=ru', '')}"/>
        </c:if>
        <c:if test="${fn:contains(requestScope['javax.servlet.forward.query_string'], 'locale=en')}">
          <c:set var="parameters" value="${fn:replace(requestScope['javax.servlet.forward.query_string'], 'locale=en', '')}"/>
        </c:if>
        <c:set var="parameters" value="${fn:substring(parameters, 0, fn:length(parameters))}"/>
        <c:choose>
          <c:when test="${fn:length(parameters) == 0}">
            <c:set var="parameters" value="/news-management-admin${requestScope['javax.servlet.forward.servlet_path']}?"/>
          </c:when>
          <c:otherwise>
            <c:set var="parameters" value="/news-management-admin${requestScope['javax.servlet.forward.servlet_path']}?${parameters}&"/>
          </c:otherwise>
        </c:choose>
        <c:choose>
          <c:when test="${pageContext.response.locale == 'en'}">
            <a id="ru" href="${parameters}locale=ru" class="locale-button active">RU</a>
            <a id="en" href="${parameters}locale=en" class="locale-button locale-selected">EN</a>
          </c:when>
          <c:when test="${pageContext.response.locale == 'ru'}">
            <a id="ru" href="${parameters}locale=ru" class="locale-button locale-selected">RU</a>
            <a id="en" href="${parameters}locale=en" class="locale-button active">EN</a>
          </c:when>
        </c:choose>
      </div>
    </sec:authorize>
  </div>
</div>
