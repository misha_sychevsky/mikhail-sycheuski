<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="authorization-container">
  <form action="j_spring_security_check" method="post" class="authorization">
    <div class="authorization-row">
      <label for="login"><spring:message code="view.authorization.login"/></label>
      <input id="login" class="auth-field" type="text" name="j_username" value="" autofocus>
    </div>
    <div class="authorization-row">
      <label for="password"><spring:message code="view.authorization.password"/></label>
      <input id="password" class="auth-field" type="password" name='j_password'>
    </div>
    <c:if test="${param.error != null}">
      <span class="invalid-field"><spring:message code="authorization.message.error"/></span>
    </c:if>
    <input class="button" type="submit" name="Login" value="<spring:message code="view.button.log.in"/>">
    <input type="hidden" class="auth-field" name="${_csrf.parameterName}" value="${_csrf.token}" />
  </form>
</div>
