<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<fmt:setLocale value="${pageContext.response.locale}"/>
<c:if test="${!newsMessages.isEmpty()}">
    <c:forEach items="${newsMessages}" var="newsMessage" varStatus="loop">
        <div class="news-item">
          <div class="news-info-top">
            <a onclick="hrefNewsMessageWithTimeZone(${newsMessage.news.newsId}, ${loop.count})" class="text-title news-title href">
              <c:out value="${newsMessage.news.title}"/>
            </a>
            <p class="text-title news-author"><c:out value="${newsMessage.author.name}"/></p>
            <p class="date-field">
                <fmt:formatDate pattern="dd MMM yyyy" value="${newsMessage.news.modificationDate}" />
            </p>
          </div>
          <p class="news-content">
            <c:out value="${newsMessage.news.shortText}"/>
          </p>
          <div class="news-info-bottom">
            <label class="delete-checkbox"><input type="checkbox" name="deleteNews" value="id${newsMessage.news.newsId}"><span></span></label>
            <a class="text-title news-signature view-href edit-href" href="show-edit-news?news_id=${newsMessage.news.newsId}">
              <spring:message code="view.news.href.edit"/>
            </a>
            <p class="text-title news-signature">
              <spring:message code="view.news.comments"/> ${newsMessage.comments.size()}
            </p>
            <p class="text-title news-signature">
              <spring:message code="view.news.tags"/>
              <c:if test="${newsMessage.tags.size() != 0}">
                <c:forEach items="${newsMessage.tags}" var="tag">
                  <c:out value="#${tag.name} "/>
                </c:forEach>
              </c:if>
              <c:if test="${newsMessage.tags.size() == 0}">
                <spring:message code="view.news.no.tags"/>
              </c:if>
            </p>
          </div>
        </div>
    </c:forEach>
    <input id="delete-news" class="button delete-button" type="button" name="Delete" value="">
</c:if>
<c:if test="${newsMessages.isEmpty()}">
  <div class="not-found-container">
    <img src="view/img/not_found.png" class="not_found_img">
    <p class="not_found">
      <spring:message code="view.news.not.found"/>.
    </p>
  </div>
</c:if>

