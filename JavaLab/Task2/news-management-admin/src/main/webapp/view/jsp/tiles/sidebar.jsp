<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="sidebar">
  <ul>
    <li>
      <c:if test="${activeSidebar == 'news-list'}">
        <a class="sidebar-href sidebar-active" href="show-news-list-page"><spring:message code="view.sidebar.news.list"/></a>
      </c:if>
      <c:if test="${activeSidebar != 'news-list'}">
        <a class="sidebar-href" href="show-news-list-page"><spring:message code="view.sidebar.news.list"/></a>
      </c:if>
    </li>
    <li>
      <c:if test="${activeSidebar == 'add-news'}">
        <a class="sidebar-href sidebar-active" href="show-add-news-page"><spring:message code="view.sidebar.add.news"/></a>
      </c:if>
      <c:if test="${activeSidebar != 'add-news'}">
        <a class="sidebar-href" href="show-add-news-page"><spring:message code="view.sidebar.add.news"/></a>
      </c:if>
    </li>
    <li>
      <c:if test="${activeSidebar == 'manage-authors'}">
        <a class="sidebar-href sidebar-active" href="show-author-management-page"><spring:message code="view.sidebar.manage.author"/></a>
      </c:if>
      <c:if test="${activeSidebar != 'manage-authors'}">
        <a class="sidebar-href" href="show-author-management-page"><spring:message code="view.sidebar.manage.author"/></a>
      </c:if>
    </li>
    <li>
      <c:if test="${activeSidebar == 'manage-tags'}">
        <a class="sidebar-href sidebar-active" href="show-tag-management-page"><spring:message code="view.sidebar.manage.tag"/></a>
      </c:if>
      <c:if test="${activeSidebar != 'manage-tags'}">
        <a class="sidebar-href" href="show-tag-management-page"><spring:message code="view.sidebar.manage.tag"/></a>
      </c:if>
    </li>
  </ul>
</div>
