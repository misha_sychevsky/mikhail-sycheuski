<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="news-container">
  <div class="criteria-container">
    <form id="criteria-form" action="filter-news" method="get">
      <dl class="dropdown">
        <dt>
          <a href="#">
            <span class="hida">
              <spring:message code="view.dropdown.tags"/>
            </span>
          </a>
        </dt>
        <input type="hidden" name="selected_tags" value="${checkedTags}"/>
        <dd>
          <div class="mutliSelect">
            <ul>
              <div class="checkbox">
                <c:forEach items="${tags}" var="tag">
                  <li>
                    <input id="id${tag.tagId}" type="checkbox" value="${tag.name}" />
                    <label class="checkbox-label" for="id${tag.tagId}"><c:out value="${tag.name}"/></label>
                  </li>
                </c:forEach>
              </div>
            </ul>
          </div>
        </dd>
      </dl>
      <div id="dd" class="wrapper-dropdown-2 margin-left" tabindex="1">
        <p class="select-title">
          <spring:message code="view.dropdown.author"/>
        </p>
        <input type="hidden" name="selected_author" value="${checkedAuthor}">
        <ul class="dropdown-select">
          <c:forEach items="${authors}" var="author">
            <li id="${author.authorId}">
              <label class="radio-label"><c:out value="${author.name}"/></label>
            </li>
          </c:forEach>
        </ul>
      </div>
      <div id="filter-button" class="button active">
        <spring:message code="view.button.filter"/>
      </div>
      <c:if test="${sessionScope.searchCriteria != null}">
        <a id="reset-button" class="button active" href="reset-filter">
          <spring:message code="view.button.reset"/>
        </a>
      </c:if>
      <c:if test="${sessionScope.searchCriteria == null}">
        <div id="reset-button" class="button">
          <spring:message code="view.button.reset"/>
        </div>
      </c:if>
    </form>
  </div>
  <div id="news-items-container">
        <jsp:include page="../print/printNewsPage.jsp"/>
  </div>
  <c:if test="${!newsMessages.isEmpty()}">
    <div class="navigation-container">
      <form id="pagination">
        <input id="button-prev" type="button" class="button active" value="<">
        <p class="page-number">${sessionScope.current_page}</p>
        <input id="button-next" type="button" class="button active" value=">">
      </form>
    </div>
  </c:if>
</div>
