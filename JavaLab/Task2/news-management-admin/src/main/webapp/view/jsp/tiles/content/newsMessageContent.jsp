<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="news-container">
  <div class="news-border-block">
    <a class="back-button" href="show-news"></a>
    <div class="news-block">
      <div id="news-message">
        <jsp:include page="../print/printNewsMessage.jsp"/>
      </div>
      <div class="comment-container comment-control">
        <textarea id="comment-area" class="row comment-area" name="comment_text" placeholder="<spring:message code="view.news.message.comment.placeholder"/>"></textarea>
        <p class="alert-message hidden">
          <spring:message code="view.news.message.comment.error"/>
        </p>
        <div id="post-comment" class="button submit-comment-button active row">
          <spring:message code="view.news.message.comment.post.button"/>
        </div>
      </div>
    </div>
    <div class="navigation-form">
      <div id="prev" class="button navigation-button-left active">&lt;</div>
      <div id="next" class="button navigation-button-right active">&gt;</div>
    </div>
  </div>
</div>
