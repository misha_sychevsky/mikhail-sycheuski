<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib  uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="add-news-container">
  <form id="news-form" action="edit-news" method="post">
    <div class="criteria-container clear-background">
      <div id="dd" class="wrapper-dropdown-2 margin-left fix-wrapper-dropdown-width" tabindex="1">
        <p class="select-title">
          <spring:message code="view.dropdown.author"/>
          <c:out value="${checkedAuthor.name}"/>
        </p>
        <input type="hidden" name="selected_author" value="${checkedAuthor.authorId}">
        <ul class="dropdown-select">
          <c:forEach items="${authors}" var="author">
            <c:if test="${author.expired == null}">
              <li id="${author.authorId}">
                <label class="radio-label"><c:out value="${author.name}"/></label>
              </li>
            </c:if>
          </c:forEach>
        </ul>
      </div>
      <dl class="dropdown fix-width-dropdown">
        <dt>
          <a style="width: 188px" href="#">
            <span class="hida">
              <spring:message code="view.dropdown.tags"/>
              <c:forEach items="${checkedTags}" var="tag">
                <span class="select-span-style" title="${tag.name}"><c:out value="#${tag.name}"/></span>
              </c:forEach>
            </span>
          </a>
        </dt>
        <c:set var="last" value=""/>
        <c:forEach items="${checkedTags}" var="tag">
          <c:set var="tagIdsString" value="${last}${'-id'.concat(tag.tagId)}"/>
          <c:set var="last" value="${tagIdsString}" />
        </c:forEach>
        <input type="hidden" name="selected_tags" value="${tagIdsString}"/>
        <dd>
          <div class="mutliSelect">
            <ul style="width: 218px">
              <div class="checkbox">
                <c:forEach items="${tags}" var="tag">
                  <li>
                    <c:choose>
                      <c:when test="${fn:contains(tagIdsString, 'id'.concat(tag.tagId))}">
                        <input id="id${tag.tagId}" type="checkbox" value="${tag.name}" checked />
                      </c:when>
                      <c:otherwise>
                        <input id="id${tag.tagId}" type="checkbox" value="${tag.name}" />
                      </c:otherwise>
                    </c:choose>
                    <label class="checkbox-label" for="id${tag.tagId}"><c:out value="${tag.name}"/></label>
                  </li>
                </c:forEach>
              </div>
            </ul>
          </div>
        </dd>
      </dl>
    </div>
    <c:if test="${error != null}">
      <div class="result-div" id="result-container">
        <div class="result-window">
          <c:if test="${error == false}">
            <img class="success-img" src="view/img/success.png">
            <p class="failed-text ">
              <spring:message code="result.window.success"/>
            </p>
            <p class="failed-text">
              <spring:message code="result.window.success.news.edit.message"/>
            </p>
          </c:if>
          <c:if test="${error == true}">
            <img class="success-img" src="view/img/error.png">
            <p class="failed-text ">
              <spring:message code="result.window.error"/>
            </p>
            <p class="failed-text">
              <spring:message code="result.window.error.message"/>
            </p>
          </c:if>
        </div>
      </div>
    </c:if>
    <input type="hidden" name="news_id" value="${news.newsId}">
    <div class="field-row">
      <label class="text-label" for="title"><spring:message code="view.manage.news.title"/></label>
      <input type="text" id="title" class="text-input" name="title" value="${news.title}">
      <span class="hidden invalid-field"><spring:message code="manage.news.error.message.title"/></span>
    </div>
    <div class="field-row">
      <c:if test="${pageContext.response.locale == 'en'}">
        <fmt:formatDate timeZone="America/New_York" pattern="dd-MM-YYYY" value="${news.modificationDate}" var="modificationDate" />
      </c:if>
      <c:if test="${pageContext.response.locale == 'ru'}">
        <fmt:formatDate timeZone="UTC+3" pattern="dd-MM-YYYY" value="${news.modificationDate}" var="modificationDate" />
      </c:if>
      <label class="text-label" for="date"><spring:message code="view.manage.news.date"/></label>
      <input type="text" id="date" class="text-input" name="date" value="${modificationDate}" disabled>
      <span class="hidden invalid-field"><spring:message code="manage.news.error.message.date"/></span>
    </div>
    <div class="field-row">
      <label class="text-label" for="brief"><spring:message code="view.manage.news.brief"/></label>
      <textarea id="brief" class="" name="short_text"><c:out value="${news.shortText}"/></textarea>
      <span class="hidden invalid-field"><spring:message code="manage.news.error.message.brief"/></span>
    </div>
    <div class="field-row">
      <label class="text-label" for="content"><spring:message code="view.manage.news.content"/></label>
      <textarea id="content" class="" name="full_text"><c:out value="${news.fullText}"/></textarea>
      <span class="hidden invalid-field"><spring:message code="manage.news.error.message.content"/></span>
    </div>
    <div class="clear-background align-right">
      <input type="submit" name="Save" value="<spring:message code="view.button.save"/>" class="button save-button">
    </div>
  </form>
</div>