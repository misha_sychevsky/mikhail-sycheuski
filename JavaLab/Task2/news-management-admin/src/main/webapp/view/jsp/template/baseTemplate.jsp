<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<html>
  <head>
    <title><tiles:getAsString name="title"/></title>
    <link rel="icon" href="${pageContext.request.contextPath}/view/img/logo.ico">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/view/css/style.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment-with-locales.min.js"></script>
    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
  </head>
  <body>
    <div class="page-container">
      <tiles:insertAttribute name="header"/>
      <tiles:insertAttribute name="sidebar"/>
      <tiles:insertAttribute name="content"/>
      <tiles:insertAttribute name="footer"/>
    </div>
    <script src="${pageContext.request.contextPath}/view/js/script.js"></script>
  </body>
</html>
