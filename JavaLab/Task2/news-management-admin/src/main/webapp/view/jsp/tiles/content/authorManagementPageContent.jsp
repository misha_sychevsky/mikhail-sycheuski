<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:if test="${error != null}">
  <div class="result-div" id="result-container">
    <div class="result-window">
      <c:if test="${error == false}">
        <img class="success-img" src="view/img/success.png">
        <p class="failed-text ">
          <spring:message code="result.window.success"/>
        </p>
        <p class="failed-text">
          <spring:message code="result.window.success.operation.message"/>
        </p>
      </c:if>
      <c:if test="${error == true}">
        <img class="success-img" src="view/img/error.png">
        <p class="failed-text ">
          <spring:message code="result.window.error"/>
        </p>
        <p class="failed-text">
          <spring:message code="result.window.error.message"/>
        </p>
      </c:if>
    </div>
  </div>
</c:if>
<div class="edit-container">
  <form id="add-author-form" action="add-author" method="post">
    <div class="tag-box add-tag">
      <label class="text-label" for="author"><spring:message code="view.manage.author.add"/></label>
      <input type="text" name="author_name" id="author" class="text-input tag-name">
      <input type="submit" name="save" value="<spring:message code="view.button.save"/>" class="button save-tag-button">
      <span class="hidden invalid-field"><spring:message code="manage.author.name.error"/></span>
    </div>
  </form>
  <form id="expire-author-form" action="expire-author" method="post">
    <input type="hidden" name="author_id" value="">
  </form>
  <form id="edit-author-form"  action="edit-author" method="post">
    <input type="hidden" name="author_name" value="">
    <input type="hidden" name="author_id" value="">
  </form>
  <div class="edit-tags">
    <c:forEach items="${authors}" var="author">
      <c:if test="${author.expired == null}">
        <div class="tag-box">
          <input type="hidden" name="author_id" value="${author.authorId}">
          <label class="text-label dark-label" for="tag-${author.authorId}"><spring:message code="view.dropdown.author"/> </label>
          <input type="text" name="author_name" id="tag-${author.authorId}" class="text-input tag-name" value="${author.name}" disabled>
          <div class="button edit-button first-edit-button edit"><spring:message code="view.news.href.edit"/></div>
          <div class="tag-actions hidden">
            <input type="submit" name="update-author" value="<spring:message code="view.button.update"/>" class="button edit-button first-edit-button">
            <input type="submit" name="expire-author" value="<spring:message code="view.button.expire"/>" class="button edit-button">
            <div class="button edit-button cancel"><spring:message code="view.button.cancel"/></div>
          </div>
          <span class="hidden invalid-field"><spring:message code="manage.author.name.error"/></span>
        </div>
      </c:if>
    </c:forEach>
  </div>
</div>
