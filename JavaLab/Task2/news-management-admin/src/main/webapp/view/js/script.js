// Tag select handling -------------------------------------------------------------------------------------------------
var tags = [];

$(".dropdown dt a").on('click', function () {

    $(".dropdown dd ul").slideToggle(50);
});

$(".dropdown dd ul li a").on('click', function () {

    $(".dropdown dd ul").hide();
});

$(document).bind('click', function (e) {

    var $clicked = $(e.target);
    if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
});

$('.mutliSelect input[type="checkbox"]').on('click', function () {

    var title = $(this).val();

    if ($(this).is(':checked')) {
        var html = '<span class="select-span-style" title="' + title + '">#' + title + '</span>';
        $('.hida').append(html);
        $(this).attr('checked', true);
        tags.push($(this).context.id);
    }
    else {
        $('span[title="' + title + '"]').remove();
        var ret = $(".hida");
        $(this).removeAttr('checked');
        $('.dropdown dt a').append(ret);
        var index = tags.indexOf($(this).context.id);
        tags.splice(index, 1);
    }
});

// Author select handling ----------------------------------------------------------------------------------------------
function DropDown(el) {

    this.dd = el;
    this.placeholder = this.dd.children('p');
    this.opts = this.dd.find('ul.dropdown-select > li');
    this.selected = this.dd.find('input');
    this.val = '';
    this.index = -1;
    this.initEvents();
}
DropDown.prototype = {

    initEvents : function() {
        var obj = this;

        obj.dd.on('click', function(event){
            $(this).toggleClass('active');
            return false;
        });

        obj.opts.on('click',function(){
            var opt = $(this);
            var selected = document.getElementsByName('selected_author')[0];
            selected.value = opt.context.id;
            obj.val = opt.text();
            obj.index = opt.index();
            obj.placeholder.text('Author: ' + obj.val);
        });
    },
    getValue : function() {
        return this.val;
    },
    getIndex : function() {
        return this.index;
    }
};

$(function () {

    var dd = new DropDown($('#dd'));

    $(document).click(function () {
        $('.wrapper-dropdown-2').removeClass('active');
    });

});

// Setup current date and resize textarea elements, setup selected filters ---------------------------------------------
$(document).ready(function() {

    var brief = $("#brief");
    var content = $("#content");
    var selectedTags = $("input[name=selected_tags]");

    if(brief[0] != undefined) {
        var briefOffset = brief[0].offsetHeight - brief[0].clientHeight;
        brief.css('height', 'auto').css('height', brief[0].scrollHeight + briefOffset);
    }
    if(content[0] != undefined) {
        var contentOffset = content[0].offsetHeight - content[0].clientHeight;
        content.css('height', 'auto').css('height', content[0].scrollHeight + contentOffset);
    }

    if(selectedTags[0] != undefined) {
        tags = selectedTags.val().split("-");
        selectedTags.val(selectedTags.val().replace(/-/g, ''));
        if(tags[0] == '') {
            tags.shift();
        }
    }

    if($("#criteria-form")[0] != undefined) {
        var selectedAuthorId = $("input[name=selected_author]").val();

        if(selectedAuthorId != '' || selectedTags != '') {
            var selectedAuthorName = $('#' + selectedAuthorId).find('label').text();
            var span = $(".hida");
            $('.select-title').append(' ' + selectedAuthorName);

            for(var counter = 0; counter < tags.length; counter++) {
                var tag = $('#' + tags[counter]);
                tag.attr('checked', true);
                var tagName = tag.val();
                var html = '<span class="select-span-style" title="' + tagName + '">#' + tagName + '</span>';
                span.append(html);
            }
        }
    }
});

// Handling filter button click ----------------------------------------------------------------------------------------
$("#filter-button").click(function(event) {

    var selectedTags = document.getElementsByName('selected_tags')[0];
    var selectedAuthor = document.getElementsByName('selected_author')[0];
    if(tags.length != 0 || selectedAuthor.value != "") {
        selectedTags.value = tags.join("-");
        $("#criteria-form").submit();
    }
});

// PAGINATION NEWS -----------------------------------------------------------------------------------------------------
$("#button-prev").click(function(event) {

    var prevButton = $("#button-prev");
    var nextButton = $("#button-next");
    nextButton.removeClass("news-button-disabled");
    nextButton.addClass("active");

    if(prevButton.hasClass("active")) {
        $.ajax({
            type : "GET",
            url : "show-news-page",
            dataType : "html",
            data : {
                request_type : "ajax",
                navigation : "prev"
            },
            success : function(responseText) {
                $("#news-items-container").html(responseText);
                var pageElement = $(".page-number");
                var page = Number(pageElement.html()) - 1;
                pageElement.html(page);
            },
            error: function(jqXHR, exception) {
                if (jqXHR.status == 404) {
                    prevButton.addClass("news-button-disabled");
                    prevButton.removeClass("active");

                } else if (jqXHR.status == 500) {
                    window.location.replace("/news-management-admin/view/jsp/page/ServerErrorPage.jsp")
                } else if (jqXHR.status == 401) {
                    window.location.replace("/news-management-admin/login");
                }
            }
        });
    }
});

$("#button-next").click(function(event) {

    var prevButton = $("#button-prev");
    var nextButton = $("#button-next");
    prevButton.removeClass("news-button-disabled");
    prevButton.addClass("active");

    if(nextButton.hasClass("active")) {
        $.ajax({
            type : "GET",
            url : "show-news-page",
            dataType : "html",
            data : {
                request_type : "ajax",
                navigation : "next"
            },
            success : function(responseText) {
                $("#news-items-container").html(responseText);
                var pageElement = $(".page-number");
                var page = Number(pageElement.html()) + 1;
                pageElement.html(page);
            },
            error: function(jqXHR, exception) {
                if (jqXHR.status == 404) {
                    nextButton.addClass("news-button-disabled");
                    nextButton.removeClass("active");

                } else if (jqXHR.status == 500) {
                    window.location.replace("/news-management-admin/view/jsp/page/ServerErrorPage.jsp")
                } else if (jqXHR.status == 401) {
                    window.location.replace("/news-management-admin/login");
                }
            }
        });
    }
});

// PAGINATION NEWS_MESSAGE ---------------------------------------------------------------------------------------------
$("#prev").click(function(event) {

    var alertMessage = $(".alert-message");
    var prevButton = $("#prev");
    var nextButton = $("#next");
    nextButton.removeClass("news-message-button-disabled");
    nextButton.addClass("active");
    var timeZone = new Date().getTimezoneOffset() / 60;

    if(prevButton.hasClass("active")) {
        $.ajax({
            type : "GET",
            url : "show-news-message-page",
            dataType : "html",
            data : {
                request_type : "ajax",
                navigation : "prev",
                time_zone : timeZone
            },
            success : function(responseText) {
                $("#news-message").html(responseText);
                if(!alertMessage.hasClass("hidden")) {
                    alertMessage.addClass("hidden");
                }
            },
            error: function(jqXHR, exception) {
                if (jqXHR.status == 404) {
                    prevButton.addClass("news-message-button-disabled");
                    prevButton.removeClass("active");

                } else if (jqXHR.status == 500) {
                    window.location.replace("/news-management-admin/view/jsp/page/ServerErrorPage.jsp")
                } else if (jqXHR.status == 401) {
                    window.location.replace("/news-management-admin/login");
                }
            }
        });
    }
});

$("#next").click(function(event) {

    var alertMessage = $(".alert-message");
    var prevButton = $("#prev");
    var nextButton = $("#next");
    prevButton.removeClass("news-message-button-disabled");
    prevButton.addClass("active");
    var timeZone = new Date().getTimezoneOffset() / 60;

    if(nextButton.hasClass("active")) {
        $.ajax({
            type : "GET",
            url : "show-news-message-page",
            dataType : "html",
            data : {
                request_type : "ajax",
                navigation : "next",
                time_zone : timeZone
            },
            success : function(responseText) {
                $("#news-message").html(responseText);
                if(!alertMessage.hasClass("hidden")) {
                    alertMessage.addClass("hidden");
                }
            },
            error: function(jqXHR, exception) {
                if (jqXHR.status == 404) {
                    nextButton.addClass("news-message-button-disabled");
                    nextButton.removeClass("active");

                } else if (jqXHR.status == 500) {
                    window.location.replace("/news-management-admin/view/jsp/page/ServerErrorPage.jsp")
                } else if (jqXHR.status == 401) {
                    window.location.replace("/news-management-admin/login");
                }
            }
        });
    }
});

// Post and delete comment query ---------------------------------------------------------------------------------------
$("#post-comment").click(function(event) {

    var commentArea = $("#comment-area");
    var alertMessage = $(".alert-message");
    var commentText = commentArea.val();
    var length = commentText.length;
    var locale = $("input[name=locale]").val();
    moment().locale(locale);
    var date = moment().format('DD MMM YYYY hh:mm:ss').toString();

    if(length != 0 && length <= 100) {
        $.ajax({
            type : "POST",
            url : "post-comment",
            dataType : "html",
            data : {
                request_type : "ajax",
                comment_text : commentText
            },
            success : function(commentId) {
                var noCommentsElement = $("#no-comments-element");
                if(noCommentsElement.get(0)) {
                    noCommentsElement.addClass("hidden");
                }
                if(!alertMessage.hasClass("hidden")) {
                    alertMessage.addClass("hidden");
                }
                var newCommentHtml = '<div class="comment"><input type="hidden" name="comment_id" value="'+ commentId +
                    '"><input class="button delete-comment-button" type="button" name="delete"/><span class="date-span row">' + date +
                    '</span> <p class="text-title comment-text row">' + commentText.replace(/</g,'&lt;').replace(/>/g,'&gt;') + '</p></div>';
                $("#comments").append(newCommentHtml);
                commentArea.val("");
            },
            error: function(jqXHR) {
                if (jqXHR.status == 400) {
                    commentArea.val("");
                    alertMessage.removeClass("hidden");
                } else if (jqXHR.status == 500) {
                    window.location.replace("/news-management-admin/view/jsp/page/ServerErrorPage.jsp")
                } else if (jqXHR.status == 401) {
                    window.location.replace("/news-management-admin/login");
                }
            }
        });
    }
    else {
        commentArea.val("");
        alertMessage.removeClass("hidden");
    }
});

$("#news-message").on('click','.delete-comment-button', function(event) {

    var button = $(this);
    var comment = button.parent();
    var commentId = button.prev().val();

    $.ajax({
        type : "POST",
        url : "delete-comment",
        dataType : "html",
        data : {
            request_type : "ajax",
            comment_id : commentId
        },
        success : function() {
            comment.remove();
        },
        error: function(jqXHR) {
            if (jqXHR.status == 400) {
                commentArea.val("");
            } else if (jqXHR.status == 500) {
                window.location.replace("/news-management-admin/view/jsp/page/ServerErrorPage.jsp")
            } else if (jqXHR.status == 401) {
                window.location.replace("/news-management-admin/login");
            }
        }
    });
});

// Edit button animation -----------------------------------------------------------------------------------------------
$("div .edit").click(function() {

    var edit = $(this);
    edit.addClass("hidden");
    edit.parent().addClass("change-padding");
    edit.parent().css("background-color", "rgba(87, 86, 85, 1)");
    edit.prev().addClass("change-width");
    edit.prev().removeAttr("disabled");
    setTimeout(function() {
        edit.next().removeClass("hidden");
    }, 600);
});

$("div .cancel").click(function() {

    var edit = $(this).parent().prev();
    $(this).parent().addClass("hidden");
    edit.prev().removeClass("change-width");
    edit.prev().attr("disabled", true);
    edit.parent().removeClass("change-padding");
    edit.parent().css("background-color", "transparent");
    setTimeout(function() {
        edit.removeClass("hidden");
    }, 600);
});

// Resize textarea elements by content ---------------------------------------------------------------------------------
jQuery.each(jQuery('textarea'), function() {
    var offset = this.offsetHeight - this.clientHeight;

    var resizeTextarea = function(el) {
        jQuery(el).css('height', 'auto').css('height', el.scrollHeight + offset);
    };
    jQuery(this).on('keyup input', function() { resizeTextarea(this); });
});

// Delete selected news ------------------------------------------------------------------------------------------------
$("#news-items-container").on('click','#delete-news', function(event) {

    var data = [];
    $("input:checkbox[name=deleteNews]:checked").each(function() {
        data.push($(this).val());
    });

    if (data.length > 0) {

        $.ajax({
            type : "POST",
            url : "delete-news",
            dataType : "html",
            data : {
                request_type : "ajax",
                news_ids : data.join("-")
            },
            success : function(response) {
                $("#news-items-container").html(response);
            },
            error: function(jqXHR) {
                if (jqXHR.status == 400) {
                    alert('Invalid id format.')
                } else if (jqXHR.status == 500) {
                    window.location.replace("/news-management-admin/view/jsp/page/ServerErrorPage.jsp")
                } else if (jqXHR.status == 401) {
                    window.location.replace("/news-management-admin/login");
                }
            }
        });
    }
});

// News adding / editing validation ------------------------------------------------------------------------------------
$("#news-form").submit(function( event ) {

    var valid = true;
    var title = $("input[name=title]");
    var date = $("input[name=date]");
    var shortText = $("textarea[name=short_text]");
    var fullText = $("textarea[name=full_text]");
    var authorField = $("#dd");

    authorField.css("border-color", "rgb(87, 86, 85)");
    title.next().addClass("hidden");
    date.next().addClass("hidden");
    shortText.next().addClass("hidden");
    fullText.next().addClass("hidden");

    var selectedTags = document.getElementsByName('selected_tags')[0];
    var selectedAuthor = document.getElementsByName('selected_author')[0];
    if(tags.length != 0) {
        selectedTags.value = tags.join("-");
    }
    else {
        selectedTags.value = "";
    }
    var regex_date = /^(0[1-9]|1\d|2\d|3[01])[\/.-](0[1-9]|1[0-2])[\/.-](19|20)\d{2}$/;

    if(selectedAuthor.value == "") {
        valid = false;
        authorField.css("border-color", "#ff5566");
    }
    if(title.val().length < 1 || title.val().length > 30) {
        valid = false;
        title.next().removeClass("hidden");
        title.val("");
    }
    if (!regex_date.test(date.val())) {
        valid = false;
        date.next().removeClass("hidden");
        date.val("");
    }
    if(shortText.val().length < 1 || shortText.val().length > 100) {
        valid = false;
        shortText.next().removeClass("hidden");
        shortText.val("");
    }
    if(fullText.val().length < 1 || fullText.val().length > 2000) {
        valid = false;
        fullText.next().removeClass("hidden");
        fullText.val("");
    }

    if(!valid) {
        event.preventDefault();
    }
});

// Tag adding / editing / deleting validation -------------------------------------------------------------------------------------
$("#add-tag-form").submit(function( event ) {

    var tagName = $(this).find("input[name=tag_name]").val();
    var errorField = $(this).find(".invalid-field");

    errorField.addClass("hidden");

    if(tagName.length < 1 || tagName.length > 30) {
        errorField.removeClass("hidden");
        event.preventDefault();
    }
});

$("input[name=update]").click(function(event) {

    var form = $("#edit-tag-form");
    var errorField = $(this).parent().next();
    var tagId = $(this).parent().parent().find("input[name=tag_id]").val();
    var tagName = $(this).parent().parent().find("input[name=tag_name]").val();

    errorField.addClass("hidden");

    form.find("input[name=tag_id]").val(tagId);
    form.find("input[name=tag_name]").val(tagName);

    if(tagName.length < 1 || tagName.length > 30) {
        errorField.removeClass("hidden");
    }
    else {
        form.submit();
    }
});

$("input[name=delete]").click(function(event) {

    var form = $("#delete-tag-form");
    var tagId = $(this).parent().parent().find("input[name=tag_id]").val();
    form.find("input[name=tag_id]").val(tagId);
    form.submit();
});

// Author adding / editing / expiring validation -------------------------------------------------------------------------------------
$("#add-author-form").submit(function( event ) {

    var authorName = $(this).find("input[name=author_name]").val();
    var errorField = $(this).find(".invalid-field");

    errorField.addClass("hidden");

    if(authorName.length < 1 || authorName.length > 30) {
        errorField.removeClass("hidden");
        event.preventDefault();
    }
});

$("input[name=update-author]").click(function(event) {

    var form = $("#edit-author-form");
    var errorField = $(this).parent().next();
    var tagId = $(this).parent().parent().find("input[name=author_id]").val();
    var authorName = $(this).parent().parent().find("input[name=author_name]").val();

    errorField.addClass("hidden");

    form.find("input[name=author_id]").val(tagId);
    form.find("input[name=author_name]").val(authorName);

    if(authorName.length < 1 || authorName.length > 30) {
        errorField.removeClass("hidden");
    }
    else {
        form.submit();
    }
});

$("input[name=expire-author]").click(function(event) {

    var form = $("#expire-author-form");
    var tagId = $(this).parent().parent().find("input[name=author_id]").val();
    form.find("input[name=author_id]").val(tagId);
    form.submit();
});

// Authorization validation --------------------------------------------------------------------------------------------
$(".authorization").submit(function(event) {

    var login = $(this).find("#login");
    var password = $(this).find("#password");

    login.removeClass("invalid-auth-field");
    password.removeClass("invalid-auth-field");

    if(login.val().length < 1 || login.val().length > 30) {
        login.addClass("invalid-auth-field");
        event.preventDefault();
    }
    if(password.val().length < 1 || password.val().length > 32) {
        password.addClass("invalid-auth-field");
        event.preventDefault();
    }
});

// News message href with timezone -------------------------------------------------------------------------------------
function hrefNewsMessageWithTimeZone(id, index) {
    var current = new Date();
    document.location.href = "show-news-message?news_id=" + id +
        "&news_index=" + index +
        "&time_zone=" + -current.getTimezoneOffset()/60;
}

// Close result window -------------------------------------------------------------------------------------------------
$(".result-div").click(function(event) {
    if(event.target.id == "result-container") {
        $(".result-div").addClass("hidden");
    }
});
