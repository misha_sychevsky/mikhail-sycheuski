package com.epam.portal.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;
import com.epam.portal.dao.CommentDAO;
import com.epam.portal.model.Comment;
import com.epam.portal.service.implementation.CommentServiceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.mockito.Mockito.*;

/**
 * Comment service Mockito test class.
 * Class <code>CommentServiceTest</code> contains Mockito unit tests for all <code>CommentService</code> operations.
 */
@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(locations = {"/spring-context.xml"})
public class CommentServiceTest {

    /**
     * Mock object, which will be inject into CommentServiceImpl class.
     */
    @Mock
    CommentDAO commentDAO;
    /**
     * Service layer object, which we will testing.
     */
    @InjectMocks
    CommentServiceImpl commentService;

    @Test
    public void addComment() throws Exception {

        Comment comment = makeComment();
        when(commentDAO.insert(comment)).thenReturn(1L);

        Long commentId = commentService.addComment(comment);
        verify(commentDAO, times(1)).insert(comment);
        assertEquals(commentId, Long.valueOf(1L));
    }

    @Test
    public void editComment() throws Exception {

        Comment comment = makeComment();

        commentService.updateComment(comment);
        verify(commentDAO, times(1)).update(comment);
    }

    @Test
    public void deleteComment() throws Exception {

        Comment comment = makeComment();

        commentService.deleteComment(comment);
        verify(commentDAO, times(1)).delete(comment);
    }

    @Test
    public void getCommentById() throws Exception {

        Comment comment = makeComment();
        Long commentId = comment.getCommentId();
        Long newsId = comment.getNewsId();
        String commentText = comment.getCommentText();

        when(commentService.getCommentById(commentId)).thenReturn(comment);
        comment = commentService.getCommentById(commentId);

        verify(commentDAO, times(1)).get(commentId);
        assertEquals(commentId, comment.getCommentId());
        assertEquals(commentText, comment.getCommentText());
        assertEquals(newsId, comment.getNewsId());
    }

    @Test
    public void viewTheListOfComments() throws Exception {

        when(commentDAO.getAll()).thenReturn(new ArrayList<Comment>());

        List<Comment> comments = commentService.viewTheListOfComments();
        verify(commentDAO, times(1)).getAll();
        assertNotNull(comments);
    }

    @Test
    public void getCommentsByNews() throws Exception {

        Long newsId = 1L;
        when(commentDAO.getCommentsByNews(newsId)).thenReturn(new ArrayList<Comment>());

        List<Comment> comments = commentService.getCommentsByNews(newsId);
        verify(commentDAO, times(1)).getCommentsByNews(newsId);
        assertNotNull(comments);
    }

    @Test
    public void addComments() throws Exception {

        List<Comment> comments = new ArrayList<>();

        commentDAO.insertAll(comments);
        verify(commentDAO, timeout(1)).insertAll(comments);
    }

    private Comment makeComment() {

        Comment comment = new Comment();
        comment.setCommentId(1L);
        comment.setCommentText("this is a comment");
        comment.setNewsId(1L);
        comment.setCreationDate(new Date());
        return comment;
    }
}
