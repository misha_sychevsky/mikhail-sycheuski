package com.epam.portal.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;
import com.epam.portal.dao.TagDAO;
import com.epam.portal.model.Tag;
import com.epam.portal.service.implementation.TagServiceImpl;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.mockito.Mockito.*;

/**
 * Tag service Mockito test class.
 * Class <code>TagServiceTest</code> contains Mockito unit tests for all <code>TagService</code> operations.
 */
@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(locations = {"/spring-context.xml"})
public class TagServiceTest {

    /**
     * Mock object, which will be inject into TagServiceImpl class.
     */
    @Mock
    TagDAO tagDAO;
    /**
     * Service layer object, which we will testing.
     */
    @InjectMocks
    TagServiceImpl tagService;

    @Test
    public void addTag() throws Exception {

        Tag tag = makeTag();

        when(tagDAO.insert(tag)).thenReturn(1L);

        Long tagId = tagService.addTag(tag);
        verify(tagDAO, times(1)).insert(tag);
        assertEquals(tagId, Long.valueOf(1L));
    }

    @Test
    public void deleteTag() throws Exception {

        Tag tag = makeTag();

        tagService.deleteTag(tag);
        verify(tagDAO, times(1)).delete(tag);
    }

    @Test
    public void updateTag() throws Exception {

        Tag tag = makeTag();

        tagService.updateTag(tag);
        verify(tagDAO, times(1)).update(tag);
    }

    @Test
    public void getTagById() throws Exception {

        Tag tag = makeTag();
        Long tagId = tag.getTagId();
        String tagName = tag.getName();

        when(tagDAO.get(tagId)).thenReturn(tag);
        tag = tagService.getTagById(tagId);

        verify(tagDAO, times(1)).get(tagId);
        assertEquals(tagId, tag.getTagId());
        assertEquals(tagName, tag.getName());
    }

    @Test
    public void viewTheListOfTags() throws Exception {

        when(tagDAO.getAll()).thenReturn(new ArrayList<Tag>());

        List<Tag> tags = tagService.viewTheListOfTags();
        verify(tagDAO, times(1)).getAll();
        assertNotNull(tags);
    }

    @Test
    public void getTagsByNews() throws Exception {

        Long newsId = 1L;
        when(tagDAO.getTagsByNews(newsId)).thenReturn(new ArrayList<Tag>());

        List<Tag> tags = tagService.getTagsByNews(newsId);
        verify(tagDAO, times(1)).getTagsByNews(newsId);
        assertNotNull(tags);
    }

    private Tag makeTag() {

        Tag tag = new Tag();
        tag.setTagId(1L);
        tag.setName("Tag name");
        return tag;
    }
}
