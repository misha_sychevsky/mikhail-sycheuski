package com.epam.portal.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;
import com.epam.portal.model.*;
import com.epam.portal.service.implementation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * News message service Mockito test class.
 * Class <code>NewsMessageTest</code> contains Mockito unit tests for all <code>NewsMessageService</code> operations.
 */
@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(locations = {"/spring-context.xml"})
public class NewsMessageServiceTest {

    /**
     * Mock object, which will be inject into NewsMessageServiceImpl class.
     */
    @Mock
    private NewsService newsService;
    /**
     * Mock object, which will be inject into NewsMessageImpl class.
     */
    @Mock
    private AuthorService authorService;
    /**
     * Mock object, which will be inject into NewsMessageImpl class.
     */
    @Mock
    private CommentService commentService;
    /**
     * Mock object, which will be inject into NewsMessageImpl class.
     */
    @Mock
    private TagService tagService;
    /**
     * Service layer object, which we will testing.
     */
    @InjectMocks
    private NewsMessageServiceImpl newsMessageService;

    @Test
    public void saveNewsMessage() throws Exception {

        NewsMessage newsMessage = makeNewsMessage();
        Author author = newsMessage.getAuthor();
        News news = newsMessage.getNews();
        List<Tag> tags = newsMessage.getTags();

        newsMessageService.saveNewsMessage(newsMessage);

        verify(newsService, times(1)).addNews(news);
        verify(newsService, times(1)).addAuthorToNews(news.getNewsId(), author.getAuthorId());
        verify(newsService, times(1)).addTagsToNews(tags, news.getNewsId());
    }

    @Test
    public void viewSingleNewsMessage() throws Exception {

        NewsMessage newsMessage = makeNewsMessage();
        Long newsId = newsMessage.getNews().getNewsId();
        Author author = newsMessage.getAuthor();
        News news = newsMessage.getNews();
        List<Tag> tags = newsMessage.getTags();
        List<Comment> comments = newsMessage.getComments();

        when(newsService.getNewsById(newsId)).thenReturn(news);
        when(authorService.getAuthorByNews(newsId)).thenReturn(author);
        when(commentService.getCommentsByNews(newsId)).thenReturn(comments);
        when(tagService.getTagsByNews(newsId)).thenReturn(tags);
        newsMessage = newsMessageService.viewSingleNewsMessage(newsId);

        verify(newsService, times(1)).getNewsById(newsId);
        verify(authorService, times(1)).getAuthorByNews(newsId);
        verify(tagService, times(1)).getTagsByNews(newsId);
        verify(commentService, times(1)).getCommentsByNews(newsId);
        assertEquals(news, newsMessage.getNews());
        assertEquals(author, newsMessage.getAuthor());
        assertEquals(tags, newsMessage.getTags());
        assertEquals(comments, newsMessage.getComments());
    }

    private NewsMessage makeNewsMessage() {

        Long newsId = 0L;
        Author author = new Author();
        author.setAuthorId(1L);
        author.setName("lol");
        author.setExpired(new Date());
        News news = new News();
        news.setNewsId(newsId);
        List<Tag> tags = new ArrayList<Tag>() {{
            add(new Tag());
            add(new Tag());
        }};
        List<Comment> comments = new ArrayList<Comment>() {{
            add(new Comment());
            add(new Comment());
        }};
        NewsMessage newsMessage = new NewsMessage();
        newsMessage.setAuthor(author);
        newsMessage.setNews(news);
        newsMessage.setTags(tags);
        newsMessage.setComments(comments);
        return newsMessage;
    }
}
