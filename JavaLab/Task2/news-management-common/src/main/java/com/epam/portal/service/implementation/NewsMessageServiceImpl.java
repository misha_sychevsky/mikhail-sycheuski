package com.epam.portal.service.implementation;

import org.springframework.transaction.annotation.Transactional;
import com.epam.portal.model.News;
import com.epam.portal.model.NewsMessage;
import com.epam.portal.model.SearchCriteria;
import com.epam.portal.service.*;
import com.epam.portal.service.exception.ServiceException;

import java.util.ArrayList;
import java.util.List;

/**
 * News message service.
 * This is an implementation of the news message service interface.
 */
public class NewsMessageServiceImpl implements NewsMessageService {

    private NewsService newsService;
    private TagService tagService;
    private AuthorService authorService;
    private CommentService commentService;

    public void setNewsService(NewsService newsService) {

        this.newsService = newsService;
    }

    public void setTagService(TagService tagService) {

        this.tagService = tagService;
    }

    public void setAuthorService(AuthorService authorService) {

        this.authorService = authorService;
    }

    public void setCommentService(CommentService commentService) {

        this.commentService = commentService;
    }

    @Override
    @Transactional(rollbackFor = ServiceException.class)
    public void updateNewsMessage(NewsMessage newsMessage) throws ServiceException {

        Long newsId = newsMessage.getNews().getNewsId();
        newsService.editNews(newsMessage.getNews());
        newsService.deleteTagsFromNews(newsId);
        newsService.addTagsToNews(newsMessage.getTags(), newsId);
        newsService.deleteAuthorFromNews(newsId);
        newsService.addAuthorToNews(newsId, newsMessage.getAuthor().getAuthorId());
    }

    @Override
    @Transactional(rollbackFor = ServiceException.class)
    public void saveNewsMessage(NewsMessage newsMessage) throws ServiceException {

        long newsId = newsService.addNews(newsMessage.getNews());
        newsService.addAuthorToNews(newsId, newsMessage.getAuthor().getAuthorId());
        newsService.addTagsToNews(newsMessage.getTags(), newsId);
    }

    @Override
    @Transactional(rollbackFor = ServiceException.class)
    public NewsMessage viewSingleNewsMessage(long newsId) throws ServiceException {

        NewsMessage newsMessage = new NewsMessage();
        newsMessage.setNews(newsService.getNewsById(newsId));
        newsMessage.setAuthor(authorService.getAuthorByNews(newsId));
        newsMessage.setTags(tagService.getTagsByNews(newsId));
        newsMessage.setComments(commentService.getCommentsByNews(newsId));

        return newsMessage;
    }

    @Override
    @Transactional(rollbackFor = ServiceException.class)
    public List<NewsMessage> getNewsMessagePage(SearchCriteria searchCriteria, int start, int end) throws ServiceException {

        List<NewsMessage> newsMessages = new ArrayList<>();
        for(News news : newsService.searchNewsBySearchCriteria(searchCriteria, start, end)) {
            NewsMessage newsMessage = new NewsMessage();
            newsMessage.setNews(news);
            newsMessage.setAuthor(authorService.getAuthorByNews(news.getNewsId()));
            newsMessage.setComments(commentService.getCommentsByNews(news.getNewsId()));
            newsMessage.setTags(tagService.getTagsByNews(news.getNewsId()));
            newsMessages.add(newsMessage);
        }
        return newsMessages;
    }

    @Override
    @Transactional(rollbackFor = ServiceException.class)
    public NewsMessage getNextNewsMessage(SearchCriteria searchCriteria, Long currentNewsMessageId) throws ServiceException {

        NewsMessage newsMessage = null;
        News news = newsService.getNextNews(searchCriteria, currentNewsMessageId);
        if(news != null) {
            newsMessage = new NewsMessage();
            newsMessage.setNews(news);
            newsMessage.setAuthor(authorService.getAuthorByNews(news.getNewsId()));
            newsMessage.setComments(commentService.getCommentsByNews(news.getNewsId()));
        }

        return newsMessage;
    }

    @Override
    @Transactional(rollbackFor = ServiceException.class)
    public NewsMessage getPrevNewsMessage(SearchCriteria searchCriteria, Long currentNewsMessageId) throws ServiceException {

        NewsMessage newsMessage = null;
        News news = newsService.getPrevNews(searchCriteria, currentNewsMessageId);
        if(news != null) {
            newsMessage = new NewsMessage();
            newsMessage.setNews(news);
            newsMessage.setAuthor(authorService.getAuthorByNews(news.getNewsId()));
            newsMessage.setComments(commentService.getCommentsByNews(news.getNewsId()));
        }

        return newsMessage;
    }
}
