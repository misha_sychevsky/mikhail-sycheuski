package com.epam.portal.service.implementation;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import com.epam.portal.dao.RoleDAO;
import com.epam.portal.dao.UserDAO;
import com.epam.portal.dao.exception.DAOException;
import com.epam.portal.model.Role;
import com.epam.portal.model.User;

import java.util.List;
import java.util.Set;

public class CustomUserServiceImpl implements UserDetailsService {

    private UserDAO userDAO;

    private RoleDAO roleDAO;

    public void setUserDAO(UserDAO userDAO) {

        this.userDAO = userDAO;
    }

    public void setRoleDAO(RoleDAO roleDAO) {

        this.roleDAO = roleDAO;
    }

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {

        try {
            User user = userDAO.loadUserByUsername(login);
            Set<Role> usersRoles = roleDAO.readRolesByUser(user.getUserId());
            user.setAuthorities(usersRoles);

            return user;
        }
        catch (DAOException e) {
            throw new UsernameNotFoundException("Can't read user", e);
        }
    }
}
