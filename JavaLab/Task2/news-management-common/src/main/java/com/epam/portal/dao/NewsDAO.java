package com.epam.portal.dao;

import com.epam.portal.dao.exception.DAOException;
import com.epam.portal.model.SearchCriteria;
import com.epam.portal.model.News;

import java.util.List;

/**
 * News data access object interface.
 * This interface is necessary for interaction with a table of news in the database.
 */
public interface NewsDAO extends GenericDAO<News> {

    /**
     * This method returns the number of news in the NEWS table.
     * Can produce <code>DAOException</code>.
     *
     * @return - count of news
     * @throws DAOException
     */
    int countNews() throws DAOException;

    /**
     * This method returns all news that meets the search criteria in the parameter.
     * Use <code>SearchQueryBuilder</code> to build the query.
     * Can produce <code>DAOException</code>.
     *
     * @param searchCriteria
     * @param start
     * @param end
     * @return - <code>List</code> of News
     * @throws DAOException
     */
    List<News> getNewsBySearchCriteria(SearchCriteria searchCriteria, int start, int end) throws DAOException;

    /**
     * This method returns single news that is situated after the current in the list of news filtered by the search criteria.
     * Use <code>SearchQueryBuilder</code> to build the query.
     * Can produce <code>DAOException</code>.
     *
     * @param searchCriteria
     * @param currentNewsMessageId
     * @return - <code>List</code> of News
     * @throws DAOException
     */
    News getNextNews(SearchCriteria searchCriteria, Long currentNewsMessageId) throws DAOException;

    /**
     * This method returns single news that is situated before current in the list of news filtered by the search criteria.
     * Use <code>SearchQueryBuilder</code> to build the query.
     * Can produce <code>DAOException</code>.
     *
     * @param searchCriteria
     * @param currentNewsMessageId
     * @return - previous <code>News</code> object
     * @throws DAOException
     */
    News getPrevNews(SearchCriteria searchCriteria, Long currentNewsMessageId) throws DAOException;
}
