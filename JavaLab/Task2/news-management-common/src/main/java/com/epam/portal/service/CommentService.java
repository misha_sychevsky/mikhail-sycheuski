package com.epam.portal.service;

import com.epam.portal.model.Comment;
import com.epam.portal.service.exception.ServiceException;

import java.util.List;

/**
 * Comment service interface.
 * This interface represents business operations for comment entity.
 */
public interface CommentService {

    /**
     * This method adds a new comment.
     * Can produce <code>ServiceException</code>.
     *
     * @param comment
     * @return <code>Comment</code> id
     * @throws ServiceException
     */
    Long addComment(Comment comment) throws ServiceException;

    /**
     * This method returns the comment by id.
     * Can produce <code>ServiceException</code>.
     *
     * @param commentId
     * @return <code>Comment</code>
     * @throws ServiceException
     */
    Comment getCommentById(Long commentId) throws ServiceException;

    /**
     * This method adds more than one comments.
     * Can produce <code>ServiceException</code>.
     *
     * @param comments
     * @throws ServiceException
     */
    void addComments(List<Comment> comments) throws ServiceException;

    /**
     * This method edits comment information.
     * Can produce <code>ServiceException</code>.
     *
     * @param comment
     * @throws ServiceException
     */
    void updateComment(Comment comment) throws ServiceException;

    /**
     * This method deletes the comment.
     * Can produce <code>ServiceException</code>.
     *
     * @param comment
     * @throws ServiceException
     */
    void deleteComment(Comment comment) throws ServiceException;

    /**
     * This method return all news comments.
     * Can produce <code>ServiceException</code>.
     *
     * @param newsId
     * @return <code>List</code> of comments
     * @throws ServiceException
     */
    List<Comment> getCommentsByNews(Long newsId) throws ServiceException;

    /**
     * This method returns all comments.
     * Can produce <code>ServiceException</code>.
     *
     * @return <code>List</code> of comments
     * @throws ServiceException
     */
    List<Comment> viewTheListOfComments() throws ServiceException;
}
