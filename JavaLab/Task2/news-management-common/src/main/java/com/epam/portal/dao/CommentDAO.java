package com.epam.portal.dao;

import com.epam.portal.dao.exception.DAOException;
import com.epam.portal.model.Comment;

import java.util.List;

/**
 * Comment data access object interface.
 * This interface is necessary for interaction with a table of comments in the database.
 */
public interface CommentDAO extends GenericDAO<Comment> {

    /**
     * This method removes all comments of the news in the COMMENTS table.
     * Can produce <code>DAOException</code>.
     *
     * @param newsId
     * @throws DAOException
     */
    void deleteCommentsByNews(Long newsId) throws DAOException;

    /**
     * This method returns all comments of the news in the parameter.
     * Can produce <code>DAOException</code>.
     *
     * @param newsId
     * @return - <code>List</code> of comments
     * @throws DAOException
     */
    List<Comment> getCommentsByNews(Long newsId) throws DAOException;

    /**
     * This method inserts all comments from parameter into the COMMENTS table.
     * Can produce <code>DAOException</code>.
     *
     * @param comments
     * @throws DAOException
     */
    void insertAll(List<Comment> comments) throws DAOException;
}
