package com.epam.portal.dao;

import com.epam.portal.dao.util.ConnectionManager;
import org.hibernate.SessionFactory;

/**
 * Abstract DAO class contains ref to the connection manager object witch manages work with DB connections and resources.
 */
public abstract class AbstractDAO {

    /**
     * Connection manager class is responsible for binding to database and realising resources.
     */
    protected SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {

        this.sessionFactory = sessionFactory;
    }

}
