package com.epam.portal.dao.exception;

/**
 * DAO Exception.
 * This class represents exception which can be occurred in DAO Layer.
 */
public class DAOException extends Exception {

    /**
     * DAO Exception message.
     */
    public static final String ERROR_MESSAGE = "Error while working with database.";

    public DAOException(String message, Exception e)
    {
        super(message, e);
    }

    public DAOException(Exception e)
    {
        super(e);
    }

    public DAOException()
    {
        super();
    }

    public DAOException(String message)
    {
        super(message);
    }
}
