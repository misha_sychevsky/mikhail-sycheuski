package com.epam.portal.dao.jdbc.oracle;

import com.epam.portal.dao.AbstractDAO;
import com.epam.portal.dao.AuthorDAO;
import com.epam.portal.dao.exception.DAOException;
import com.epam.portal.model.Author;

import java.sql.*;
import java.util.*;
import java.util.Date;

/**
 * Oracle implementation of the DAO interface for author entity.
 */
public class AuthorDAOImpl extends AbstractDAO implements AuthorDAO {

    /**
     * Name of the primary key column in AUTHOR table in the database.
     */
    public static final String ID_COLUMN = "AUTHOR_ID";

    /**
     * Name of the column with author name in AUTHOR table in the database.
     */
    public static final String NAME_COLUMN = "AUTHOR_NAME";

    /**
     * Name of the expire column in AUTHOR table in the database.
     */
    public static final String EXPIRE_COLUMN = "EXPIRED";

    @Override
    public void associateNewsWithAuthor(Long newsId, Long authorId) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(Queries.SQL_INSERT_NEWS_AUTHOR);
            statement.setLong(1, newsId);
            statement.setLong(2, authorId);
            statement.executeUpdate();
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(statement, connection);
        }
    }

    @Override
    public void deleteAssociationNewsWithAuthor(Long newsId) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(Queries.SQL_DELETE_NEWS_AUTHOR);
            statement.setLong(1, newsId);
            statement.executeUpdate();
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(statement, connection);
        }
    }

    @Override
    public void deleteAllAssociationsNewsWithAuthor(Long authorId) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(Queries.SQL_DELETE_ALL_NEWS_AUTHOR);
            statement.setLong(1, authorId);
            statement.executeUpdate();
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(statement, connection);
        }
    }

    @Override
    public Long insert(Author author) throws DAOException {

        String generatedColumns[] = {ID_COLUMN};
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet generatedKeys = null;

        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(Queries.SQL_INSERT_AUTHOR, generatedColumns);
            statement.setString(1, author.getName());
            statement.executeUpdate();

            long id = 0;
            generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                id = generatedKeys.getLong(1);
            }
            return id;
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(generatedKeys, statement, connection);
        }
    }

    @Override
    public void delete(Author author) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(Queries.SQL_DELETE_AUTHOR);
            statement.setLong(1, author.getAuthorId());
            statement.executeUpdate();
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(statement, connection);
        }
    }

    @Override
    public void update(Author author) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(Queries.SQL_UPDATE_AUTHOR);
            statement.setString(1, author.getName());
            statement.setLong(2, author.getAuthorId());
            statement.executeUpdate();
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(statement, connection);
        }
    }

    @Override
    public void setAuthorExpired(Long authorId) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(Queries.SQL_SET_AUTHOR_EXPIRED);
            statement.setTimestamp(1, new Timestamp(new Date().getTime()), Calendar.getInstance(TimeZone.getTimeZone("UTC")));
            statement.setLong(2, authorId);
            statement.executeUpdate();
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(statement, connection);
        }
    }

    @Override
    public Author getAuthorByNews(Long newsId) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(Queries.SQL_GET_AUTHOR_BY_NEWS);
            statement.setLong(1, newsId);

            resultSet = statement.executeQuery();
            Author author = null;
            while (resultSet.next()) {
                author = makeAuthor(resultSet);
            }
            return author;
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(resultSet, statement, connection);
        }
    }

    @Override
    public List<Author> getAll() throws DAOException {

        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = manager.getConnection();

            statement = connection.createStatement();

            resultSet = statement.executeQuery(Queries.SQL_GET_ALL_AUTHORS);
            List<Author> authors = new ArrayList<>();
            while (resultSet.next()) {
                authors.add(makeAuthor(resultSet));
            }
            return authors;
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(resultSet, statement, connection);
        }
    }

    @Override
    public Author get(Long authorId) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(Queries.SQL_GET_AUTHOR_BY_ID);
            statement.setLong(1, authorId);

            resultSet = statement.executeQuery();
            Author author = null;
            if (resultSet.next()) {
                author = makeAuthor(resultSet);
            }
            return author;
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(resultSet, statement, connection);
        }
    }

    /**
     * This method build <code>Author</code> object with data from database.
     *
     * @param resultSet
     * @return <code>Author</code>
     * @throws SQLException
     */
    private Author makeAuthor(ResultSet resultSet) throws SQLException {

        Author author = new Author();
        author.setAuthorId(resultSet.getLong(ID_COLUMN));
        author.setName(resultSet.getString(NAME_COLUMN));
        Timestamp expired = resultSet.getTimestamp(EXPIRE_COLUMN);
        if(expired != null)
        {
            author.setExpired(new Date(expired.getTime()));
        }
        return author;
    }
}
