package com.epam.portal.dao.jdbc.oracle;

import com.epam.portal.dao.AbstractDAO;
import com.epam.portal.dao.CommentDAO;
import com.epam.portal.dao.exception.DAOException;
import com.epam.portal.model.Comment;

import java.sql.*;
import java.util.*;
import java.util.Date;

/**
 * Oracle implementation of the DAO interface for comment entity.
 */
public class CommentDAOImpl extends AbstractDAO implements CommentDAO {

    /**
     * Name of the primary key column in COMMENT table in the database.
     */
    public static final String ID_COLUMN = "COMMENT_ID";

    public static final String NEWS_ID_COLUMN = "NEWS_ID";

    public static final String COMMENT_TEXT_COLUMN = "COMMENT_TEXT";

    public static final String COMMENT_CREATION_DATE = "CREATION_DATE";

    /**
     * Default timezone
     */
    public static final String TIME_ZONE_GMT = "GMT";

    public Long insert(Comment comment) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet generatedKeys = null;
        String generatedColumns[] = {ID_COLUMN};

        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(Queries.SQL_INSERT_COMMENT, generatedColumns);
            statement.setLong(1, comment.getNewsId());
            statement.setString(2, comment.getCommentText());
            statement.setTimestamp(3, new Timestamp(comment.getCreationDate().getTime()), Calendar.getInstance(
                    TimeZone.getTimeZone(TIME_ZONE_GMT)));
            statement.executeUpdate();

            long id = 0;
            generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                id = generatedKeys.getLong(1);
            }
            return id;
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(generatedKeys, statement, connection);
        }
    }

    @Override
    public Comment get(Long commentId) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(Queries.SQL_GET_COMMENT_BY_ID);
            statement.setLong(1, commentId);

            resultSet = statement.executeQuery();
            Comment comment = null;
            if (resultSet.next()) {
                comment = makeComment(resultSet);
            }

            return comment;
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(resultSet, statement, connection);
        }
    }

    public void delete(Comment comment) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(Queries.SQL_DELETE_COMMENT);
            statement.setLong(1, comment.getCommentId());
            statement.executeUpdate();
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(statement, connection);
        }
    }

    @Override
    public void deleteCommentsByNews(Long newsId) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(Queries.SQL_DELETE_COMMENTS_BY_NEWS);
            statement.setLong(1, newsId);
            statement.executeUpdate();
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(statement, connection);
        }
    }

    public void update(Comment comment) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(Queries.SQL_UPDATE_COMMENT);
            statement.setString(1, comment.getCommentText());
            statement.setTimestamp(2, new Timestamp(comment.getCreationDate().getTime()), Calendar.getInstance(
                    TimeZone.getTimeZone(TIME_ZONE_GMT)));
            statement.setLong(3, comment.getCommentId());
            statement.executeUpdate();
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(statement, connection);
        }
    }

    @Override
    public List<Comment> getCommentsByNews(Long newsId) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(Queries.SQL_GET_COMMENTS_BY_NEWS);
            statement.setLong(1, newsId);

            resultSet = statement.executeQuery();
            List<Comment> comments = new ArrayList<>();
            while (resultSet.next()) {
                comments.add(makeComment(resultSet));
            }
            return comments;
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(resultSet, statement, connection);
        }
    }

    public List<Comment> getAll() throws DAOException {

        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = manager.getConnection();

            statement = connection.createStatement();

            resultSet = statement.executeQuery(Queries.SQL_GET_ALL_COMMENTS);
            List<Comment> comments = new ArrayList<>();
            while (resultSet.next()) {
                comments.add(makeComment(resultSet));
            }
            return comments;
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(resultSet, statement, connection);
        }
    }

    @Override
    public void insertAll(List<Comment> comments) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(Queries.SQL_INSERT_COMMENT);
            for (Comment comment : comments) {
                statement.setLong(1, comment.getNewsId());
                statement.setString(2, comment.getCommentText());
                statement.setTimestamp(3, new Timestamp(comment.getCreationDate().getTime()));
                statement.addBatch();
            }
            statement.executeBatch();
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(statement, connection);
        }
    }

    /**
     * This method build <code>Comment</code> object with data from database.
     *
     * @param resultSet
     * @return <code>Comment</code>
     * @throws SQLException
     */
    private Comment makeComment(ResultSet resultSet) throws SQLException {

        Comment comment = new Comment();
        comment.setCommentId(resultSet.getLong(ID_COLUMN));
        comment.setNewsId(resultSet.getLong(NEWS_ID_COLUMN));
        comment.setCommentText(resultSet.getString(COMMENT_TEXT_COLUMN));
        comment.setCreationDate(new Date(resultSet.getTimestamp(COMMENT_CREATION_DATE).getTime()));
        return comment;
    }
}
