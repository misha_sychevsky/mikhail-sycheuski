package com.epam.portal.service;

import com.epam.portal.model.NewsMessage;
import com.epam.portal.model.SearchCriteria;
import com.epam.portal.service.exception.ServiceException;

import java.util.List;

/**
 * News message service interface.
 * This is a super-service interface which represents business operations for composite news message entity.
 */
public interface NewsMessageService {

    /**
     * This method updates new news message and delegates updating of it's parts to the according services.
     * Can produce <code>ServiceException</code>.
     *
     * @param newsMessage
     * @throws ServiceException
     */
    void updateNewsMessage(NewsMessage newsMessage) throws ServiceException;

    /**
     * This method returns single news message entity by id.
     * Can produce <code>ServiceException</code>.
     *
     * @param newsId
     * @return <code>NewsMessage</code>
     * @throws ServiceException
     */
    NewsMessage viewSingleNewsMessage(long newsId) throws ServiceException;

    /**
     * This method adds new news message and delegates inserting of it's parts to the according services.
     * Can produce <code>ServiceException</code>.
     *
     * @param newsMessage
     * @throws ServiceException
     */
    void saveNewsMessage(NewsMessage newsMessage) throws ServiceException;

    /**
     * This method returns news message page starts with start parameter according to search criteria.
     * Can produce <code>ServiceException</code>.
     *
     * @param start
     * @param searchCriteria
     * @param end
     * @throws ServiceException
     */
    List<NewsMessage> getNewsMessagePage(SearchCriteria searchCriteria, int start, int end) throws ServiceException;

    /**
     * This method returns single news that is situated after the current in the list of news filtered by the search criteria.
     * Can produce <code>ServiceException</code>.
     *
     * @param searchCriteria
     * @param currentNewsMessageId
     * @return
     * @throws ServiceException
     */
    NewsMessage getNextNewsMessage(SearchCriteria searchCriteria, Long currentNewsMessageId) throws ServiceException;

    /**
     * This method returns single news that is situated before the current in the list of news filtered by the search criteria.
     * Can produce <code>ServiceException</code>.
     *
     * @param searchCriteria
     * @param currentNewsMessageId
     * @return
     * @throws ServiceException
     */
    NewsMessage getPrevNewsMessage(SearchCriteria searchCriteria, Long currentNewsMessageId) throws ServiceException;
}
