package com.epam.portal.dao.jdbc.oracle;

import com.epam.portal.dao.AbstractDAO;
import com.epam.portal.dao.RoleDAO;
import com.epam.portal.dao.exception.DAOException;
import com.epam.portal.model.Role;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RoleDAOImpl extends AbstractDAO implements RoleDAO {

    /**
     * Name of the primary key column in USER_TABLE table in the database.
     */
    public static final String ID_COLUMN = "ROLE_ID";

    /**
     * Name of the user id column in USER_TABLE table in the database.
     */
    public static final String USER_ID_COLUMN = "USER_ID";

    /**
     * Name of the role name column in USER_TABLE table in the database.
     */
    public static final String ROLE_NAME_COLUMN = "ROLE_NAME";

    @Override
    public Set<Role> readRolesByUser(Long userId) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(Queries.SQL_GET_USER_ROLE_BY_ID);
            statement.setLong(1, userId);

            resultSet = statement.executeQuery();
            Set<Role> roles = new HashSet<>();
            if (resultSet.next()) {
                roles.add(makeRole(resultSet));
            }
            return roles;
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(resultSet, statement, connection);
        }
    }

    /**
     * This method build <code>User</code> object with data from database.
     *
     * @param resultSet
     * @return <code>User</code>
     * @throws SQLException
     */
    private Role makeRole(ResultSet resultSet) throws SQLException{

        Role role = new Role();
        role.setUserId(resultSet.getLong(USER_ID_COLUMN));
        role.setRoleName(resultSet.getString(ROLE_NAME_COLUMN));

        return role;
    }
}
