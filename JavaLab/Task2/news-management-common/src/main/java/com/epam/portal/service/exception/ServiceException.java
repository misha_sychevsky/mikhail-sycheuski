package com.epam.portal.service.exception;

/**
 * Service Exception.
 * This class represents exception which can be occurred in Service Layer.
 */
public class ServiceException extends Exception {

    /**
     * Service Exception message.
     */
    public static final String ERROR_MESSAGE = "Service exception occurred!";

    public ServiceException(Exception e) {

        super(ERROR_MESSAGE, e);
    }

    public ServiceException() {

        super(ERROR_MESSAGE);
    }

}
