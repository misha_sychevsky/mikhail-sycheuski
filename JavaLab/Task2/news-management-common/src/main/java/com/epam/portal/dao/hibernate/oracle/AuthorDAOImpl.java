package com.epam.portal.dao.hibernate.oracle;

import com.epam.portal.dao.AbstractDAO;
import com.epam.portal.dao.AuthorDAO;
import com.epam.portal.dao.exception.DAOException;
import com.epam.portal.model.Author;

import java.util.List;

public class AuthorDAOImpl extends AbstractDAO implements AuthorDAO {

    @Override
    public void associateNewsWithAuthor(Long newsId, Long authorId) throws DAOException, UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void deleteAssociationNewsWithAuthor(Long newsId) throws DAOException, UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void deleteAllAssociationsNewsWithAuthor(Long authorId) throws DAOException, UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setAuthorExpired(Long authorId) throws DAOException {

    }

    @Override
    public Author getAuthorByNews(Long newsId) throws DAOException {
        return null;
    }

    @Override
    public Long insert(Author author) throws DAOException {
        this.sessionFactory.openSession();

        return null;
    }

    @Override
    public Author get(Long entityID) throws DAOException {
        return null;
    }

    @Override
    public List<Author> getAll() throws DAOException {
        return null;
    }

    @Override
    public void update(Author author) throws DAOException {

    }

    @Override
    public void delete(Author author) throws DAOException {

    }
}
