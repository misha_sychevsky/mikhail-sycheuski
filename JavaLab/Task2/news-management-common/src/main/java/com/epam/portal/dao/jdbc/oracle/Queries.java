package com.epam.portal.dao.jdbc.oracle;

/**
 * SQL Queries necessary to work with database.
 * This final class contains queries for all of the DAO.
 */
final class Queries {

    /*
    AUTHOR DAO QUERIES -------------------------------------------------------------------------------------------------
     */
    public static final String SQL_GET_ALL_AUTHORS = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHOR";
    public static final String SQL_INSERT_AUTHOR = "INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (AUTHOR_SEQ.NEXTVAL, ?)";
    public static final String SQL_DELETE_AUTHOR = "DELETE FROM AUTHOR WHERE AUTHOR_ID = ?";
    public static final String SQL_UPDATE_AUTHOR = "UPDATE AUTHOR SET AUTHOR_NAME = ? WHERE AUTHOR_ID = ?";
    public static final String SQL_GET_AUTHOR_BY_NEWS = "SELECT AUTHOR.AUTHOR_ID, AUTHOR.AUTHOR_NAME, AUTHOR.EXPIRED FROM AUTHOR JOIN NEWS_AUTHOR ON AUTHOR.AUTHOR_ID = NEWS_AUTHOR.AUTHOR_ID WHERE NEWS_ID = ?";
    public static final String SQL_GET_AUTHOR_BY_ID = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHOR WHERE AUTHOR_ID = ?";
    public static final String SQL_INSERT_NEWS_AUTHOR = "INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (?, ?)";
    public static final String SQL_DELETE_NEWS_AUTHOR = "DELETE FROM NEWS_AUTHOR WHERE NEWS_ID = ?";
    public static final String SQL_DELETE_ALL_NEWS_AUTHOR = "DELETE FROM NEWS_AUTHOR WHERE AUTHOR_ID = ?";
    public static final String SQL_SET_AUTHOR_EXPIRED = "UPDATE AUTHOR SET EXPIRED = ? WHERE AUTHOR_ID = ?";
    /*
    COMMENT DAO QUERIES ------------------------------------------------------------------------------------------------
     */
    public static final String SQL_GET_ALL_COMMENTS = "SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE FROM COMMENTS";
    public static final String SQL_INSERT_COMMENT = "INSERT INTO COMMENTS (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE) VALUES (COMMENTS_SEQ.NEXTVAL, ?, ?, ?)";
    public static final String SQL_DELETE_COMMENT = "DELETE FROM COMMENTS WHERE COMMENT_ID = ?";
    public static final String SQL_UPDATE_COMMENT = "UPDATE COMMENTS SET COMMENT_TEXT = ?, CREATION_DATE = ? WHERE COMMENT_ID = ?";
    public static final String SQL_GET_COMMENTS_BY_NEWS = "SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE FROM COMMENTS WHERE NEWS_ID = ?";
    public static final String SQL_GET_COMMENT_BY_ID = "SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE FROM COMMENTS WHERE COMMENT_ID = ?";
    public static final String SQL_DELETE_COMMENTS_BY_NEWS = "DELETE FROM COMMENTS WHERE NEWS_ID = ?";
    /*
    NEWS DAO QUERIES ---------------------------------------------------------------------------------------------------
     */
    public static final String SQL_INSERT_NEWS = "INSERT INTO NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (NEWS_SEQ.NEXTVAL, ?, ?, ?, ?, ?)";
    public static final String SQL_DELETE_NEWS = "DELETE FROM NEWS WHERE NEWS_ID = ?";
    public static final String SQL_UPDATE_NEWS = "UPDATE NEWS SET TITLE = ?, SHORT_TEXT = ?, FULL_TEXT = ?, MODIFICATION_DATE = ? WHERE NEWS_ID = ?";
    public static final String SQL_GET_NEWS_BY_ID = "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE FROM NEWS WHERE NEWS_ID = ?";
    public static final String SQL_COUNT_NEWS = "SELECT COUNT(*) FROM MIKHAIL.NEWS";
    public static final String SQL_GET_ALL_NEWS = "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE FROM NEWS";
    /*
    TAG DAO QUERIES ----------------------------------------------------------------------------------------------------
    */
    public static final String SQL_GET_ALL_TAGS = "SELECT TAG_ID, TAG_NAME FROM TAG";
    public static final String SQL_INSERT_TAG = "INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (TAG_SEQ.NEXTVAL, ?)";
    public static final String SQL_DELETE_TAG = "DELETE FROM TAG WHERE TAG_ID = ?";
    public static final String SQL_GET_TAG = "SELECT TAG_ID, TAG_NAME FROM TAG WHERE TAG_ID = ?";
    public static final String SQL_GET_TAGS_BY_NEWS = "SELECT TAG.TAG_ID, TAG.TAG_NAME FROM TAG JOIN NEWS_TAG ON TAG.TAG_ID = NEWS_TAG.TAG_ID WHERE NEWS_ID = ?";
    public static final String SQL_UPDATE_TAG = "UPDATE TAG SET TAG_NAME = ? WHERE TAG_ID = ?";
    public static final String SQL_INSERT_NEWS_TAG = "INSERT INTO NEWS_TAG (NEWS_ID, TAG_ID) VALUES (?, ?)";
    public static final String SQL_DELETE_NEWS_TAG = "DELETE FROM NEWS_TAG WHERE TAG_ID = ?";
    public static final String SQL_DELETE_NEWS_TAG_BY_NEWS = "DELETE FROM NEWS_TAG WHERE NEWS_ID = ?";
    /*
    USER DAO QUERIES ---------------------------------------------------------------------------------------------------
     */
    public static final String SQL_GET_USER_BY_LOGIN = "SELECT USER_ID, USER_NAME, LOGIN, PASSWORD FROM USER_TABLE WHERE LOGIN = ?";
    /*
    ROLE DAO QUERIES ---------------------------------------------------------------------------------------------------
     */
    public static final String SQL_GET_USER_ROLE_BY_ID = "SELECT USER_ID, ROLE_NAME FROM ROLES WHERE USER_ID = ?";
}
