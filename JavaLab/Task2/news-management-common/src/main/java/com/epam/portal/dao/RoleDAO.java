package com.epam.portal.dao;

import com.epam.portal.dao.exception.DAOException;
import com.epam.portal.model.Role;

import java.util.List;
import java.util.Set;

/**
 * Role data access object interface.
 * This interface is necessary for interaction with a table of roles in the database.
 */
public interface RoleDAO {

    /**
     * Return roles for user from database by user id value.
     *
     * @return list of entity records to read
     * @throws DAOException;
     */
    Set<Role> readRolesByUser(Long userId)throws DAOException;
}
