package com.epam.portal.model;

import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;

public class Role implements GrantedAuthority, Serializable {

    private static final long serialVersionUID = 10112015L;

    private Long userId;
    private String roleName;
    private User user;

    public Long getUserId() {

        return userId;
    }

    public void setUserId(Long userId) {

        this.userId = userId;
    }

    public String getRoleName() {

        return roleName;
    }

    public void setRoleName(String roleName) {

        this.roleName = roleName;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String getAuthority() {

        return roleName;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Role role = (Role) o;

        if (userId != null ? !userId.equals(role.userId) : role.userId != null) {
            return false;
        }
        return !(roleName != null ? !roleName.equals(role.roleName) : role.roleName != null);

    }

    @Override
    public int hashCode() {

        int result = 17;
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        result = 31 * result + (roleName != null ? roleName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {

        return "Role[" +
                "userId=" + userId +
                ", roleName=" + roleName +
                ']';
    }
}
