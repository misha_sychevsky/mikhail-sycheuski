package com.epam.portal.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Comment entity transfer object class.
 */
public class Comment implements Serializable {

    private static final long serialVersionUID = 20150831L;

    private Long commentId;
    private Long newsId;
    private String commentText;
    private Date creationDate;
    private News news;

    public Long getCommentId() {

        return commentId;
    }

    public void setCommentId(Long commentId) {

        this.commentId = commentId;
    }

    public Long getNewsId() {

        return newsId;
    }

    public void setNewsId(Long newsId) {

        this.newsId = newsId;
    }

    public String getCommentText() {

        return commentText;
    }

    public void setCommentText(String commentText) {

        this.commentText = commentText;
    }

    public Date getCreationDate() {

        return creationDate;
    }

    public void setCreationDate(Date creationDate) {

        this.creationDate = creationDate;
    }

    public News getNews() {

        return news;
    }

    public void setNews(News news) {

        this.news = news;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Comment comment = (Comment) o;

        if (commentId != null ? !commentId.equals(comment.commentId) : comment.commentId != null) {
            return false;
        }
        if (newsId != null ? !newsId.equals(comment.newsId) : comment.newsId != null) {
            return false;
        }
        if (commentText != null ? !commentText.equals(comment.commentText) : comment.commentText != null) {
            return false;
        }
        return !(creationDate != null ? !creationDate.equals(comment.creationDate) : comment.creationDate != null);

    }

    @Override
    public int hashCode() {

        int result = 17;
        result = 31 * result + (commentId != null ? commentId.hashCode() : 0);
        result = 31 * result + (newsId != null ? newsId.hashCode() : 0);
        result = 31 * result + (commentText != null ? commentText.hashCode() : 0);
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {

        return "Comment[" +
                "commentId=" + commentId +
                ", newsId=" + newsId +
                ", commentText=" + commentText +
                ", creationDate=" + creationDate +
                ']';
    }
}
