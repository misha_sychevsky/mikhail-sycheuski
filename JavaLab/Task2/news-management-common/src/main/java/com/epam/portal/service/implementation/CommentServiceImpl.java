package com.epam.portal.service.implementation;

import com.epam.portal.dao.CommentDAO;
import com.epam.portal.dao.exception.DAOException;
import com.epam.portal.model.Comment;
import com.epam.portal.service.CommentService;
import com.epam.portal.service.exception.ServiceException;

import java.util.List;

/**
 * Comment service.
 * This is an implementation of the comment service interface.
 */
public class CommentServiceImpl implements CommentService {

    /**
     * Date access object of the comment entity.
     */
    private CommentDAO commentDAO;

    public void setCommentDAO(CommentDAO commentDAO) {

        this.commentDAO = commentDAO;
    }

    @Override
    public Comment getCommentById(Long commentId) throws ServiceException {

        try {
            return commentDAO.get(commentId);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Long addComment(Comment comment) throws ServiceException {

        try {
            return commentDAO.insert(comment);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void addComments(List<Comment> comments) throws ServiceException {

        try {
            commentDAO.insertAll(comments);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void updateComment(Comment comment) throws ServiceException {

        try {
            commentDAO.update(comment);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteComment(Comment comment) throws ServiceException {

        try {
            commentDAO.delete(comment);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Comment> viewTheListOfComments() throws ServiceException {

        try {
            return commentDAO.getAll();
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Comment> getCommentsByNews(Long newsId) throws ServiceException {

        try {
            return commentDAO.getCommentsByNews(newsId);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
