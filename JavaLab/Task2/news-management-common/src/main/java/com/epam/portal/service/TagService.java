package com.epam.portal.service;

import com.epam.portal.model.Tag;
import com.epam.portal.service.exception.ServiceException;

import java.util.List;

/**
 * Tag service interface.
 * This interface represents business operations for tag entity.
 */
public interface TagService {

    /**
     * This method adds a new tag.
     * Can produce <code>ServiceException</code>.
     *
     * @param tag
     * @return <code>Tag</code> id
     * @throws ServiceException
     */
    Long addTag(Tag tag) throws ServiceException;

    /**
     * This method deletes tag.
     * Can produce <code>ServiceException</code>.
     *
     * @param tag
     * @throws ServiceException
     */
    void deleteTag(Tag tag) throws ServiceException;

    /**
     * This method returns all tags.
     * Can produce <code>ServiceException</code>.
     *
     * @return <code>List</code> of tags
     * @throws ServiceException
     */
    List<Tag> viewTheListOfTags() throws ServiceException;

    /**
     * This method returns all tags of the news.
     * Can produce <code>ServiceException</code>.
     *
     * @param newsId
     * @return <code>List</code> of tags
     * @throws ServiceException
     */
    List<Tag> getTagsByNews(Long newsId) throws ServiceException;

    /**
     * This method returns tag by id.
     * Can produce <code>ServiceException</code>.
     *
     * @param tagId
     * @return <code>tag</code>
     * @throws ServiceException
     */
    Tag getTagById(Long tagId) throws ServiceException;

    /**
     * This method edits tag information.
     * Can produce <code>ServiceException</code>.
     *
     * @param tag
     * @throws ServiceException
     */
    void updateTag(Tag tag) throws ServiceException;
}
