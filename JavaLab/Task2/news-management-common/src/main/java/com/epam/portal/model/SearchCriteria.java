package com.epam.portal.model;

import java.io.Serializable;
import java.util.List;

/**
 * Search criteria class.
 * This class is necessary to search news by author and tags.
 */
public class SearchCriteria implements Serializable {

    private static final long serialVersionUID = 20150831L;

    private Long authorId;
    private List<Long> tagIds;

    public SearchCriteria() {

    }

    public SearchCriteria(Long authorId, List<Long> tagIds) {

        this.authorId = authorId;
        this.tagIds = tagIds;
    }

    public Long getAuthorId() {

        return authorId;
    }

    public void setAuthorId(Long authorId) {

        this.authorId = authorId;
    }

    public List<Long> getTagIds() {

        return tagIds;
    }

    public void setTagIds(List<Long> tagIds) {

        this.tagIds = tagIds;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SearchCriteria that = (SearchCriteria) o;

        if (authorId != null ? !authorId.equals(that.authorId) : that.authorId != null) {
            return false;
        }
        return !(tagIds != null ? !tagIds.equals(that.tagIds) : that.tagIds != null);

    }

    @Override
    public int hashCode() {

        int result = 17;
        result = 31 * result + (authorId != null ? authorId.hashCode() : 0);
        result = 31 * result + (tagIds != null ? tagIds.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {

        return "SearchCriteria[" +
                "authorId=" + authorId +
                ", tagIds=" + tagIds +
                ']';
    }
}
