package com.epam.portal.dao;

import com.epam.portal.dao.exception.DAOException;
import com.epam.portal.model.User;

/**
 * User data access object interface.
 * This interface is necessary for interaction with a table of users in the database.
 */
public interface UserDAO {

    /**
     * This method returns <code>User</code> entity which represents the user with login in the parameter.
     * Can produce <code>DAOException</code>.
     *
     * @param login
     * @throws DAOException
     */
    User loadUserByUsername(String login) throws DAOException;
}
