package com.epam.portal.service;

import com.epam.portal.model.News;
import com.epam.portal.model.SearchCriteria;
import com.epam.portal.model.Tag;
import com.epam.portal.service.exception.ServiceException;

import java.util.List;

/**
 * News service interface.
 * This interface represents business operations for news entity.
 */
public interface NewsService {

    /**
     * This method adds new news.
     * Can produce <code>ServiceException</code>.
     *
     * @param news
     * @return <code>News</code> id
     * @throws ServiceException
     */
    Long addNews(News news) throws ServiceException;

    /**
     * This method edits news information.
     * Can produce <code>ServiceException</code>.
     *
     * @param news
     * @throws ServiceException
     */
    void editNews(News news) throws ServiceException;

    /**
     * This method deletes news.
     * Can produce <code>ServiceException</code>.
     *
     * @param news
     * @throws ServiceException
     */
    void deleteNews(News news) throws ServiceException;

    /**
     * This method returns news by id.
     * Can produce <code>ServiceException</code>.
     *
     * @param newsId
     * @return <code>News</code>
     * @throws ServiceException
     */
    News getNewsById(Long newsId) throws ServiceException;

    /**
     * This method returns the number of all news.
     * Can produce <code>ServiceException</code>.
     *
     * @return the number of news
     * @throws ServiceException
     */
    int countAllNews() throws ServiceException;

    /**
     * This method returns news page according to search criteria.
     * Can produce <code>ServiceException</code>.
     *
     * @param start
     * @param end
     * @param searchCriteria
     * @return <code>List</code> of news
     * @throws ServiceException
     */
    List<News> searchNewsBySearchCriteria(SearchCriteria searchCriteria, int start, int end) throws ServiceException;

    /**
     * This method returns single news that is situated after the current in the list of news filtered by the search criteria.
     * Can produce <code>ServiceException</code>.
     *
     * @param searchCriteria
     * @param currentNewsMessageId
     * @return
     * @throws ServiceException
     */
    News getNextNews(SearchCriteria searchCriteria, Long currentNewsMessageId) throws ServiceException;

    /**
     * This method returns single news that is situated before the current in the list of news filtered by the search criteria.
     * Can produce <code>ServiceException</code>.
     *
     * @param searchCriteria
     * @param currentNewsMessageId
     * @return
     * @throws ServiceException
     */
    News getPrevNews(SearchCriteria searchCriteria, Long currentNewsMessageId) throws ServiceException;

    /**
     * This method adds tag to news.
     * Can produce <code>ServiceException</code>.
     *
     * @param tagId
     * @param newsId
     * @throws ServiceException
     */
    void addTagToNews(Long tagId, Long newsId) throws ServiceException;

    /**
     * This method adds more than one tag to news.
     * Can produce <code>ServiceException</code>.
     *
     * @param tags
     * @param newsId
     * @throws ServiceException
     */
    void addTagsToNews(List<Tag> tags, Long newsId) throws ServiceException;

    /**
     * This method deletes association of the author with news.
     * Can produce <code>ServiceException</code>.
     *
     * @param authorId
     * @throws ServiceException
     */
    void deleteAuthorFromAllNews(Long authorId) throws ServiceException;

    /**
     * This method adds author to news.
     * Can produce <code>ServiceException</code>.
     *
     * @param newsId
     * @param authorId
     * @throws ServiceException
     */
    void addAuthorToNews(Long newsId, Long authorId) throws ServiceException;

    /**
     * This method deletes author from news.
     * Can produce <code>ServiceException</code>.
     *
     * @param newsId
     * @throws ServiceException
     */
    void deleteAuthorFromNews(Long newsId) throws ServiceException;

    /**
     * This method deletes all tags from news.
     * Can produce <code>ServiceException</code>.
     *
     * @param newsId
     * @throws ServiceException
     */
    void deleteTagsFromNews(Long newsId) throws ServiceException;
}
