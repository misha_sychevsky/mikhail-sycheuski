package com.epam.portal.service.implementation;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.epam.portal.dao.AuthorDAO;
import com.epam.portal.dao.CommentDAO;
import com.epam.portal.dao.NewsDAO;
import com.epam.portal.dao.TagDAO;
import com.epam.portal.dao.exception.DAOException;
import com.epam.portal.model.News;
import com.epam.portal.model.SearchCriteria;
import com.epam.portal.model.Tag;
import com.epam.portal.service.NewsService;
import com.epam.portal.service.exception.ServiceException;

import java.util.List;

/**
 * News service.
 * This is an implementation of the news service interface.
 */
public class NewsServiceImpl implements NewsService {

    /**
     * Date access object of the news entity.
     */
    private NewsDAO newsDAO;
    /**
     * Date access object of the tag entity.
     */
    private TagDAO tagDAO;
    /**
     * Date access object of the author entity.
     */
    private AuthorDAO authorDAO;
    /**
     * Date access object of the comment entity.
     */
    private CommentDAO commentDAO;

    public void setCommentDAO(CommentDAO commentDAO) {

        this.commentDAO = commentDAO;
    }

    public void setTagDAO(TagDAO tagDAO) {

        this.tagDAO = tagDAO;
    }

    public void setAuthorDAO(AuthorDAO authorDAO) {

        this.authorDAO = authorDAO;
    }

    public void setNewsDAO(NewsDAO newsDAO) {

        this.newsDAO = newsDAO;
    }

    @Override
    public Long addNews(News news) throws ServiceException {

        try {
            return newsDAO.insert(news);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void editNews(News news) throws ServiceException {

        try {
            newsDAO.update(news);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    @Transactional(rollbackFor = ServiceException.class, propagation = Propagation.REQUIRED)
    public void deleteNews(News news) throws ServiceException {

        try {
            authorDAO.deleteAssociationNewsWithAuthor(news.getNewsId());
            tagDAO.deleteAssociationsNewsWithTagsByNews(news.getNewsId());
            commentDAO.deleteCommentsByNews(news.getNewsId());
            newsDAO.delete(news);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public int countAllNews() throws ServiceException {

        try {
            return newsDAO.countNews();
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public News getNextNews(SearchCriteria searchCriteria, Long currentNewsMessageId) throws ServiceException {

        try {
            return newsDAO.getNextNews(searchCriteria, currentNewsMessageId);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public News getPrevNews(SearchCriteria searchCriteria, Long currentNewsMessageId) throws ServiceException {

        try {
            return newsDAO.getPrevNews(searchCriteria, currentNewsMessageId);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<News> searchNewsBySearchCriteria(SearchCriteria searchCriteria, int start, int end) throws ServiceException {

        try {
            return newsDAO.getNewsBySearchCriteria(searchCriteria, start, end);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public News getNewsById(Long newsId) throws ServiceException {

        try {
            return newsDAO.get(newsId);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void addTagToNews(Long tagId, Long newsId) throws ServiceException {

        try {
            tagDAO.associateNewsWithTag(tagId, newsId);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void addTagsToNews(List<Tag> tags, Long newsId) throws ServiceException {

        try {
            tagDAO.associateNewsWithTags(tags, newsId);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteTagsFromNews(Long newsId) throws ServiceException {

        try {
            tagDAO.deleteAssociationsNewsWithTagsByNews(newsId);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteAuthorFromAllNews(Long authorId) throws ServiceException {

        try {
            authorDAO.deleteAllAssociationsNewsWithAuthor(authorId);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void addAuthorToNews(Long newsId, Long authorId) throws ServiceException {

        try {
            authorDAO.associateNewsWithAuthor(newsId, authorId);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void deleteAuthorFromNews(Long newsId) throws ServiceException {

        try {
            authorDAO.deleteAssociationNewsWithAuthor(newsId);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
