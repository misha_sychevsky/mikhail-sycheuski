package com.epam.portal.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * Author entity transfer object class.
 */
public class Author implements Serializable {

    private static final long serialVersionUID = 20150831L;

    private Long authorId;
    private String name;
    private Date expired;
    private Set<News> newsSet;

    public Long getAuthorId() {

        return authorId;
    }

    public void setAuthorId(Long authorId) {

        this.authorId = authorId;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public Date getExpired() {

        return expired;
    }

    public void setExpired(Date expired) {

        this.expired = expired;
    }

    public Set<News> getNewsSet() {

        return newsSet;
    }

    public void setNewsSet(Set<News> newsSet) {

        this.newsSet = newsSet;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Author author = (Author) o;

        if (authorId != null ? !authorId.equals(author.authorId) : author.authorId != null) {
            return false;
        }
        if (name != null ? !name.equals(author.name) : author.name != null) {
            return false;
        }
        return !(expired != null ? !expired.equals(author.expired) : author.expired != null);

    }

    @Override
    public int hashCode() {

        int result = 17;
        result = 31 * result + (authorId != null ? authorId.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (expired != null ? expired.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {

        return "Author[" +
                "id=" + authorId +
                ", name=" + name +
                ", expired=" + expired +
                ']';
    }
}
