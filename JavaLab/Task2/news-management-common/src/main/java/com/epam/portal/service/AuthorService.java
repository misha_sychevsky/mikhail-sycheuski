package com.epam.portal.service;

import com.epam.portal.model.Author;
import com.epam.portal.service.exception.ServiceException;

import java.util.List;

/**
 * Author service interface.
 * This interface represents business operations for author entity.
 */
public interface AuthorService {

    /**
     * This method adds a new author.
     * Can produce <code>ServiceException</code>.
     *
     * @param author
     * @return <code>Author</code> id
     * @throws ServiceException
     */
    Long addAuthor(Author author) throws ServiceException;

    /**
     * This method edits information about the author.
     * Can produce <code>ServiceException</code>.
     *
     * @param author
     * @throws ServiceException
     */
    void editAuthor(Author author) throws ServiceException;

    /**
     * This method deletes author.
     * Can produce <code>ServiceException</code>.
     *
     * @param author
     * @throws ServiceException
     */
    void deleteAuthor(Author author) throws ServiceException;

    /**
     * This method returns all existing authors.
     * Can produce <code>ServiceException</code>.
     *
     * @return <code>List</code> of authors
     * @throws ServiceException
     */
    List<Author> viewTheListOfAuthors() throws ServiceException;

    /**
     * This method returns the author of the news in parameter.
     * Can produce <code>ServiceException</code>.
     *
     * @param newsId
     * @return <code>Author</code>
     * @throws ServiceException
     */
    Author getAuthorByNews(Long newsId) throws ServiceException;

    /**
     * This method makes author expired.
     * Can produce <code>ServiceException</code>.
     *
     * @param authorId
     * @throws ServiceException
     */
    void setAuthorExpired(Long authorId) throws ServiceException;

    /**
     * This method returns author by id.
     * Can produce <code>ServiceException</code>.
     *
     * @param authorId
     * @return
     * @throws ServiceException
     */
    Author getAuthorById(Long authorId) throws ServiceException;
}
