package com.epam.portal.dao.jdbc.oracle;

import com.epam.portal.dao.AbstractDAO;
import com.epam.portal.dao.UserDAO;
import com.epam.portal.dao.exception.DAOException;
import com.epam.portal.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Oracle implementation of the DAO interface for user entity.
 */
public class UserDAOImpl extends AbstractDAO implements UserDAO {

    /**
     * Name of the primary key column in USER_TABLE table in the database.
     */
    public static final String ID_COLUMN = "USER_ID";

    /**
     * Name of the user name column in USER_TABLE table in the database.
     */
    public static final String USER_NAME_COLUMN = "USER_NAME";

    /**
     * Name of the user login column in USER_TABLE table in the database.
     */
    public static final String USER_LOGIN_COLUMN = "LOGIN";

    /**
     * Name of the user password column in USER_TABLE table in the database.
     */
    public static final String USER_PASSWORD_COLUMN = "PASSWORD";

    public User loadUserByUsername(String login) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(Queries.SQL_GET_USER_BY_LOGIN);
            statement.setString(1, login);

            resultSet = statement.executeQuery();
            User user = null;
            if (resultSet.next()) {
                user = makeUser(resultSet);
            }
            return user;
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(resultSet, statement, connection);
        }
    }

    /**
     * This method build <code>User</code> object with data from database.
     *
     * @param resultSet
     * @return <code>User</code>
     * @throws SQLException
     */
    private User makeUser(ResultSet resultSet) throws SQLException{

        User user = new User();
        user.setUserId(resultSet.getLong(ID_COLUMN));
        user.setUserName(resultSet.getString(USER_NAME_COLUMN));
        user.setLogin(resultSet.getString(USER_LOGIN_COLUMN));
        user.setPassword(resultSet.getString(USER_PASSWORD_COLUMN));

        return user;
    }
}
