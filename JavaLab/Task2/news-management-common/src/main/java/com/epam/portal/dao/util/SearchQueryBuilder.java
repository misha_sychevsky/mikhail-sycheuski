package com.epam.portal.dao.util;

import com.epam.portal.model.SearchCriteria;

/**
 * Search query builder class.
 * This class builds query using <code>SearchCriteria</code> object.
 */
public class SearchQueryBuilder {

    /**
     * The next news constant string
     */
    public static final String NEXT_NEWS = " NEXT_NEWS ";
    /**
     * The previous news constant string
     */
    public static final String PREV_NEWS = " PREV_NEWS ";
    /**
     * Part of the query with PlSQL LAG function. It is necessary to get id of the previous news.
     */
    public static final String LAG_PART = ", LAG(NEWS_ID,1) OVER (ORDER BY RNUM) AS ";
    /**
     * Part of the query with PlSQL LEAD function. It is necessary to get id of the next news.
     */
    public static final String LEAD_PART = ", LEAD (NEWS_ID,1) OVER (ORDER BY RNUM) AS ";
    /**
     * This constant string represents the fields that we are selecting from the table.
     */
    public static final String SELECTOR = " NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE ";
    /**
     * This constant string represents the fields that we are selecting from the table in the main query.
     */
    public static final String MAIN_SELECTOR = " DISTINCT NEWS.NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE ";
    /**
     * Join with NEWS_TAG table.
     */
    public static final String JOIN_NEWS_TAG = " LEFT OUTER JOIN NEWS_TAG ON NEWS.NEWS_ID = NEWS_TAG.NEWS_ID ";
    /**
     * Join with NEWS_AUTHOR table.
     */
    public static final String JOIN_NEWS_AUTHOR = " LEFT OUTER JOIN NEWS_AUTHOR ON NEWS.NEWS_ID = NEWS_AUTHOR.NEWS_ID ";
    /**
     * Join with COMMENTS table.
     */
    public static final String JOIN_COMMENTS = " LEFT OUTER JOIN (SELECT NEWS_ID AS N_ID FROM COMMENTS GROUP BY NEWS_ID ORDER BY COUNT(*) DESC) ON NEWS.NEWS_ID = N_ID";
    /**
     * Part of the SQL query with order clause.
     */
    public static final String ORDER_CLAUSE = " ORDER BY MODIFICATION_DATE DESC";
    /**
     * This is the string with the news table's name.
     */
    public static final String  NEWS_TABLE_NAME = " NEWS ";
    /**
     * Declaration of the ROWNUM field.
     */
    public static final String ROWNUM_DECLARATION = ", ROWNUM RNUM";
    /**
     * Left par.
     */
    public static final String L_PAR = "(";
    /**
     * Right par.
     */
    public static final String R_PAR = ")";
    /**
     * The select keyword.
     */
    public static final String SELECT = " SELECT ";
    /**
     * The from keyword.
     */
    public static final String FROM = " FROM ";
    /**
     * Where clause.
     */
    public static final String WHERE = " WHERE ";
    /**
     * AUTHOR_ID clause.
     */
    public static final String AUTHOR_ID_CLAUSE = "AUTHOR_ID = ";
    /**
     * OR clause.
     */
    public static final String OR = " OR ";
    /**
     * Parameter to set id of current news in prepared statement.
     */
    public static final String PARAM = " ? ";
    /**
     * The clause for news id  parameter.
     */
    public static final String NEWS_ID_CLAUSE = "NEWS_ID = ";
    /**
     * The clause for tag id parameter.
     */
    public static final String TAG_ID_CLAUSE = "TAG_ID = ";
    /**
     * This clause is necessary to set the upper border of news in page.
     */
    public static final String PAGINATION_UPPER_BORDER = " WHERE ROWNUM < ?)";
    /**
     * This clause is necessary to set the lower border of news in page.
     */
    public static final String PAGINATION_LOWER_BORDER = " WHERE RNUM >= ?";

    /**
     * This method builds the search query to find the previous news after current in the list of news that are suitable for the search criteria parameters.
     *
     * @param searchCriteria
     * @return the <code>String</code> with built query.
     */
    public String buildPrevNewsQuery(SearchCriteria searchCriteria) {

        StringBuilder query = new StringBuilder(SELECT);
        query.append(SELECTOR)
                .append(FROM)
                .append(NEWS_TABLE_NAME)
                .append(WHERE)
                .append(NEWS_ID_CLAUSE)
                .append(L_PAR)
                .append(SELECT)
                .append(PREV_NEWS)
                .append(FROM)
                .append(L_PAR)
                .append(SELECT)
                .append(SELECTOR)
                .append(LAG_PART)
                .append(PREV_NEWS)
                .append(FROM)
                .append(L_PAR)
                .append(buildBaseQueryPart(searchCriteria))
                .append(R_PAR)
                .append(R_PAR)
                .append(WHERE)
                .append(NEWS_ID_CLAUSE)
                .append(PARAM)
                .append(R_PAR);

        return query.toString();
    }

    /**
     * This method builds the search query to find the next news after current in the list of news that are suitable for the search criteria parameters.
     *
     * @param searchCriteria
     * @return the <code>String</code> with built query.
     */
    public String buildNextNewsQuery(SearchCriteria searchCriteria) {

        StringBuilder query = new StringBuilder(SELECT);
        query.append(SELECTOR)
                .append(FROM)
                .append(NEWS_TABLE_NAME)
                .append(WHERE)
                .append(NEWS_ID_CLAUSE)
                .append(L_PAR)
                .append(SELECT)
                .append(NEXT_NEWS)
                .append(FROM)
                .append(L_PAR)
                .append(SELECT)
                .append(SELECTOR)
                .append(LEAD_PART)
                .append(NEXT_NEWS)
                .append(FROM)
                .append(L_PAR)
                .append(buildBaseQueryPart(searchCriteria))
                .append(R_PAR)
                .append(R_PAR)
                .append(WHERE)
                .append(NEWS_ID_CLAUSE)
                .append(PARAM)
                .append(R_PAR);

        return query.toString();
    }

    /**
     * This method builds the search query to find the page of news that are suitable for the search criteria parameters.
     * Also it always sorts news by modification date and comment's count.
     *
     * @param searchCriteria
     * @return <code>String</code> with ready SQL search query.
     */
    public String buildSearchQuery(SearchCriteria searchCriteria) {

        StringBuilder query = new StringBuilder(SELECT);
        query.append(SELECTOR)
                .append(FROM)
                .append(L_PAR)
                .append(buildBaseQueryPart(searchCriteria))
                .append(PAGINATION_UPPER_BORDER)
                .append(PAGINATION_LOWER_BORDER);

        return query.toString();
    }
    /**
     * This is the base part for all queries this class can build;
     * This method checks the <code>SearchCriteria</code> and builds SQL query using it.
     * Check author and tags parameters and always sorts news by modification date and comment's count.
     *
     * @param searchCriteria
     * @return <code>String</code> with ready base SQL query part.
     */
    private String buildBaseQueryPart(SearchCriteria searchCriteria) {

        StringBuilder query = new StringBuilder(SELECT);
        query.append(SELECTOR)
                .append(ROWNUM_DECLARATION)
                .append(FROM)
                .append(L_PAR)
                .append(SELECT)
                .append(MAIN_SELECTOR)
                .append(FROM)
                .append(NEWS_TABLE_NAME);

        if (checkSearchCriteria(searchCriteria)) {
            query.append(JOIN_COMMENTS).append(ORDER_CLAUSE).append(R_PAR);

        } else {
            boolean checkAuthor = false;
            if (searchCriteria.getAuthorId() != null) {
                query.append(JOIN_NEWS_AUTHOR);
                checkAuthor = true;
            }
            boolean checkTags = false;
            if (searchCriteria.getTagIds() != null && !searchCriteria.getTagIds().isEmpty()) {
                query.append(JOIN_NEWS_TAG);
                checkTags = true;
            }

            query.append(JOIN_COMMENTS);

            if(checkAuthor || checkTags) {
                query.append(WHERE);

                if (checkAuthor) {
                    query.append(AUTHOR_ID_CLAUSE).append(searchCriteria.getAuthorId()).append(OR);
                }
                if (checkTags) {
                    for (Long tagId : searchCriteria.getTagIds()) {
                        query.append(TAG_ID_CLAUSE).append(tagId).append(OR);
                    }
                }

                query.delete(query.length() - 3, query.length());
            }

            query.append(ORDER_CLAUSE).append(R_PAR);
        }

        return query.toString();
    }

    /**
     * This method checks the <code>SearchCriteria</code> and it's parameters on null.
     *
     * @param searchCriteria
     * @return <code>true</code> if the <code>SearchCriteria</code> is empty and <code>false</code> if it isn't.
     */
    private boolean checkSearchCriteria(SearchCriteria searchCriteria) {

        boolean result = true;
        if (searchCriteria != null) {
            result = searchCriteria.getAuthorId() == null && searchCriteria.getTagIds() == null;
        }
        return result;
    }
}
