package com.epam.portal.dao.jdbc.oracle;

import com.epam.portal.dao.AbstractDAO;
import com.epam.portal.dao.NewsDAO;
import com.epam.portal.dao.exception.DAOException;
import com.epam.portal.dao.util.SearchQueryBuilder;
import com.epam.portal.model.News;
import com.epam.portal.model.SearchCriteria;

import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

/**
 * Oracle implementation of the DAO interface for news entity.
 */
public class NewsDAOImpl extends AbstractDAO implements NewsDAO {

    /**
     * Name of the primary key column in NEWS table in the database.
     */
    public static final String ID_COLUMN = "NEWS_ID";

    /**
     * Name of the news title column in NEWS table in the database.
     */
    public static final String TITLE_COLUMN = "TITLE";

    /**
     * Name of the short news text column in NEWS table in the database.
     */
    public static final String SHORT_TEXT_COLUMN = "SHORT_TEXT";

    /**
     * Name of the full news text column in NEWS table in the database.
     */
    public static final String FULL_TEXT_COLUMN = "FULL_TEXT";

    /**
     * Name of the news creation date column in NEWS table in the database.
     */
    public static final String CREATION_DATE_COLUMN = "CREATION_DATE";

    /**
     * Name of the news modification date column in NEWS table in the database.
     */
    public static final String MODIFICATION_DATE_COLUMN = "MODIFICATION_DATE";

    /**
     * Default timezone
     */
    public static final String TIME_ZONE_GMT = "GMT";

    public Long insert(News news) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet generatedKeys = null;
        String generatedColumns[] = {ID_COLUMN};

        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(Queries.SQL_INSERT_NEWS, generatedColumns);
            statement.setString(1, news.getTitle());
            statement.setString(2, news.getShortText());
            statement.setString(3, news.getFullText());
            statement.setTimestamp(4, new Timestamp(news.getCreationDate().getTime()), Calendar.getInstance(
                    TimeZone.getTimeZone(TIME_ZONE_GMT)));
            statement.setDate(5, new Date(news.getModificationDate().getTime()), Calendar.getInstance(
                    TimeZone.getTimeZone(TIME_ZONE_GMT)));
            statement.executeUpdate();

            long id = 0;
            generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                id = generatedKeys.getLong(1);
            }
            return id;
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(generatedKeys, statement, connection);
        }
    }

    public void delete(News news) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(Queries.SQL_DELETE_NEWS);
            statement.setLong(1, news.getNewsId());
            statement.executeUpdate();
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(statement, connection);
        }
    }

    public void update(News news) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(Queries.SQL_UPDATE_NEWS);
            statement.setString(1, news.getTitle());
            statement.setString(2, news.getShortText());
            statement.setString(3, news.getFullText());
            statement.setDate(4, new Date(news.getModificationDate().getTime()), Calendar.getInstance(
                    TimeZone.getTimeZone(TIME_ZONE_GMT)));
            statement.setLong(5, news.getNewsId());
            statement.executeUpdate();
        }
        catch (SQLException e)
        {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(statement, connection);
        }
    }

    @Override
    public int countNews() throws DAOException {

        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = manager.getConnection();

            statement = connection.createStatement();

            resultSet = statement.executeQuery(Queries.SQL_COUNT_NEWS);
            int count = 0;
            while (resultSet.next()) {
                count = resultSet.getInt(1);
            }
            return count;
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(resultSet, statement, connection);
        }
    }

    @Override
    public News get(Long newsId) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(Queries.SQL_GET_NEWS_BY_ID);
            statement.setLong(1, newsId);

            resultSet = statement.executeQuery();
            News news = null;
            while (resultSet.next()) {
                news = (makeNews(resultSet));
            }
            return news;
        }
        catch (SQLException e)
        {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(resultSet, statement, connection);
        }
    }

    @Override
    public List<News> getNewsBySearchCriteria(SearchCriteria searchCriteria, int start, int end) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        SearchQueryBuilder searchQueryBuilder = new SearchQueryBuilder();
        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(searchQueryBuilder.buildSearchQuery(searchCriteria));
            statement.setInt(1, end);
            statement.setInt(2, start);

            resultSet = statement.executeQuery();
            List<News> news = new ArrayList<>();
            while (resultSet.next()) {
                news.add(makeNews(resultSet));
            }
            return news;
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(resultSet, statement, connection);
        }
    }

    @Override
    public News getNextNews(SearchCriteria searchCriteria, Long currentNewsMessageId) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        SearchQueryBuilder queryBuilder = new SearchQueryBuilder();
        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(queryBuilder.buildNextNewsQuery(searchCriteria));
            statement.setLong(1, currentNewsMessageId);

            resultSet = statement.executeQuery();
            News news = null;
            while (resultSet.next()) {
                news =  makeNews(resultSet);
            }

            return news;
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(resultSet, statement, connection);
        }
    }

    @Override
    public News getPrevNews(SearchCriteria searchCriteria, Long currentNewsMessageId) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        SearchQueryBuilder queryBuilder = new SearchQueryBuilder();
        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(queryBuilder.buildPrevNewsQuery(searchCriteria));
            statement.setLong(1, currentNewsMessageId);

            resultSet = statement.executeQuery();
            News news = null;
            while (resultSet.next()) {
                news = makeNews(resultSet);
            }

            return news;
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(resultSet, statement, connection);
        }
    }

    @Override
    public List<News> getAll() throws DAOException {

        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = manager.getConnection();

            statement = connection.createStatement();

            resultSet = statement.executeQuery(Queries.SQL_GET_ALL_NEWS);
            List<News> news = new ArrayList<>();
            while (resultSet.next()) {
                news.add(makeNews(resultSet));
            }
            return news;
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(resultSet, statement, connection);
        }
    }

    /**
     * This method build <code>News</code> object with data from database.
     *
     * @param resultSet
     * @return <code>News</code>
     * @throws SQLException
     */
    private News makeNews(ResultSet resultSet) throws SQLException {

        News news = new News();
        news.setNewsId(resultSet.getLong(ID_COLUMN));
        news.setTitle(resultSet.getString(TITLE_COLUMN));
        news.setShortText(resultSet.getString(SHORT_TEXT_COLUMN));
        news.setFullText(resultSet.getString(FULL_TEXT_COLUMN));
        news.setCreationDate(new java.util.Date(resultSet.getTimestamp(CREATION_DATE_COLUMN).getTime()));
        news.setModificationDate(new java.util.Date(resultSet.getDate(MODIFICATION_DATE_COLUMN).getTime()));
        return news;
    }
}
