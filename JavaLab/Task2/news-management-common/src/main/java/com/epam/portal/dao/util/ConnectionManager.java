package com.epam.portal.dao.util;

import org.apache.log4j.Logger;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Database connection manager.
 * This class is responsible for binding to database and realising resources.
 * Gets connection from the connection pool and returns it back after usage.
 */
public class ConnectionManager {

    public static final Logger LOGGER = Logger.getLogger(ConnectionManager.class);

    /**
     * Data source.
     * Object that contains all information about used database to work with it.
     */
    private DataSource dataSource;

    public ConnectionManager(DataSource dataSource) {

        this.dataSource = dataSource;
    }

    /**
     * This method returns connection with used database from the connection pool.
     *
     * @return <code>Connection</code> with used database
     * @throws SQLException
     */
    public synchronized Connection getConnection() throws SQLException {

        return DataSourceUtils.doGetConnection(dataSource);
    }

    /**
     * This method realises all resources and returns used connection back to the connection pool.
     *
     * @param resultSet
     * @param statement
     * @param connection
     */
    public void releaseResources(ResultSet resultSet, Statement statement, Connection connection) {

        try {
            if (resultSet != null && !resultSet.isClosed()) {
                resultSet.close();
            }
            releaseResources(statement, connection);

        } catch (SQLException e) {
            LOGGER.error(e);
        }
    }

    /**
     * This method realises all resources and returns used connection back to the connection pool.
     *
     * @param statement
     * @param connection
     */
    public void releaseResources (Statement statement, Connection connection) {

        try {
            if (statement != null && !statement.isClosed()) {
                statement.close();
            }
            if (connection != null && !connection.isClosed()) {
                DataSourceUtils.doReleaseConnection(connection, dataSource);
            }
        }
        catch (SQLException e) {
            LOGGER.error(e);
        }
    }
}
