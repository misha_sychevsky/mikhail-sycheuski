package com.epam.portal.service.implementation;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.epam.portal.dao.TagDAO;
import com.epam.portal.dao.exception.DAOException;
import com.epam.portal.model.Tag;
import com.epam.portal.service.TagService;
import com.epam.portal.service.exception.ServiceException;

import java.util.List;

/**
 * Tag service.
 * This is an implementation of the tag service interface.
 */
public class TagServiceImpl implements TagService {

    /**
     * Date access object of the tag entity.
     */
    private TagDAO tagDAO;

    public void setTagDAO(TagDAO tagDAO) {

        this.tagDAO = tagDAO;
    }

    @Override
    public Tag getTagById(Long tagId) throws ServiceException {

        try {
            return tagDAO.get(tagId);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Long addTag(Tag tag) throws ServiceException {

        try {
            return tagDAO.insert(tag);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    @Transactional(rollbackFor = ServiceException.class, propagation = Propagation.REQUIRED)
    public void deleteTag(Tag tag) throws ServiceException {

        try {
            tagDAO.deleteAssociationsNewsWithTags(tag.getTagId());
            tagDAO.delete(tag);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }


    @Override
    public void updateTag(Tag tag) throws ServiceException {

        try {
            tagDAO.update(tag);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Tag> viewTheListOfTags() throws ServiceException {

        try {
            return tagDAO.getAll();
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Tag> getTagsByNews(Long newsId) throws ServiceException {

        try {
            return tagDAO.getTagsByNews(newsId);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
