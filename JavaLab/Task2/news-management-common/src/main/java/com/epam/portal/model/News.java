package com.epam.portal.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * News entity transfer object class.
 */
public class News implements Serializable {

    private static final long serialVersionUID = 20150831L;

    private Long newsId;
    private String title;
    private String shortText;
    private String fullText;
    private Date creationDate;
    private Date modificationDate;
    private Set<Author> authorSet;
    private Set<Comment> commentSet;
    private Set<Tag> tagSet;

    public Long getNewsId() {

        return newsId;
    }

    public void setNewsId(Long newsId) {

        this.newsId = newsId;
    }

    public String getTitle() {

        return title;
    }

    public void setTitle(String title) {

        this.title = title;
    }

    public String getShortText() {

        return shortText;
    }

    public void setShortText(String shortText) {

        this.shortText = shortText;
    }

    public String getFullText() {

        return fullText;
    }

    public void setFullText(String fullText) {

        this.fullText = fullText;
    }

    public Date getCreationDate() {

        return creationDate;
    }

    public void setCreationDate(Date creationDate) {

        this.creationDate = creationDate;
    }

    public Date getModificationDate() {

        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {

        this.modificationDate = modificationDate;
    }

    public Set<Author> getAuthorSet() {

        return authorSet;
    }

    public void setAuthorSet(Set<Author> authorSet) {

        this.authorSet = authorSet;
    }

    public Set<Comment> getCommentSet() {

        return commentSet;
    }

    public void setCommentSet(Set<Comment> commentSet) {

        this.commentSet = commentSet;
    }

    public Set<Tag> getTagSet() {

        return tagSet;
    }

    public void setTagSet(Set<Tag> tagSet) {

        this.tagSet = tagSet;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        News news = (News) o;

        if (newsId != null ? !newsId.equals(news.newsId) : news.newsId != null) {
            return false;
        }
        if (title != null ? !title.equals(news.title) : news.title != null) {
            return false;
        }
        if (shortText != null ? !shortText.equals(news.shortText) : news.shortText != null) {
            return false;
        }
        if (fullText != null ? !fullText.equals(news.fullText) : news.fullText != null) {
            return false;
        }
        if (creationDate != null ? !creationDate.equals(news.creationDate) : news.creationDate != null) {
            return false;
        }
        return !(modificationDate != null ? !modificationDate.equals(news.modificationDate) : news.modificationDate != null);

    }

    @Override
    public int hashCode() {

        int result = 17;
        result = 31 * result + (newsId != null ? newsId.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (shortText != null ? shortText.hashCode() : 0);
        result = 31 * result + (fullText != null ? fullText.hashCode() : 0);
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        result = 31 * result + (modificationDate != null ? modificationDate.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {

        return "News[" +
                "id=" + newsId +
                ", title=" + title +
                ", shortText=" + shortText +
                ", fullText=" + fullText +
                ", creationDate=" + creationDate +
                ", modificationDate=" + modificationDate +
                ']';
    }
}
