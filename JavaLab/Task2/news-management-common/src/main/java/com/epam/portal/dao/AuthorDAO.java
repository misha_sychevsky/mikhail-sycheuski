package com.epam.portal.dao;

import com.epam.portal.dao.exception.DAOException;
import com.epam.portal.model.Author;

/**
 * Author data access object interface.
 * This interface is necessary for interaction with a table of authors in the database.
 */
public interface AuthorDAO extends GenericDAO<Author> {

    /**
     * This method associates news with author adding a bunch in the NEWS_AUTHOR table.
     * Can produce <code>DAOException</code>.
     *
     * @param newsId
     * @param authorId
     * @throws DAOException, UnsupportedOperationException
     */
    void associateNewsWithAuthor(Long newsId, Long authorId) throws DAOException, UnsupportedOperationException;

    /**
     * This method removes the association news with author deleting a bunch in the NEWS_AUTHOR table.
     * Can produce <code>DAOException</code>.
     *
     * @param newsId
     * @throws DAOException, UnsupportedOperationException
     */
    void deleteAssociationNewsWithAuthor(Long newsId) throws DAOException, UnsupportedOperationException;

    /**
     * This method removes all associations news with author deleting a bunches in the NEWS_AUTHOR table.
     * Can produce <code>DAOException</code>.
     *
     * @param authorId
     * @throws DAOException, UnsupportedOperationException
     */
    void deleteAllAssociationsNewsWithAuthor(Long authorId) throws DAOException, UnsupportedOperationException;

    /**
     * This method set the author expired in the AUTHOR table.
     * Can produce <code>DAOException</code>.
     *
     * @param authorId
     * @throws DAOException
     */
    void setAuthorExpired(Long authorId) throws DAOException;

    /**
     * This method returns the author of the news in the parameter.
     * Can produce <code>DAOException</code>.
     *
     * @param newsId
     * @return - <code>Author</code> of the news
     * @throws DAOException
     */
    Author getAuthorByNews(Long newsId) throws DAOException;
}
