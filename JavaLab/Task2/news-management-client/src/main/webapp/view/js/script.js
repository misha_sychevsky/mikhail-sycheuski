// SELECTS -------------------------------------------------------------------------------------------------------------
$(".dropdown dt a").on('click', function () {

    $(".dropdown dd ul").slideToggle(50);
});

$(".dropdown dd ul li a").on('click', function () {

    $(".dropdown dd ul").hide();
});

function getSelectedValue(id) {

    return $("#" + id).find("dt a span.value").html();
}

$(document).bind('click', function (e) {

    var $clicked = $(e.target);
    if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
});

var tags = [];

$('.mutliSelect input[type="checkbox"]').on('click', function () {

    var title = $(this).closest('.mutliSelect').find('input[type="checkbox"]').val(),
        title = $(this).val();

    if ($(this).is(':checked')) {
        var html = '<span class="select-span-style" title="' + title + '">#' + title + '</span>';
        $('.hida').append(html);
        tags.push($(this).context.id);
    }
    else {
        $('span[title="' + title + '"]').remove();
        var ret = $(".hida");
        $('.dropdown dt a').append(ret);
        var index = tags.indexOf($(this).context.id);
        tags.splice(index, 1);
    }
});

function DropDown(el) {

    this.dd = el;
    this.placeholder = this.dd.children('p');
    this.opts = this.dd.find('ul.dropdown-select > li');
    this.selected = this.dd.find('input');
    this.val = '';
    this.index = -1;
    this.initEvents();
}
DropDown.prototype = {

    initEvents : function() {
        var obj = this;

        obj.dd.on('click', function(event){
            $(this).toggleClass('active');
            return false;
        });

        obj.opts.on('click',function(){
            var opt = $(this);
            var selected = document.getElementsByName('selected_author')[0];
            selected.value = opt.context.id;
            obj.val = opt.text();
            obj.index = opt.index();
            obj.placeholder.text('Author: ' + obj.val);
        });
    },
    getValue : function() {
        return this.val;
    },
    getIndex : function() {
        return this.index;
    }
};

$(function () {

    var dd = new DropDown($('#dd'));

    $(document).click(function () {
        $('.wrapper-dropdown-2').removeClass('active');
    });

});

$("#filter-button").click(function(event) {

    var selectedTags = document.getElementsByName('selected_tags')[0];
    var selectedAuthor = document.getElementsByName('selected_author')[0];
    if(tags.length != 0 || selectedAuthor.value != "") {
        selectedTags.value = tags.join("-");
        $("#criteria-form").submit();
    }
});

// PAGINATION NEWS -----------------------------------------------------------------------------------------------------
$("#button-prev").click(function(event) {

    var prevButton = $("#button-prev");
    var nextButton = $("#button-next");
    nextButton.removeClass("news-button-disabled");
    nextButton.addClass("active");

    if(prevButton.hasClass("active")) {
        $.ajax({
            type : "GET",
            url : "news_portal",
            dataType : "html",
            data : {
                command : "show_news_page",
                navigation : "prev"
            },
            success : function(responseText) {
                $("#news-items-container").html(responseText);
                var pageElement = $(".page-number");
                var page = Number(pageElement.html()) - 1;
                pageElement.html(page);
            },
            error: function(jqXHR, exception) {
                if (jqXHR.status == 404) {
                    prevButton.addClass("news-button-disabled");
                    prevButton.removeClass("active");

                } else if (jqXHR.status == 500) {
                    window.location.replace("news_portal/view/jsp/error/ServerErrorPage.jsp")
                }
            }
        });
    }
});

$("#button-next").click(function(event) {

    var prevButton = $("#button-prev");
    var nextButton = $("#button-next");
    prevButton.removeClass("news-button-disabled");
    prevButton.addClass("active");

    if(nextButton.hasClass("active")) {
        $.ajax({
            type : "GET",
            url : "news_portal",
            dataType : "html",
            data : {
                command : "show_news_page",
                navigation : "next"
            },
            success : function(responseText) {
                $("#news-items-container").html(responseText);
                var pageElement = $(".page-number");
                var page = Number(pageElement.html()) + 1;
                pageElement.html(page);
            },
            error: function(jqXHR, exception) {
                if (jqXHR.status == 404) {
                    nextButton.addClass("news-button-disabled");
                    nextButton.removeClass("active");

                } else if (jqXHR.status == 500) {
                    window.location.replace("news_portal/view/jsp/error/ServerErrorPage.jsp")
                }
            }
        });
    }
});

// PAGINATION NEWS_MESSAGE ---------------------------------------------------------------------------------------------
$("#prev").click(function(event) {

    var alertMessage = $(".alert-message");
    var prevButton = $("#prev");
    var nextButton = $("#next");
    nextButton.removeClass("news-message-button-disabled");
    nextButton.addClass("active");

    if(prevButton.hasClass("active")) {
        $.ajax({
            type : "GET",
            url : "news_portal",
            dataType : "html",
            data : {
                command : "show_news_message",
                navigation : "prev"
            },
            success : function(responseText) {
                $("#news-message").html(responseText);
                if(!alertMessage.hasClass("hidden")) {
                    alertMessage.addClass("hidden");
                }
            },
            error: function(jqXHR, exception) {
                if (jqXHR.status == 404) {
                    prevButton.addClass("news-message-button-disabled");
                    prevButton.removeClass("active");

                } else if (jqXHR.status == 500) {
                    window.location.replace("news_portal/view/jsp/error/ServerErrorPage.jsp")
                }
            }
        });
    }
});

$("#next").click(function(event) {

    var alertMessage = $(".alert-message");
    var prevButton = $("#prev");
    var nextButton = $("#next");
    prevButton.removeClass("news-message-button-disabled");
    prevButton.addClass("active");

    if(nextButton.hasClass("active")) {
        $.ajax({
            type : "GET",
            url : "news_portal",
            dataType : "html",
            data : {
                command : "show_news_message",
                navigation : "next"
            },
            success : function(responseText) {
                $("#news-message").html(responseText);
                if(!alertMessage.hasClass("hidden")) {
                    alertMessage.addClass("hidden");
                }
            },
            error: function(jqXHR, exception) {
                if (jqXHR.status == 404) {
                    nextButton.addClass("news-message-button-disabled");
                    nextButton.removeClass("active");

                } else if (jqXHR.status == 500) {
                    window.location.replace("news_portal/view/jsp/error/ServerErrorPage.jsp")
                }
            }
        });
    }
});

// Post comment ajax query ---------------------------------------------------------------------------------------------
$("#post-comment").click(function(event) {

    var commentArea = $("#comment-area");
    var language = $("input[name=language]").val();
    var alertMessage = $(".alert-message");
    var commentText = commentArea.val();
    var length = commentText.length;
    var date = moment().lang(language).format('DD MMM YYYY hh:mm:ss').toString();
    var timeZone = new Date().getTimezoneOffset() / 60;

    if(length != 0 && length <= 100) {
        $.ajax({
            type : "POST",
            url : "news_portal",
            dataType : "html",
            data : {
                command : "post_comment",
                comment_text : commentText,
                time_zone : timeZone
            },
            success : function(commentId) {
                var noCommentsElement = $("#no-comments-element");
                if(noCommentsElement.get(0)) {
                    noCommentsElement.addClass("hidden");
                }
                if(!alertMessage.hasClass("hidden")) {
                    alertMessage.addClass("hidden");
                }
                var newCommentHtml = '<div class="comment"><span class="date-span row">' + date +
                    '</span> <p class="text-title comment-text row">' + commentText.replace(/</g,'&lt;').replace(/>/g,'&gt;') + '</p></div>';
                $("#comments").append(newCommentHtml);
                commentArea.val("");
            },
            error: function(jqXHR) {
                if (jqXHR.status == 400) {
                    commentArea.val("");
                } else if (jqXHR.status == 500) {
                    window.location.replace("news_portal/view/jsp/error/ServerErrorPage.jsp")
                }
            }
        });
    }
    else {
        commentArea.val("");
        alertMessage.removeClass("hidden");
    }
});

// Resize textarea elements by content ---------------------------------------------------------------------------------
jQuery.each(jQuery('textarea'), function() {
    var offset = this.offsetHeight - this.clientHeight;

    var resizeTextarea = function(el) {
        jQuery(el).css('height', 'auto').css('height', el.scrollHeight + offset);
    };
    jQuery(this).on('keyup input', function() { resizeTextarea(this); });
});

// Setup current date and resize textarea elements, setup selected filters ---------------------------------------------
$(document).ready(function() {

    var selectedTags = $("input[name=selected_tags]");

    if(selectedTags[0] != undefined) {
        tags = selectedTags.val().split("-");
        selectedTags.val(selectedTags.val().replace(/-/g, ''));
        if(tags[0] == '') {
            tags.shift();
        }
    }

    if($("#criteria-form")[0] != undefined) {
        var selectedAuthorId = $("input[name=selected_author]").val();

        if(selectedAuthorId != '' || selectedTags != '') {
            var selectedAuthorName = $('#' + selectedAuthorId).find('label').text();
            var span = $(".hida");
            $('.select-title').append(' ' + selectedAuthorName);

            for(var counter = 0; counter < tags.length; counter++) {
                var tag = $('#' + tags[counter]);
                tag.attr('checked', true);
                var tagName = tag.val();
                var html = '<span class="select-span-style" title="' + tagName + '">#' + tagName + '</span>';
                span.append(html);
            }
        }
    }
});

// News message href with timezone -------------------------------------------------------------------------------------
function hrefNewsMessageWithTimeZone(id, index) {
    var current = new Date();
    document.location.href = "news_portal?command=show_selected_news_message&news_id=" + id +
        "&news_index=" + index +
        "&time_zone=" + -current.getTimezoneOffset()/60;
}
