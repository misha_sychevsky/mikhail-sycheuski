<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tag" uri="/WEB-INF/taglib.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${sessionScope.language == null ? 'en_US' : sessionScope.language}"/>
<c:if test="${!newsMessages.isEmpty()}">
    <c:forEach items="${newsMessages}" var="newsMessage" varStatus="loop">
        <div class="news-item">
          <div class="news-info-top">
            <p class="text-title news-title"><c:out value="${newsMessage.news.title}"/></p>
            <p class="text-title news-author"><c:out value="${newsMessage.author.name}"/></p>
            <p class="date-field">
              <fmt:formatDate pattern="dd MMM yyyy" value="${newsMessage.news.modificationDate}" />
            </p>
          </div>
          <p class="news-content">
              ${newsMessage.news.shortText}
          </p>
          <div class="news-info-bottom">
            <a onclick="hrefNewsMessageWithTimeZone(${newsMessage.news.newsId}, ${loop.count})" class="text-title news-signature view-href">
              <tag:get-property uri="view.news.href.view"/>
            </a>
            <p class="text-title news-signature">
              <tag:get-property uri="view.news.comments"/>
              <c:out value="${newsMessage.comments.size()}"/>
            </p>
            <p class="text-title news-signature">
              <tag:get-property uri="view.news.tags"/>
              <c:if test="${newsMessage.tags.size() != 0}">
                <c:forEach items="${newsMessage.tags}" var="tag">
                  <c:out value="#${tag.name} "/>
                </c:forEach>
              </c:if>
              <c:if test="${newsMessage.tags.size() == 0}">
                <tag:get-property uri="view.news.no.tags"/>
              </c:if>
            </p>
          </div>
        </div>
    </c:forEach>
</c:if>
<c:if test="${newsMessages.isEmpty()}">
  <div class="not-found-container">
    <img src="view/img/not_found.png" class="not_found_img">
    <p class="not_found">
      <tag:get-property uri="view.news.not.found"/>
    </p>
  </div>
</c:if>

