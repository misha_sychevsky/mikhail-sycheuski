<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tag" uri="/WEB-INF/taglib.tld" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<fmt:setLocale value="${sessionScope.language == null ? 'en_US' : sessionScope.language}"/>
<div class="news-info-top news-info-padding">
  <p class="text-title news-title"><c:out value="${newsMessage.news.title}"/></p>
  <p class="text-title news-author"><c:out value="${newsMessage.author.name}"/></p>
  <p class="date-field">
    <fmt:formatDate pattern="dd MMM yyyy" value="${newsMessage.news.modificationDate}" />
  </p>
</div>
<p class="news-content news-text">
  <c:out value="${newsMessage.news.fullText}"/>
</p>
<div class="comment-container">
  <div class="comment-form">
    <div id="comments">
      <c:if test="${!newsMessage.comments.isEmpty()}">
        <c:forEach items="${newsMessage.comments}" var="comment">
          <div class="comment">
            <span class="date-span row">
                <fmt:formatDate timeZone="GMT+${time_zone}" type="both" pattern="dd MMM YYYY hh:mm:ss" value="${comment.creationDate}"/>
            </span>
            <p class="text-title comment-text row"><c:out value="${comment.commentText}"/></p>
          </div>
        </c:forEach>
      </c:if>
      <c:if test="${newsMessage.comments.isEmpty()}">
        <span id="no-comments-element" class="date-span row">
          &nbsp;
          <tag:get-property uri="view.news.message.no.comments"/>
        </span>
      </c:if>
    </div>
  </div>
</div>
