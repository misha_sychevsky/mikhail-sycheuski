<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div class="header-container">
  <img src="view/img/newspaper.png" class="news-img">
  <a class="page-title" href="news_portal?command=reset">
      <tiles:getAsString name="title"/>
  </a>
  <div class="localization">
    <input type="hidden" name="language" value="${sessionScope.language}">
    <c:set var="parameters" value="${requestScope['javax.servlet.forward.query_string']}"/>
    <c:if test="${fn:contains(requestScope['javax.servlet.forward.query_string'], '&language=RU')}">
      <c:set var="parameters" value="${fn:replace(requestScope['javax.servlet.forward.query_string'], '&language=RU', '')}"/>
    </c:if>
    <c:if test="${fn:contains(requestScope['javax.servlet.forward.query_string'], '&language=EN')}">
      <c:set var="parameters" value="${fn:replace(requestScope['javax.servlet.forward.query_string'], '&language=EN', '')}"/>
    </c:if>
    <c:choose>
      <c:when test="${sessionScope.language == 'EN'}">
        <a id="ru" href="news_portal?${parameters}&language=RU" class="locale-button active">RU</a>
        <a id="en" href="news_portal?${parameters}&language=EN" class="locale-button locale-selected">EN</a>
      </c:when>
      <c:when test="${sessionScope.language == 'RU'}">
        <a id="ru" href="news_portal?${parameters}&language=RU" class="locale-button locale-selected">RU</a>
        <a id="en" href="news_portal?${parameters}&language=EN" class="locale-button active">EN</a>
      </c:when>
    </c:choose>
  </div>
</div>
