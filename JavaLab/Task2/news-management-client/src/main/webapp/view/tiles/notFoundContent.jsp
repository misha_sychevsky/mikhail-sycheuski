<%@ taglib prefix="tag" uri="/WEB-INF/taglib.tld" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="news-container">
  <p class="error-text">
    <tag:get-property uri="view.error.not.found.code"/>
  </p>
  <p class="error-text error-text-message">
    <tag:get-property uri="view.error.not.found.text"/>
    <br>
    <tag:get-property uri="view.error.sorry"/>
  </p>
</div>
