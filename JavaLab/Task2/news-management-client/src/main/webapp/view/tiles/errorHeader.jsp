<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="error-container">
  <img src="${pageContext.request.contextPath}/view/img/newspaper.png" class="news-img">
  <a class="page-title" href="/news-management-client/news_portal?command=show_news">
    <tiles:getAsString name="title"/>
  </a>
</div>
