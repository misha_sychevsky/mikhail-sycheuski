<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="footer-container">
  <p>
    <tiles:getAsString name="footer-text"/>
  </p>
</div>
