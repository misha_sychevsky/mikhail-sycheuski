<%@ taglib prefix="tag" uri="/WEB-INF/taglib.tld" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="news-container">
  <div class="criteria-container">
    <form id="criteria-form" action="news_portal" method="get">
      <input type="hidden" name="command" value="filter_news"/>
      <dl class="dropdown">
        <dt>
          <a href="#">
            <span class="hida">
              <tag:get-property uri="view.dropdown.tags"/>
            </span>
          </a>
        </dt>
        <input type="hidden" name="selected_tags" value="${checkedTags}"/>
        <dd>
          <div class="mutliSelect">
            <ul>
              <div class="checkbox">
                <c:forEach items="${tags}" var="tag">
                  <li>
                    <input id="id${tag.tagId}" type="checkbox" value="${tag.name}" />
                    <label class="checkbox-label" for="id${tag.tagId}">${tag.name}</label>
                  </li>
                </c:forEach>
              </div>
            </ul>
          </div>
        </dd>
      </dl>
      <div id="dd" class="wrapper-dropdown-2" tabindex="1">
        <p class="select-title">
          <tag:get-property uri="view.dropdown.author"/>
        </p>
        <input type="hidden" name="selected_author" value="${checkedAuthor}">
        <ul class="dropdown-select">
          <c:forEach items="${authors}" var="author">
            <li id="${author.authorId}">
              <label class="radio-label"><c:out value="${author.name}"/></label>
            </li>
          </c:forEach>
        </ul>
      </div>
      <div id="filter-button" class="button active">
        <tag:get-property uri="view.button.filter"/>
      </div>
      <c:if test="${sessionScope.searchCriteria != null}">
        <a id="reset-button" class="button active" href="news_portal?command=reset">
          <tag:get-property uri="view.button.reset"/>
        </a>
      </c:if>
      <c:if test="${sessionScope.searchCriteria == null}">
        <div id="reset-button" class="button">
          <tag:get-property uri="view.button.reset"/>
        </div>
      </c:if>
    </form>
  </div>
  <div id="news-items-container">
    <jsp:include page="../jsp/pagination/printNewsPage.jsp"/>
  </div>
  <c:if test="${!newsMessages.isEmpty()}">
    <div class="navigation-container">
      <form id="pagination">
        <input id="button-prev" type="button" class="button active" value="<">
        <p class="page-number">${sessionScope.current_page}</p>
        <input id="button-next" type="button" class="button active" value=">">
      </form>
    </div>
  </c:if>
</div>
