package com.epam.portal.command;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import com.epam.portal.command.webimpl.logic.*;
import com.epam.portal.command.webimpl.pagination.ShowNewsMessage;
import com.epam.portal.command.webimpl.pagination.ShowNewsPage;
import com.epam.portal.exception.NoSuchCommandException;

/**
 * This is class which implements a pattern factory
 * This class get a commandName from request and return a instance of class <code>Command</code>.
 * Use Spring application context to instantiate <code>Command</code> objects.
 */
public class CommandFactory {

    /**
     * Spring application context object.
     */
    private static ApplicationContext context = new ClassPathXmlApplicationContext("spring/client-spring-context.xml");

    /**
     * Get the command from the commands enumeration which is equals with parameter and return it.
     * Return the default <code>Commands.UNKNOWN_COMMAND</code> instance if there is no such command in enumeration.
     *
     * @param commandName
     * @return <code>Command</code> specified instance
     */
    private static Commands identifyCommand(String commandName) {

        Commands command;
        try {
            command = Commands.valueOf(commandName.toUpperCase());
        }
        catch (IllegalArgumentException e) {
            command = Commands.UNKNOWN_COMMAND;
        }
        return command;
    }

    /**
     * This method get a commandName from request and return a instance of class <code>Command</code> which represents this command.
     *
     * @param commandName - name of the command.
     * @return a new <code>Command</code>, this a instance of class which implements
     * @throws NoSuchCommandException
     * a pattern command
     */
    public static Command getCommand(String commandName) throws NoSuchCommandException {

        Commands command = identifyCommand(commandName);
        switch (command) {

            case SHOW_NEWS_MESSAGE:
                return context.getBean(ShowNewsMessage.class);

            case POST_COMMENT:
                return context.getBean(PostComment.class);

            case RESET:
                return context.getBean(Reset.class);

            case SHOW_SELECTED_NEWS_MESSAGE:
                return context.getBean(ShowSelectedNewsMessage.class);

            case FILTER_NEWS:
                return context.getBean(FilterNews.class);

            case SHOW_NEWS_PAGE:
                return context.getBean(ShowNewsPage.class);

            case SHOW_NEWS:
                return context.getBean(ShowNews.class);

            case UNKNOWN_COMMAND:
                throw new NoSuchCommandException();

            default:
                throw new NoSuchCommandException();
        }
    }
}
