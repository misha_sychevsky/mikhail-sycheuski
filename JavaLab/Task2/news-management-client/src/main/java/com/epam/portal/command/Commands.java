package com.epam.portal.command;

/**
 * This enumeration contains all commands that <code>ClientDispatcher</code> can execute.
 */
public enum Commands {

    UNKNOWN_COMMAND, SHOW_NEWS, SHOW_NEWS_PAGE,
    FILTER_NEWS, SHOW_SELECTED_NEWS_MESSAGE, RESET,
    POST_COMMENT, SHOW_NEWS_MESSAGE,
}
