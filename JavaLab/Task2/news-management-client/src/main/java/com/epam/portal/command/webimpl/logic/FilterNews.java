package com.epam.portal.command.webimpl.logic;

import org.apache.log4j.Logger;
import com.epam.portal.command.Command;
import com.epam.portal.model.Author;
import com.epam.portal.model.NewsMessage;
import com.epam.portal.model.SearchCriteria;
import com.epam.portal.model.Tag;
import com.epam.portal.service.AuthorService;
import com.epam.portal.service.NewsMessageService;
import com.epam.portal.service.TagService;
import com.epam.portal.service.exception.ServiceException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.epam.portal.command.CommandConstants.*;

/**
 * This class represents Command pattern and implements <code>Command</code> interface.
 * This class is necessary to filter <code>News</code> list by search parameters.
 */
public class FilterNews implements Command {

    private static Logger LOGGER = Logger.getLogger(FilterNews.class);

    private NewsMessageService newsMessageService;
    private AuthorService authorService;
    private TagService tagService;

    public void setNewsMessageService(NewsMessageService newsMessageService) {

        this.newsMessageService = newsMessageService;
    }

    public void setAuthorService(AuthorService authorService) {

        this.authorService = authorService;
    }

    public void setTagService(TagService tagService) {

        this.tagService = tagService;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            HttpSession session = request.getSession();

            String authorIdString = request.getParameter(PARAMETER_SELECTED_AUTHOR);
            String tagIdsString = request.getParameter(PARAMETER_SELECTED_TAGS);
            Long authorId = parseLong(authorIdString);
            List<Long> tagIds = parseTagIds(tagIdsString);

            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.setAuthorId(authorId);
            searchCriteria.setTagIds(tagIds);

            int start = FIRST_PAGE;
            int count = start + ITEMS_COUNT_ON_PAGE;

            List<NewsMessage> newsMessages = newsMessageService.getNewsMessagePage(searchCriteria, start, count);
            List<Tag> tags = tagService.viewTheListOfTags();
            List<Author> authors = authorService.viewTheListOfAuthors();

            session.setAttribute(ATTRIBUTE_SEARCH_CRITERIA, searchCriteria);
            session.setAttribute(ATTRIBUTE_PAGE, FIRST_PAGE);
            session.setAttribute(ATTRIBUTE_LAST_NEWS_PAGE, false);

            request.setAttribute(ATTRIBUTE_CHECKED_AUTHOR, authorIdString);
            request.setAttribute(ATTRIBUTE_CHECKED_TAGS, tagIdsString);
            request.setAttribute(ATTRIBUTE_TAGS, tags);
            request.setAttribute(ATTRIBUTE_AUTHORS, authors);
            request.setAttribute(ATTRIBUTE_NEWS_MESSAGE_PAGE, newsMessages);
            request.getRequestDispatcher(FORWARD_MAIN_PAGE).forward(request, response);
        }
        catch (ServiceException e) {
            LOGGER.error(e);
            request.getRequestDispatcher(FORWARD_SERVER_ERROR_PAGE).forward(request, response);
        }
    }

    /**
     * This method parse tag ids from string to the <code>Long</code> and put it into the <code>List</code>.
     *
     * @param tagIdsString
     * @return <code>List</code> with tag ids in <code>Long</code> type
     */
    private List<Long> parseTagIds(String tagIdsString) {

        List<Long> tagIds = new ArrayList<>();
        List<String> tagIdsList = Arrays.asList(tagIdsString.replaceAll("id", "").split("-"));
        for(String tagIdItem : tagIdsList) {
            Long tagId = parseLong(tagIdItem);
            if(tagId != null) {
                tagIds.add(tagId);
            }
        }
        return tagIds;
    }

    /**
     * This method parse tag id from string to <code>Long</code>.
     *
     * @param longString
     * @return tag id <code>Long</code> type
     */
    private Long parseLong(String longString) {

        try {
            return Long.parseLong(longString);
        }
        catch (NumberFormatException e) {
            return null;
        }
    }
}
