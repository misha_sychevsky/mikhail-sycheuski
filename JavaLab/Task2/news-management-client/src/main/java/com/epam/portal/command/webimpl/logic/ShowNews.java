package com.epam.portal.command.webimpl.logic;

import org.apache.log4j.Logger;
import com.epam.portal.command.Command;
import com.epam.portal.model.Author;
import com.epam.portal.model.NewsMessage;
import com.epam.portal.model.SearchCriteria;
import com.epam.portal.model.Tag;
import com.epam.portal.service.AuthorService;
import com.epam.portal.service.NewsMessageService;
import com.epam.portal.service.TagService;
import com.epam.portal.service.exception.ServiceException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

import static com.epam.portal.command.CommandConstants.*;

/**
 * This class represents Command pattern and implements <code>Command</code> interface.
 * It shows the hole page with list of <code>News</code> entities. It also shows action bar to filter news.
 */
public class ShowNews implements Command {

    private static Logger LOGGER = Logger.getLogger(ShowNews.class);

    private NewsMessageService newsMessageService;
    private AuthorService authorService;
    private TagService tagService;

    public void setNewsMessageService(NewsMessageService newsMessageService) {

        this.newsMessageService = newsMessageService;
    }

    public void setAuthorService(AuthorService authorService) {

        this.authorService = authorService;
    }

    public void setTagService(TagService tagService) {

        this.tagService = tagService;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            HttpSession session = request.getSession();
            SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute(ATTRIBUTE_SEARCH_CRITERIA);
            Integer page = (Integer) session.getAttribute(ATTRIBUTE_PAGE);

            if(page == null) {
                page = FIRST_PAGE;
            }

            int start = (page - 1) * ITEMS_COUNT_ON_PAGE + 1;
            int end = start + ITEMS_COUNT_ON_PAGE;

            List<NewsMessage> newsMessages = newsMessageService.getNewsMessagePage(searchCriteria, start, end);
            List<Tag> tags = tagService.viewTheListOfTags();
            List<Author> authors = authorService.viewTheListOfAuthors();

            session.setAttribute(ATTRIBUTE_PAGE, page);

            request.setAttribute(ATTRIBUTE_TAGS, tags);
            request.setAttribute(ATTRIBUTE_AUTHORS, authors);
            request.setAttribute(ATTRIBUTE_NEWS_MESSAGE_PAGE, newsMessages);
            request.getRequestDispatcher(FORWARD_MAIN_PAGE).forward(request, response);
        }
        catch (ServiceException e) {
            LOGGER.error(e);
            request.getRequestDispatcher(FORWARD_SERVER_ERROR_PAGE).forward(request, response);
        }
    }
}
