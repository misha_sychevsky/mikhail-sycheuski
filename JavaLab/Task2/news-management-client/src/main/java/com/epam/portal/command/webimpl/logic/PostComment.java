package com.epam.portal.command.webimpl.logic;

import org.apache.log4j.Logger;
import com.epam.portal.command.Command;
import com.epam.portal.model.Comment;
import com.epam.portal.service.CommentService;
import com.epam.portal.service.exception.ServiceException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

import static com.epam.portal.command.CommandConstants.ATTRIBUTE_CURRENT_NEWS_MESSAGE;
import static com.epam.portal.command.CommandConstants.PARAMETER_COMMENT_TEXT;

/**
 * This class represents Command pattern and implements <code>Command</code> interface.
 * It checks if the comment is valid and save it if it is.
 */
public class PostComment implements Command {

    private static Logger LOGGER = Logger.getLogger(PostComment.class);

    private CommentService commentService;

    public void setCommentService(CommentService commentService) {

        this.commentService = commentService;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            String commentText = request.getParameter(PARAMETER_COMMENT_TEXT);
            Long currentNewsMessageId = (Long) request.getSession().getAttribute(ATTRIBUTE_CURRENT_NEWS_MESSAGE);

            if(isCommentValid(commentText)) {
                Comment comment = new Comment();
                comment.setNewsId(currentNewsMessageId);
                comment.setCreationDate(new Date());
                comment.setCommentText(commentText);

                commentService.addComment(comment);
            }
            else {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
        }
        catch (ServiceException e) {
            LOGGER.error(e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Method for comment validation. Check comment text.
     *
     * @param commentText
     * @return <code>true</code> if the comment is valid, and <code>false</code> if it isn't.
     */
    private boolean isCommentValid(String commentText) {

        return commentText != null && !commentText.isEmpty() && commentText.length() < 100;
    }
}
