package com.epam.portal.controller.filter;

import com.epam.portal.command.CommandConstants;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.epam.portal.command.CommandConstants.ATTRIBUTE_LANGUAGE;

public class LocaleChangeHandleFilter implements Filter {

    private static final String DEFAULT_LOCALE = "EN";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpSession session = request.getSession();
        String changeLocale = request.getParameter(CommandConstants.ATTRIBUTE_LANGUAGE);

        if(session.getAttribute(ATTRIBUTE_LANGUAGE) == null) {
            session.setAttribute(ATTRIBUTE_LANGUAGE, DEFAULT_LOCALE);
        }
        if(changeLocale != null) {
            if(changeLocale.equalsIgnoreCase("EN") || changeLocale.equalsIgnoreCase("RU")) {
                session.setAttribute(ATTRIBUTE_LANGUAGE, changeLocale);
            }
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}
