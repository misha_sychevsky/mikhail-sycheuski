package com.epam.portal.controller;

import org.apache.log4j.Logger;
import com.epam.portal.command.Command;
import com.epam.portal.command.CommandConstants;
import com.epam.portal.command.CommandFactory;
import com.epam.portal.exception.NoSuchCommandException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This class implements the pattern MVC representing front controller.
 * This is the servlet which handles requests from client.
 */
public class ClientDispatcher extends HttpServlet {

    public static final long serialVersionUID = 20150907L;
    public static Logger LOGGER = Logger.getLogger(ClientDispatcher.class);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        processRequest(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        processRequest(request, response);
    }

    /**
     * This method gets a instance of <code>Command</code> from <code>CommandFactory</code>
     * by request and execute this command.
     *
     * @param request - <code>HttpServletRequest</code> object
     * @param response - <code>HttpServletResponse</code> object
     * @throws ServletException
     * @throws IOException
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            Command command = CommandFactory.getCommand(request.getParameter(CommandConstants.PARAMETER_COMMAND));
            command.execute(request, response);
        }
        catch (ServletException | IOException e) {
            LOGGER.error(e);
            request.getRequestDispatcher(CommandConstants.FORWARD_SERVER_ERROR_PAGE).forward(request, response);
        }
        catch (NoSuchCommandException e) {
            LOGGER.error(e);
            request.getRequestDispatcher(CommandConstants.FORWARD_NOT_FOUND_PAGE).forward(request, response);
        }
    }
}
