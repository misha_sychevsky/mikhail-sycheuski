package com.epam.portal.command.webimpl.pagination;

import org.apache.log4j.Logger;
import com.epam.portal.command.Command;
import com.epam.portal.model.NewsMessage;
import com.epam.portal.model.SearchCriteria;
import com.epam.portal.service.NewsMessageService;
import com.epam.portal.service.exception.ServiceException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

import static com.epam.portal.command.CommandConstants.*;

/**
 * This class represents Command pattern and implements <code>Command</code> interface.
 * This class show next or previous <code>News</code> entity page using the navigation parameter.
 * Makes response for ajax request. If there is one next or previous page it will be added to response. If there is no one, this command will add NOT_FOUND http code to the response.
 */
public class ShowNewsPage implements Command {

    private static Logger LOGGER = Logger.getLogger(ShowNewsPage.class);

    private NewsMessageService newsMessageService;

    public void setNewsMessageService(NewsMessageService newsMessageService) {

        this.newsMessageService = newsMessageService;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            HttpSession session = request.getSession();
            SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute(ATTRIBUTE_SEARCH_CRITERIA);
            String navigation = request.getParameter(PARAMETER_NAVIGATION);

            boolean notFound = false;
            if(navigation.equals(NAVIGATION_NEXT)) {
                notFound = handleNextPage(session, request, response, searchCriteria);
            }
            if(navigation.equals(NAVIGATION_PREV)) {
                handlePreviewPage(session, request, response, searchCriteria);
            }

            if(!notFound) {
                request.getRequestDispatcher(FORWARD_PRINT_NEWS_PAGE).forward(request, response);
            }
        }
        catch (ServiceException e) {
            LOGGER.error(e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * This method is necessary to handle next page action. It gets next page and updates current page attribute.
     * It also checks if the page is last and adds NOT_FOUND http code to response if it is.
     *
     * @param session
     * @param request
     * @param response
     * @param searchCriteria - <code>SearchCriteria</code> object which represents search parameters
     * @return <code>true</code> if it is last page now, and <code>false</code> if it isn't.
     * @throws ServiceException
     */
    private boolean handleNextPage(HttpSession session, HttpServletRequest request,
                                   HttpServletResponse response, SearchCriteria searchCriteria) throws ServiceException{

        boolean isLastPage = false;

        List<NewsMessage> newsMessages = null;
        if(session.getAttribute(ATTRIBUTE_LAST_NEWS_PAGE) != true) {
            int page = (int) session.getAttribute(ATTRIBUTE_PAGE);
            int start = page == FIRST_PAGE ? ITEMS_COUNT_ON_PAGE + 1 : page * ITEMS_COUNT_ON_PAGE + 1;
            int count = start + ITEMS_COUNT_ON_PAGE;

            newsMessages = newsMessageService.getNewsMessagePage(searchCriteria, start, count);
            session.setAttribute(ATTRIBUTE_PAGE, ++page);

            if (newsMessages.size() == 0) {
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                session.setAttribute(ATTRIBUTE_PAGE, --page);
                session.setAttribute(ATTRIBUTE_LAST_NEWS_PAGE, true);
                isLastPage = true;
            }
        }
        else {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            isLastPage = true;
        }

        if(!isLastPage) {
            request.setAttribute(ATTRIBUTE_NEWS_MESSAGE_PAGE, newsMessages);
        }

        return isLastPage;
    }

    /**
     * This method is necessary to handle previous page action. It gets previous page and updates current page attribute.
     * It also checks if the page is first and adds NOT_FOUND http code to response if it is.
     *
     * @param session
     * @param request
     * @param response
     * @param searchCriteria - <code>SearchCriteria</code> object which represents search parameters
     * @throws ServiceException
     */
    private void handlePreviewPage(HttpSession session, HttpServletRequest request,
                                   HttpServletResponse response, SearchCriteria searchCriteria) throws ServiceException{

        int page = (int) session.getAttribute(ATTRIBUTE_PAGE);
        if(page != FIRST_PAGE) {
            int start = (page == 2 ? 1 : (page - 2) * ITEMS_COUNT_ON_PAGE + 1);
            int count = start + ITEMS_COUNT_ON_PAGE;

            List<NewsMessage> newsMessages = newsMessageService.getNewsMessagePage(searchCriteria, start, count);
            session.setAttribute(ATTRIBUTE_PAGE, --page);
            request.setAttribute(ATTRIBUTE_NEWS_MESSAGE_PAGE, newsMessages);

            if(session.getAttribute(ATTRIBUTE_LAST_NEWS_PAGE) == true)
            {
                session.setAttribute(ATTRIBUTE_LAST_NEWS_PAGE, false);
            }
        }
        else {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
    }
}
