package com.epam.portal.command.webimpl.pagination;

import org.apache.log4j.Logger;
import com.epam.portal.command.Command;
import com.epam.portal.model.NewsMessage;
import com.epam.portal.model.SearchCriteria;
import com.epam.portal.service.NewsMessageService;
import com.epam.portal.service.exception.ServiceException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.epam.portal.command.CommandConstants.*;

/**
 * This class represents Command pattern and implements <code>Command</code> interface.
 * This class show next or previous <code>NewsMessage</code> entity using the navigation parameter.
 * Makes response for ajax request. If there is one next or previous entity it will be added to response. If there is no one, this command will add NOT_FOUND http code to the response.
 */
public class ShowNewsMessage implements Command {

    private static Logger LOGGER = Logger.getLogger(ShowNewsMessage.class);

    private NewsMessageService newsMessageService;

    public void setNewsMessageService(NewsMessageService newsMessageService) {

        this.newsMessageService = newsMessageService;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            String navigation = request.getParameter(PARAMETER_NAVIGATION);

            HttpSession session = request.getSession();
            Long currentNewsMessageId = (Long) session.getAttribute(ATTRIBUTE_CURRENT_NEWS_MESSAGE);
            int currentPage = (int) session.getAttribute(ATTRIBUTE_PAGE);
            int newsMessageIndex = (int) session.getAttribute(ATTRIBUTE_NEWS_INDEX);
            SearchCriteria searchCriteria = (SearchCriteria) session.getAttribute(ATTRIBUTE_SEARCH_CRITERIA);

            NewsMessage newsMessage;
            if (navigation.equals(NAVIGATION_NEXT)) {
                newsMessage = newsMessageService.getNextNewsMessage(searchCriteria, currentNewsMessageId);
                if(newsMessage != null) {
                    if(newsMessageIndex == ITEMS_COUNT_ON_PAGE) {
                        session.setAttribute(ATTRIBUTE_NEWS_INDEX, FIRST_NEWS_MESSAGE_ON_PAGE);
                        session.setAttribute(ATTRIBUTE_PAGE, ++currentPage);
                    }
                    else {
                        session.setAttribute(ATTRIBUTE_NEWS_INDEX, ++newsMessageIndex);
                    }
                    session.setAttribute(ATTRIBUTE_CURRENT_NEWS_MESSAGE, newsMessage.getNews().getNewsId());

                    request.setAttribute(ATTRIBUTE_NEWS_MESSAGE, newsMessage);
                    request.getRequestDispatcher(FORWARD_PRINT_NEWS_MESSAGE_PAGE).forward(request, response);
                }
                else {
                    response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                }
            }

            if (navigation.equals(NAVIGATION_PREV)) {
                newsMessage = newsMessageService.getPrevNewsMessage(searchCriteria, currentNewsMessageId);
                if(newsMessage != null) {
                    if(newsMessageIndex == FIRST_NEWS_MESSAGE_ON_PAGE) {
                        session.setAttribute(ATTRIBUTE_NEWS_INDEX, ITEMS_COUNT_ON_PAGE);
                        session.setAttribute(ATTRIBUTE_PAGE, --currentPage);
                    }
                    else {
                        session.setAttribute(ATTRIBUTE_NEWS_INDEX, --newsMessageIndex);
                    }
                    session.setAttribute(ATTRIBUTE_CURRENT_NEWS_MESSAGE, newsMessage.getNews().getNewsId());

                    request.setAttribute(ATTRIBUTE_NEWS_MESSAGE, newsMessage);
                    request.getRequestDispatcher(FORWARD_PRINT_NEWS_MESSAGE_PAGE).forward(request, response);
                }
                else {
                    response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                }
            }
        }
        catch (ServiceException e) {
            LOGGER.error(e);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }
}
