package com.epam.portal.command;

/**
 * This class contains all the constants used by the commands.
 */
public final class CommandConstants {

    /*
    REQUEST PARAMETERS
     */
    public static final String PARAMETER_COMMAND = "command";
    public static final String PARAMETER_NAVIGATION = "navigation";
    public static final String PARAMETER_SELECTED_AUTHOR = "selected_author";
    public static final String PARAMETER_SELECTED_TAGS = "selected_tags";
    public static final String PARAMETER_NEWS_ID = "news_id";
    public static final String PARAMETER_NEWS_INDEX = "news_index";
    public static final String PARAMETER_COMMENT_TEXT = "comment_text";
    /*
    REQUEST ATTRIBUTES
     */
    public static final String ATTRIBUTE_PAGE = "current_page";
    public static final String ATTRIBUTE_LAST_NEWS_PAGE = "lastNewsPage";
    public static final String ATTRIBUTE_NEWS_MESSAGE_PAGE = "newsMessages";
    public static final String ATTRIBUTE_TAGS = "tags";
    public static final String ATTRIBUTE_AUTHORS = "authors";
    public static final String ATTRIBUTE_SEARCH_CRITERIA = "searchCriteria";
    public static final String ATTRIBUTE_NEWS_MESSAGE = "newsMessage";
    public static final String ATTRIBUTE_CURRENT_NEWS_MESSAGE = "currentNewsMessage";
    public static final String ATTRIBUTE_NEWS_INDEX = "newsIndex";
    public static final String ATTRIBUTE_LANGUAGE = "language";
    public static final String ATTRIBUTE_CHECKED_AUTHOR = "checkedAuthor";
    public static final String ATTRIBUTE_CHECKED_TAGS = "checkedTags";

    /*
    PAGINATION CONSTANTS
     */
    public static final int ITEMS_COUNT_ON_PAGE = 3;
    public static final int FIRST_PAGE = 1;
    public static final int FIRST_NEWS_MESSAGE_ON_PAGE = 1;
    public static final String NAVIGATION_NEXT = "next";
    public static final String NAVIGATION_PREV = "prev";
    /*
    FORWARD URLS
     */
    public static final String FORWARD_MAIN_PAGE = "view/jsp/logic/newsPage.jsp";
    public static final String FORWARD_PRINT_NEWS_PAGE = "view/jsp/pagination/printNewsPage.jsp";
    public static final String FORWARD_PRINT_NEWS_MESSAGE_PAGE = "view/jsp/pagination/printNewsMessage.jsp";
    public static final String FORWARD_NEWS_MESSAGE_PAGE = "view/jsp/logic/newsMessagePage.jsp";
    public static final String FORWARD_SERVER_ERROR_PAGE = "view/jsp/error/serverErrorPage.jsp";
    public static final String FORWARD_NOT_FOUND_PAGE = "view/jsp/error/notFoundPage.jsp";
    public static final String FORWARD_INDEX_PAGE = "index.jsp";
}
