package com.epam.portal.command.webimpl.logic;

import com.epam.portal.command.Command;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.portal.command.CommandConstants.FORWARD_INDEX_PAGE;

/**
 * This class represents Command pattern and implements <code>Command</code> interface.
 * It invalidates session and removes search parameters.
 */
public class Reset implements Command {

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.getSession().invalidate();
        request.getRequestDispatcher(FORWARD_INDEX_PAGE).forward(request, response);
    }
}
