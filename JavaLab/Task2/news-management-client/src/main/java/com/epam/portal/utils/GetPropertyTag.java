package com.epam.portal.utils;

import org.apache.log4j.Logger;
import com.epam.portal.command.CommandConstants;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

/**
 * Tag gets property from user taglib.
 * This class is necessary to get the bundle property for jsp page
 */
public class GetPropertyTag extends TagSupport {

    private static final String RESOURCE_PATH = "localization.language";
    private static final Logger LOGGER = Logger.getLogger(GetPropertyTag.class);

    private String uri;

    public void setUri(String uri)
    {
        this.uri = uri;
    }

    @Override
    public int doStartTag() throws JspException
    {
        String language = (String) pageContext.getSession().getAttribute(CommandConstants.ATTRIBUTE_LANGUAGE);
        Locale locale = new Locale(language);

        ResourceBundle resource = ResourceBundle.getBundle(RESOURCE_PATH, locale, new BundleControlUTF8());
        JspWriter writer = pageContext.getOut();
        try {
            try {
                writer.print(resource.getString(uri));
            }
            catch (MissingResourceException ex) {
                writer.print("undefined");
                LOGGER.error(ex);
            }
        }
        catch (IOException ex) {
            LOGGER.error(ex);
        }

        return SKIP_BODY;
    }
}
