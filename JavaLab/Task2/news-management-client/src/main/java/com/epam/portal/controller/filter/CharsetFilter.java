package com.epam.portal.controller.filter;

import javax.servlet.*;
import java.io.IOException;

/**
 * This class is necessary for coding translation in UTF-8.
 * @author Michail Sychevsky
 */
public class CharsetFilter implements Filter {

    public static final String CHARSET = "UTF-8";
    public static final String CONTENT_TYPE = "text/html;charset=UTF-8";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
    {
        String encoding = request.getCharacterEncoding();
        if (!CHARSET.equals(encoding))
        {
            request.setCharacterEncoding(CHARSET);
        }
        response.setContentType(CONTENT_TYPE);
        chain.doFilter(request, response);
    }

    public void destroy()
    {

    }
}
