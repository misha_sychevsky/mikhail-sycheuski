package com.epam.portal.exception;

/**
 * Mo such command exception.
 * This class represents exception which can be occurred in the <code>CommandFactory</code> class when there is no such command in command enumeration.
 */
public class NoSuchCommandException extends Exception {

    /**
     * No such command Exception message.
     */
    public static final String NO_SUCH_COMMAND_EXCEPTION_MESSAGE = "Command isn't exists.";

    public NoSuchCommandException()
    {
        super(NO_SUCH_COMMAND_EXCEPTION_MESSAGE);
    }
}
