package com.epam.portal.command;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Command pattern interface.
 */
public interface Command {

    /**
     * This method represents command execution. It analyzes request parameters and and transmits data for the corresponding service processing.
     *      *
     * @param request - client's request with parameters
     * @param response - server response with processed information
     * @throws ServletException
     * @throws IOException
     */
    void execute(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException;
}
