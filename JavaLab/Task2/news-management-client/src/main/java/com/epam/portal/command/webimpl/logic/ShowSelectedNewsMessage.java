package com.epam.portal.command.webimpl.logic;

import org.apache.log4j.Logger;
import com.epam.portal.command.Command;
import com.epam.portal.model.NewsMessage;
import com.epam.portal.service.NewsMessageService;
import com.epam.portal.service.exception.ServiceException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.epam.portal.command.CommandConstants.*;

/**
 * This class represents Command pattern and implements <code>Command</code> interface.
 * It shows <code>NewsMessage</code> entity which is selected by user.
 */
public class ShowSelectedNewsMessage implements Command {

    private static Logger LOGGER = Logger.getLogger(ShowSelectedNewsMessage.class);

    private NewsMessageService newsMessageService;

    public void setNewsMessageService(NewsMessageService newsMessageService) {

        this.newsMessageService = newsMessageService;
    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            Long newsId = Long.parseLong(request.getParameter(PARAMETER_NEWS_ID));
            int newsIndex = Integer.parseInt(request.getParameter(PARAMETER_NEWS_INDEX));

            NewsMessage newsMessage = newsMessageService.viewSingleNewsMessage(newsId);

            HttpSession session = request.getSession();
            session.setAttribute(ATTRIBUTE_NEWS_INDEX, newsIndex);
            session.setAttribute(ATTRIBUTE_CURRENT_NEWS_MESSAGE, newsId);

            request.setAttribute(ATTRIBUTE_NEWS_MESSAGE, newsMessage);
            request.getRequestDispatcher(FORWARD_NEWS_MESSAGE_PAGE).forward(request, response);
        }
        catch (ServiceException e) {
            LOGGER.error(e);
            request.getRequestDispatcher(FORWARD_SERVER_ERROR_PAGE).forward(request, response);
        }
    }
}
