package portal.service;

import portal.model.SearchCriteria;
import portal.model.News;
import portal.model.NewsMessage;
import portal.model.Tag;
import portal.service.exception.ServiceException;

import java.util.List;

/**
 * News service interface.
 * This interface represents business operations for news entity.
 */
public interface NewsService {

    /**
     * This method adds new news.
     * Can produce <code>ServiceException</code>.
     *
     * @param news
     * @return <code>News</code> id
     * @throws ServiceException
     */
    Long addNews(News news) throws ServiceException;

    /**
     * This method edits news information.
     * Can produce <code>ServiceException</code>.
     *
     * @param news
     * @throws ServiceException
     */
    void editNews(News news) throws ServiceException;

    /**
     * This method deletes news.
     * Can produce <code>ServiceException</code>.
     *
     * @param news
     * @throws ServiceException
     */
    void deleteNews(News news) throws ServiceException;

    /**
     * This method returns news by id.
     * Can produce <code>ServiceException</code>.
     *
     * @param newsId
     * @return <code>News</code>
     * @throws ServiceException
     */
    News getNewsById(Long newsId) throws ServiceException;

    /**
     * This method returns the number of all news.
     * Can produce <code>ServiceException</code>.
     *
     * @return the number of news
     * @throws ServiceException
     */
    int countAllNews() throws ServiceException;

    /**
     * This method returns news by search criteria.
     * Can produce <code>ServiceException</code>.
     *
     * @param searchCriteria
     * @return <code>List</code> of news
     * @throws ServiceException
     */
    List<News> searchNewsBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException;

    /**
     * This method adds tag to news.
     * Can produce <code>ServiceException</code>.
     *
     * @param tagId
     * @param newsId
     * @throws ServiceException
     */
    void addTagToNews(Long tagId, Long newsId) throws ServiceException;

    /**
     * This method adds more than one tag to news.
     * Can produce <code>ServiceException</code>.
     *
     * @param tags
     * @param newsId
     * @throws ServiceException
     */
    void addTagsToNews(List<Tag> tags, Long newsId) throws ServiceException;

    /**
     * This method deletes association of the author with news.
     * Can produce <code>ServiceException</code>.
     *
     * @param authorId
     * @throws ServiceException
     */
    void deleteAuthorFromAllNews(Long authorId) throws ServiceException;

    /**
     * This method adds author to news.
     * Can produce <code>ServiceException</code>.
     *
     * @param newsId
     * @param authorId
     * @throws ServiceException
     */
    void addAuthorToNews(Long newsId, Long authorId) throws ServiceException;
}
