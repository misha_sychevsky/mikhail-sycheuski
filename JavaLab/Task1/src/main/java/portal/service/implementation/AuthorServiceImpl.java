package portal.service.implementation;

import org.springframework.transaction.annotation.Transactional;
import portal.dao.AuthorDAO;
import portal.dao.exception.DAOException;
import portal.model.Author;
import portal.service.AuthorService;
import portal.service.exception.ServiceException;

import java.util.List;

/**
 * Author service.
 * This is an implementation of the author service interface.
 */
public class AuthorServiceImpl implements AuthorService {

    /**
     * Date access object of the author entity.
     */
    private AuthorDAO authorDAO;

    public void setAuthorDAO(AuthorDAO authorDAO) {

        this.authorDAO = authorDAO;
    }

    @Override
    public Author getAuthorById(Long authorId) throws ServiceException {

        try {
            return authorDAO.get(authorId);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Long addAuthor(Author author) throws ServiceException {

        try {
            return authorDAO.insert(author);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void editAuthor(Author author) throws ServiceException {

        try {
            authorDAO.update(author);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    @Transactional
    public void deleteAuthor(Author author) throws ServiceException {

        try {
            authorDAO.deleteAllAssociationsNewsWithAuthor(author.getAuthorId());
            authorDAO.delete(author);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public List<Author> viewTheListOfAuthors() throws ServiceException {

        try {
            return authorDAO.getAll();
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Author getAuthorByNews(Long newsId) throws ServiceException {

        try {
            return authorDAO.getAuthorByNews(newsId);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void setAuthorExpired(Long authorId) throws ServiceException {

        try {
            authorDAO.setAuthorExpired(authorId);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
