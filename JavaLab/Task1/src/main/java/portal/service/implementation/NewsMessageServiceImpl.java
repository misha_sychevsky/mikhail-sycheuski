package portal.service.implementation;

import org.springframework.transaction.annotation.Transactional;
import portal.model.NewsMessage;
import portal.service.*;
import portal.service.exception.ServiceException;

/**
 * News message service.
 * This is an implementation of the news message service interface.
 */
public class NewsMessageServiceImpl implements NewsMessageService {

    private NewsService newsManager;
    private TagService tagManager;
    private AuthorService authorManager;
    private CommentService commentManager;

    public void setNewsManager(NewsService newsManager) {

        this.newsManager = newsManager;
    }

    public void setTagManager(TagService tagManager) {

        this.tagManager = tagManager;
    }

    public void setAuthorManager(AuthorService authorManager) {

        this.authorManager = authorManager;
    }

    public void setCommentManager(CommentService commentManager) {

        this.commentManager = commentManager;
    }

    @Override
    @Transactional
    public void saveNewsMessage(NewsMessage newsMessage) throws ServiceException {

        newsManager.addNews(newsMessage.getNews());
        authorManager.addAuthor(newsMessage.getAuthor());
        newsManager.addAuthorToNews(newsMessage.getNews().getNewsId(), newsMessage.getAuthor().getAuthorId());
        newsManager.addTagsToNews(newsMessage.getTags(), newsMessage.getNews().getNewsId());
    }

    @Override
    @Transactional
    public NewsMessage viewSingleNewsMessage(long newsId) throws ServiceException {

        NewsMessage newsMessage = new NewsMessage();
        newsMessage.setNews(newsManager.getNewsById(newsId));
        newsMessage.setAuthor(authorManager.getAuthorByNews(newsId));
        newsMessage.setTags(tagManager.getTagsByNews(newsId));
        newsMessage.setComments(commentManager.getCommentsByNews(newsId));

        return newsMessage;
    }
}
