package portal.service.implementation;

import org.springframework.transaction.annotation.Transactional;
import portal.dao.AuthorDAO;
import portal.dao.CommentDAO;
import portal.dao.NewsDAO;
import portal.dao.TagDAO;
import portal.dao.exception.DAOException;
import portal.model.SearchCriteria;
import portal.model.News;
import portal.model.Tag;
import portal.service.NewsService;
import portal.service.exception.ServiceException;

import java.util.List;

/**
 * News service.
 * This is an implementation of the news service interface.
 */
public class NewsServiceImpl implements NewsService {

    /**
     * Date access object of the news entity.
     */
    private NewsDAO newsDAO;
    /**
     * Date access object of the tag entity.
     */
    private TagDAO tagDAO;
    /**
     * Date access object of the author entity.
     */
    private AuthorDAO authorDAO;
    /**
     * Date access object of the comment entity.
     */
    private CommentDAO commentDAO;

    public void setCommentDAO(CommentDAO commentDAO) {

        this.commentDAO = commentDAO;
    }

    public void setTagDAO(TagDAO tagDAO) {

        this.tagDAO = tagDAO;
    }

    public void setAuthorDAO(AuthorDAO authorDAO) {

        this.authorDAO = authorDAO;
    }

    public void setNewsDAO(NewsDAO newsDAO) {

        this.newsDAO = newsDAO;
    }

    @Override
    public Long addNews(News news) throws ServiceException {

        try {
            return newsDAO.insert(news);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void editNews(News news) throws ServiceException {

        try {
            newsDAO.update(news);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    @Transactional
    public void deleteNews(News news) throws ServiceException {

        try {
            authorDAO.deleteAssociationNewsWithAuthor(news.getNewsId());
            tagDAO.deleteAssociationsNewsWithTagsByNews(news.getNewsId());
            commentDAO.deleteCommentsByNews(news.getNewsId());
            newsDAO.delete(news);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public int countAllNews() throws ServiceException {

        try {
            return newsDAO.countNews();
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }


    @Override
    public List<News> searchNewsBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException {

        try {
            return newsDAO.getNewsBySearchCriteria(searchCriteria);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public News getNewsById(Long newsId) throws ServiceException {

        try {
            return newsDAO.get(newsId);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void addTagToNews(Long tagId, Long newsId) throws ServiceException {

        try {
            tagDAO.associateNewsWithTag(tagId, newsId);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    @Transactional
    public void addTagsToNews(List<Tag> tags, Long newsId) throws ServiceException {

        try {
            tagDAO.associateNewsWithTags(tags, newsId);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }


    @Override
    public void deleteAuthorFromAllNews(Long authorId) throws ServiceException {

        try {
            authorDAO.deleteAllAssociationsNewsWithAuthor(authorId);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public void addAuthorToNews(Long newsId, Long authorId) throws ServiceException {

        try {
            authorDAO.associateNewsWithAuthor(newsId, authorId);
        }
        catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
