package portal.service;

import portal.model.NewsMessage;
import portal.service.exception.ServiceException;

/**
 * News message service interface.
 * This is a super-service interface which represents business operations for composite news message entity.
 */
public interface NewsMessageService {

    /**
     * This method returns single news message entity by id.
     * Can produce <code>ServiceException</code>.
     *
     * @param newsId
     * @return <code>NewsMessage</code>
     * @throws ServiceException
     */
    NewsMessage viewSingleNewsMessage(long newsId) throws ServiceException;

    /**
     * This method adds new news message and delegates inserting of it's parts to the according services.
     * Can produce <code>ServiceException</code>.
     *
     * @param newsMessage
     * @throws ServiceException
     */
    void saveNewsMessage(NewsMessage newsMessage) throws ServiceException;
}
