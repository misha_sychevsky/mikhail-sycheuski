package portal.dao.util;

import portal.model.SearchCriteria;
import portal.model.Tag;

import java.util.ArrayList;
import java.util.List;

/**
 * Search query builder class.
 * This class builds query using <code>SearchCriteria</code> object.
 */
public class SearchQueryBuilder {

    /**
     * Base part of SQL query.
     */
    public static final String BASE_SQL = "SELECT DISTINCT NEWS.NEWS_ID, NEWS.TITLE, NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.CREATION_DATE, NEWS.MODIFICATION_DATE FROM MIKHAIL.NEWS ";
    /**
     * Join with NEWS_TAG table.
     */
    public static final String JOIN_NEWS_TAG = "JOIN MIKHAIL.NEWS_TAG ON MIKHAIL.NEWS.NEWS_ID = MIKHAIL.NEWS_TAG.NEWS_ID ";
    /**
     * Join with NEWS_AUTHOR table.
     */
    public static final String JOIN_NEWS_AUTHOR = "JOIN MIKHAIL.NEWS_AUTHOR ON MIKHAIL.NEWS.NEWS_ID = MIKHAIL.NEWS_AUTHOR.NEWS_ID ";
    /**
     * Join with COMMENTS table.
     */
    public static final String JOIN_COMMENTS = "JOIN (SELECT NEWS_ID AS N_ID FROM MIKHAIL.COMMENTS GROUP BY NEWS_ID ORDER BY COUNT(*) DESC) ON MIKHAIL.NEWS.NEWS_ID = N_ID";
    /**
     * Part of the SQL query with order clause.
     */
    public static final String ORDER_CLAUSE = " ORDER BY MODIFICATION_DATE";
    /**
     * Where clause.
     */
    public static final String WHERE = " WHERE ";
    /**
     * AUTHOR_ID clause.
     */
    public static final String AUTHOR_ID_CLAUSE = "AUTHOR_ID = ";
    /**
     * OR clause.
     */
    public static final String OR = " OR ";
    /**
     * TAG_ID clause.
     */
    public static final String TAG_ID_CLAUSE = "TAG_ID = ";

    /**
     * This method checks the <code>SearchCriteria</code> and builds SQL query using it.
     *
     * @param searchCriteria
     * @return <code>String</code> with a ready SQL search query
     */
    public String buildQuery(SearchCriteria searchCriteria) {

        StringBuilder query = new StringBuilder();
        if (checkSearchCriteria(searchCriteria)) {
            query.append(BASE_SQL).append(JOIN_COMMENTS).append(ORDER_CLAUSE);
        } else {
            query.append(BASE_SQL);

            boolean checkAuthor = false;
            if (searchCriteria.getAuthorId() != null) {
                query.append(JOIN_NEWS_AUTHOR);
                checkAuthor = true;
            }
            boolean checkTags = false;
            if (searchCriteria.getTagIds() != null && !searchCriteria.getTagIds().isEmpty()) {
                query.append(JOIN_NEWS_TAG);
                checkTags = true;
            }

            query.append(JOIN_COMMENTS).append(WHERE);

            if (checkAuthor) {
                query.append(AUTHOR_ID_CLAUSE).append(searchCriteria.getAuthorId()).append(OR);
            }
            if (checkTags) {
                for (Long tagId : searchCriteria.getTagIds()) {
                    query.append(TAG_ID_CLAUSE).append(tagId).append(OR);
                }
            }
            query.delete(query.length() - 3, query.length());

            query.append(ORDER_CLAUSE);
        }
        return query.toString();
    }

    /**
     * This method checks the <code>SearchCriteria</code>.
     *
     * @param searchCriteria
     * @return <code>true</code> if the <code>SearchCriteria</code> is empty and <code>false</code> if it isn't.
     */
    private boolean checkSearchCriteria(SearchCriteria searchCriteria) {

        boolean result = true;
        if (searchCriteria != null) {
            result = searchCriteria.getAuthorId() == null && searchCriteria.getTagIds() == null;
        }
        return result;
    }
}
