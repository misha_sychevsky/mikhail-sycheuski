package portal.dao;

import portal.dao.exception.DAOException;
import portal.model.SearchCriteria;
import portal.model.News;

import java.util.List;

/**
 * News data access object interface.
 * This interface is necessary for interaction with a table of news in the database.
 */
public interface NewsDAO extends GenericDAO<News> {

    /**
     * This method returns the number of news in the NEWS table.
     * Can produce <code>DAOException</code>.
     *
     * @return - count of news
     * @throws DAOException
     */
    int countNews() throws DAOException;

    /**
     * This method returns all news that meets the search criteria in the parameter.
     * Use <code>SearchQueryBuilder</code> to build the query.
     * Can produce <code>DAOException</code>.
     *
     * @param searchCriteria
     * @return - <code>List</code> of News
     * @throws DAOException
     */
    List<News> getNewsBySearchCriteria(SearchCriteria searchCriteria) throws DAOException;
}
