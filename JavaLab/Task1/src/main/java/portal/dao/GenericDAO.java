package portal.dao;

import portal.dao.exception.DAOException;

import java.io.Serializable;
import java.util.List;

/**
 * Generic data access object interface.
 * This interface represents CRUD operations for all child DAO.
 * Entity will have a type of specific child DAO.
 */
public interface GenericDAO<Entity extends Serializable> {

    /**
     * This method represents CRUD operation insert for some specific entity.
     * Can produce <code>DAOException</code>.
     *
     * @param entity
     * @return <code>Entity</code> - some specific DAO object
     * @throws DAOException
     */
    public Long insert(Entity entity) throws DAOException;

    /**
     * This method represents CRUD operation get for some specific entity.
     * Can produce <code>DAOException</code>.
     *
     * @param entityID
     * @return <code>Entity</code> - some specific DAO object
     * @throws DAOException
     */
    public Entity get(Long entityID) throws DAOException;

    /**
     * This method represents CRUD operation getAll for some specific entity.
     * Can produce <code>DAOException</code>.
     *
     * @return <code>List</code> of entities - List of some specific specific DAO objects
     * @throws DAOException
     */
    public List<Entity> getAll() throws DAOException;

    /**
     * This method represents CRUD operation update for some specific entity.
     * Can produce <code>DAOException</code>.
     *
     * @param entity
     * @throws DAOException
     */
    public void update(Entity entity) throws DAOException;

    /**
     * This method represents CRUD operation delete for some specific entity.
     * Can produce <code>DAOException</code>.
     *
     * @param entity
     * @throws DAOException
     */
    public void delete(Entity entity) throws DAOException;
}
