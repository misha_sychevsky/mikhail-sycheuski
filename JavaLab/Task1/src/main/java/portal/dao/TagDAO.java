package portal.dao;

import portal.dao.exception.DAOException;
import portal.model.Tag;

import java.util.List;

/**
 * Tag data access object interface.
 * This interface is necessary for interaction with a table of tags in the database.
 */
public interface TagDAO extends GenericDAO<Tag> {

    /**
     * This method removes all associations news in the parameter with tags deleting a bunches in the NEWS_TAG table.
     * Can produce <code>DAOException</code>.
     *
     * @param newsId
     * @throws DAOException
     */
    void deleteAssociationsNewsWithTagsByNews(Long newsId) throws DAOException;

    /**
     * This method associates news with tag adding a bunch in the NEWS_TAG table.
     * Can produce <code>DAOException</code>.
     *
     * @param tagId
     * @param newsId
     * @throws DAOException
     */
    void associateNewsWithTag(Long tagId, Long newsId) throws DAOException;

    /**
     * This method associates news with tags adding a bunches in the NEWS_TAG table.
     * Uses <code>Butch</code> to execute update.
     * Can produce <code>DAOException</code>.
     *
     * @param tags
     * @param newsId
     * @throws DAOException
     */
    void associateNewsWithTags(List<Tag> tags, Long newsId) throws DAOException;

    /**
     * This method removes association news with tag deleting a bunch in the NEWS_TAG table.
     * Can produce <code>DAOException</code>.
     *
     * @param tagId
     * @param newsId
     * @throws DAOException
     */
    void deleteAssociationNewsWithTag(Long tagId, Long newsId) throws DAOException;

    /**
     * This method removes associations news with tags deleting a bunches in the NEWS_TAG table.
     * Can produce <code>DAOException</code>.
     *
     * @param tagId
     * @throws DAOException
     */
    void deleteAssociationsNewsWithTags(Long tagId) throws DAOException;

    /**
     * This method returns all tags of the new in parameter.
     * Can produce <code>DAOException</code>.
     *
     * @param newsId
     * @return <code>List</code> of tags
     * @throws DAOException
     */
    List<Tag> getTagsByNews(Long newsId) throws DAOException;
}
