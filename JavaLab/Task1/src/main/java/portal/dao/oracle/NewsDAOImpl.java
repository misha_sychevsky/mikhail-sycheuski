package portal.dao.oracle;

import portal.dao.util.ConnectionManager;
import portal.dao.util.SearchQueryBuilder;
import portal.dao.NewsDAO;
import portal.dao.exception.DAOException;
import portal.model.SearchCriteria;
import portal.model.News;

import java.sql.*;
import java.sql.Date;
import java.util.*;

/**
 * Oracle implementation of the DAO interface for news entity.
 */
public class NewsDAOImpl implements NewsDAO {

    /**
     * Name of the primary key column in NEWS table in the database.
     */
    public static final String ID_COLUMN = "NEWS_ID";

    /**
     * Connection manager class is responsible for binding to database and realising resources.
     */
    private ConnectionManager manager;

    public void setManager(ConnectionManager manager) {

        this.manager = manager;
    }

    public Long insert(News news) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet generatedKeys = null;
        String generatedColumns[] = {ID_COLUMN};

        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(Queries.SQL_INSERT_NEWS, generatedColumns);
            statement.setString(1, news.getTitle());
            statement.setString(2, news.getShortText());
            statement.setString(3, news.getFullText());
            statement.setTimestamp(4, new Timestamp(news.getCreationDate().getTime()));
            statement.setDate(5, new Date(news.getModificationDate().getTime()));
            statement.executeUpdate();

            long id = 0;
            generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                id = generatedKeys.getLong(1);
            }
            return id;
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(generatedKeys, statement, connection);
        }
    }

    public void delete(News news) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(Queries.SQL_DELETE_NEWS);
            statement.setLong(1, news.getNewsId());
            statement.executeUpdate();
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(statement, connection);
        }
    }

    public void update(News news) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(Queries.SQL_UPDATE_NEWS);
            statement.setString(1, news.getTitle());
            statement.setString(2, news.getShortText());
            statement.setString(3, news.getFullText());
            statement.setTimestamp(4, new Timestamp(news.getCreationDate().getTime()));
            statement.setDate(5, new Date(news.getModificationDate().getTime()));
            statement.setLong(6, news.getNewsId());
            statement.executeUpdate();
        }
        catch (SQLException e)
        {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(statement, connection);
        }
    }

    @Override
    public int countNews() throws DAOException {

        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = manager.getConnection();

            statement = connection.createStatement();

            resultSet = statement.executeQuery(Queries.SQL_COUNT_NEWS);
            int count = 0;
            while (resultSet.next()) {
                count = resultSet.getInt(1);
            }
            return count;
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(resultSet, statement, connection);
        }
    }

    @Override
    public News get(Long newsId) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(Queries.SQL_GET_NEWS_BY_ID);
            statement.setLong(1, newsId);

            resultSet = statement.executeQuery();
            News news = null;
            while (resultSet.next()) {
                news = (makeNews(resultSet));
            }
            return news;
        }
        catch (SQLException e)
        {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(resultSet, statement, connection);
        }
    }

    @Override
    public List<News> getNewsBySearchCriteria(SearchCriteria searchCriteria) throws DAOException {

        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        SearchQueryBuilder searchQueryBuilder = new SearchQueryBuilder();
        try {
            connection = manager.getConnection();

            statement = connection.createStatement();

            resultSet = statement.executeQuery(searchQueryBuilder.buildQuery(searchCriteria));
            List<News> news = new ArrayList<>();
            while (resultSet.next()) {
                news.add(makeNews(resultSet));
            }
            return news;
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(resultSet, statement, connection);
        }
    }

    @Override
    public List<News> getAll() throws DAOException {

        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = manager.getConnection();

            statement = connection.createStatement();

            resultSet = statement.executeQuery(Queries.SQL_GET_ALL_NEWS);
            List<News> news = new ArrayList<>();
            while (resultSet.next()) {
                news.add(makeNews(resultSet));
            }
            return news;
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(resultSet, statement, connection);
        }
    }

    /**
     * This method build <code>News</code> object with data from database.
     *
     * @param resultSet
     * @return <code>News</code>
     * @throws SQLException
     */
    private News makeNews(ResultSet resultSet) throws SQLException {

        News news = new News();
        news.setNewsId(resultSet.getLong("NEWS_ID"));
        news.setTitle(resultSet.getString("TITLE"));
        news.setShortText(resultSet.getString("SHORT_TEXT"));
        news.setFullText(resultSet.getString("FULL_TEXT"));
        news.setCreationDate(new java.util.Date(resultSet.getTimestamp("CREATION_DATE").getTime()));
        news.setModificationDate(new java.util.Date(resultSet.getDate("MODIFICATION_DATE").getTime()));
        return news;
    }
}
