package portal.dao.oracle;

import portal.dao.TagDAO;
import portal.dao.exception.DAOException;
import portal.dao.util.ConnectionManager;
import portal.model.Tag;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Oracle implementation of the DAO interface for tag entity.
 */
public class TagDAOImpl implements TagDAO {

    /**
     * Name of the primary key column in TAG table in the database.
     */
    public static final String ID_COLUMN = "TAG_ID";

    /**
     * Connection manager class is responsible for binding to database and realising resources.
     */
    private ConnectionManager manager;

    public void setManager(ConnectionManager manager) {

        this.manager = manager;
    }

    @Override
    public void associateNewsWithTags(List<Tag> tags, Long newsId) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(Queries.SQL_INSERT_NEWS_TAG);
            for (Tag tag : tags) {
                statement.setLong(1, newsId);
                statement.setLong(2, tag.getTagId());
                statement.addBatch();
            }
            statement.executeBatch();
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(statement, connection);
        }
    }

    @Override
    public void associateNewsWithTag(Long tagId, Long newsId) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(Queries.SQL_INSERT_NEWS_TAG);
            statement.setLong(1, newsId);
            statement.setLong(2, tagId);
            statement.executeUpdate();
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(statement, connection);
        }
    }

    @Override
    public void deleteAssociationNewsWithTag(Long tagId, Long newsId) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(Queries.SQL_DELETE_NEWS_TAG);
            statement.setLong(1, newsId);
            statement.setLong(2, tagId);
            statement.executeUpdate();
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(statement, connection);
        }
    }

    @Override
    public void deleteAssociationsNewsWithTagsByNews(Long newsId) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(Queries.SQL_DELETE_NEWS_TAG_BY_NEWS);
            statement.setLong(1, newsId);
            statement.executeUpdate();
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(statement, connection);
        }
    }

    @Override
    public void deleteAssociationsNewsWithTags(Long tagId) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(Queries.SQL_DELETE_NEWS_TAG);
            statement.setLong(1, tagId);
            statement.executeUpdate();
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(statement, connection);
        }
    }

    public Long insert(Tag tag) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet generatedKeys = null;
        String generatedColumns[] = {ID_COLUMN};

        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(Queries.SQL_INSERT_TAG, generatedColumns);
            statement.setString(1, tag.getName());
            statement.executeUpdate();

            generatedKeys = statement.getGeneratedKeys();
            long id = 0;
            if (generatedKeys.next()) {
                id = generatedKeys.getLong(1);
            }
            return id;
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(generatedKeys, statement, connection);
        }
    }

    @Override
    public Tag get(Long tagId) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(Queries.SQL_GET_TAG);
            statement.setLong(1, tagId);

            resultSet = statement.executeQuery();
            Tag tag = null;
            if (resultSet.next()) {
                tag = makeTag(resultSet);
            }
            return tag;
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(resultSet, statement, connection);
        }
    }

    public void delete(Tag tag) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(Queries.SQL_DELETE_TAG);
            statement.setLong(1, tag.getTagId());
            statement.executeUpdate();
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(statement, connection);
        }
    }

    @Override
    public List<Tag> getTagsByNews(Long newsId) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(Queries.SQL_GET_TAGS_BY_NEWS);
            statement.setLong(1, newsId);

            resultSet = statement.executeQuery();
            List<Tag> tags = new ArrayList<>();
            while (resultSet.next()) {
                tags.add(makeTag(resultSet));
            }
            return tags;
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(resultSet, statement, connection);
        }
    }

    public List<Tag> getAll() throws DAOException {

        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = manager.getConnection();

            statement = connection.createStatement();

            resultSet = statement.executeQuery(Queries.SQL_GET_ALL_TAGS);
            List<Tag> tags = new ArrayList<>();
            while (resultSet.next()) {
                tags.add(makeTag(resultSet));
            }
            return tags;
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(resultSet, statement, connection);
        }
    }

    @Override
    public void update(Tag tag) throws DAOException {

        Connection connection = null;
        PreparedStatement statement = null;

        try {
            connection = manager.getConnection();

            statement = connection.prepareStatement(Queries.SQL_UPDATE_TAG);
            statement.setString(1, tag.getName());
            statement.setLong(2, tag.getTagId());
            statement.executeUpdate();
        }
        catch (SQLException e) {
            throw new DAOException(e);
        }
        finally {
            manager.releaseResources(statement, connection);
        }
    }

    /**
     * This method build <code>Tag</code> object with data from database.
     *
     * @param resultSet
     * @return <code>Tag</code>
     * @throws SQLException
     */
    private Tag makeTag(ResultSet resultSet) throws SQLException {

        Tag tag = new Tag();
        tag.setTagId(resultSet.getLong("TAG_ID"));
        tag.setName(resultSet.getString("TAG_NAME"));
        return tag;
    }
}
