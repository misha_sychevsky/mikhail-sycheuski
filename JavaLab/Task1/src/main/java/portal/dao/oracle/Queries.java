package portal.dao.oracle;

/**
 * SQL Queries necessary to work with database.
 * This final class contains queries for all of the DAO.
 */
final class Queries {

    /*
    AUTHOR DAO QUERIES ---------------------------------------------------------------------------------------
     */
    public static final String SQL_GET_ALL_AUTHORS = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM MIKHAIL.AUTHOR";
    public static final String SQL_INSERT_AUTHOR = "INSERT INTO MIKHAIL.AUTHOR (AUTHOR_ID, AUTHOR_NAME, EXPIRED) VALUES (AUTHOR_SEQ.NEXTVAL, ?, ?)";
    public static final String SQL_DELETE_AUTHOR = "DELETE FROM MIKHAIL.AUTHOR WHERE AUTHOR_ID = ?";
    public static final String SQL_UPDATE_AUTHOR = "UPDATE MIKHAIL.AUTHOR SET AUTHOR_NAME = ? WHERE AUTHOR_ID = ?";
    public static final String SQL_GET_AUTHOR_BY_NEWS = "SELECT AUTHOR.AUTHOR_ID, AUTHOR.AUTHOR_NAME, AUTHOR.EXPIRED FROM MIKHAIL.AUTHOR JOIN MIKHAIL.NEWS_AUTHOR ON MIKHAIL.AUTHOR.AUTHOR_ID = MIKHAIL.NEWS_AUTHOR.AUTHOR_ID WHERE NEWS_ID = ?";
    public static final String SQL_GET_AUTHOR_BY_ID = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM MIKHAIL.AUTHOR WHERE AUTHOR_ID = ?";
    public static final String SQL_INSERT_NEWS_AUTHOR = "INSERT INTO MIKHAIL.NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (?, ?)";
    public static final String SQL_DELETE_NEWS_AUTHOR = "DELETE FROM MIKHAIL.NEWS_AUTHOR WHERE NEWS_ID = ?";
    public static final String SQL_DELETE_ALL_NEWS_AUTHOR = "DELETE FROM MIKHAIL.NEWS_AUTHOR WHERE AUTHOR_ID = ?";
    public static final String SQL_SET_AUTHOR_EXPIRED = "UPDATE MIKHAIL.AUTHOR SET EXPIRED = ? WHERE AUTHOR_ID = ?";
    /*
    COMMENT DAO QUERIES ---------------------------------------------------------------------------------------
     */
    public static final String SQL_GET_ALL_COMMENTS = "SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE FROM MIKHAIL.COMMENTS";
    public static final String SQL_INSERT_COMMENT = "INSERT INTO MIKHAIL.COMMENTS (COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE) VALUES (COMMENTS_SEQ.NEXTVAL, ?, ?, ?)";
    public static final String SQL_DELETE_COMMENT = "DELETE FROM MIKHAIL.COMMENTS WHERE COMMENT_ID = ?";
    public static final String SQL_UPDATE_COMMENT = "UPDATE MIKHAIL.COMMENTS SET COMMENT_TEXT = ?, CREATION_DATE = ? WHERE COMMENT_ID = ?";
    public static final String SQL_GET_COMMENTS_BY_NEWS = "SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE FROM MIKHAIL.COMMENTS WHERE NEWS_ID = ?";
    public static final String SQL_GET_COMMENT_BY_ID = "SELECT COMMENT_ID, NEWS_ID, COMMENT_TEXT, CREATION_DATE FROM MIKHAIL.COMMENTS WHERE COMMENT_ID = ?";
    public static final String SQL_DELETE_COMMENTS_BY_NEWS = "DELETE FROM MIKHAIL.COMMENTS WHERE NEWS_ID = ?";
    /*
    NEWS DAO QUERIES ------------------------------------------------------------------------------------------
     */
    public static final String SQL_INSERT_NEWS = "INSERT INTO MIKHAIL.NEWS (NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE) VALUES (NEWS_SEQ.NEXTVAL, ?, ?, ?, ?, ?)";
    public static final String SQL_DELETE_NEWS = "DELETE FROM MIKHAIL.NEWS WHERE NEWS_ID = ?";
    public static final String SQL_UPDATE_NEWS = "UPDATE MIKHAIL.NEWS SET TITLE = ?, SHORT_TEXT = ?, FULL_TEXT = ?, CREATION_DATE = ?, MODIFICATION_DATE = ? WHERE NEWS_ID = ?";
    public static final String SQL_GET_NEWS_BY_ID = "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE FROM MIKHAIL.NEWS WHERE NEWS_ID = ?";
    public static final String SQL_COUNT_NEWS = "SELECT COUNT(*) FROM MIKHAIL.NEWS";
    public static final String SQL_GET_ALL_NEWS = "SELECT NEWS_ID, TITLE, SHORT_TEXT, FULL_TEXT, CREATION_DATE, MODIFICATION_DATE FROM MIKHAIL.NEWS";
    /*
    TAG DAO QUERIES -------------------------------------------------------------------------------------------
    */
    public static final String SQL_GET_ALL_TAGS = "SELECT TAG_ID, TAG_NAME FROM MIKHAIL.TAG";
    public static final String SQL_INSERT_TAG = "INSERT INTO MIKHAIL.TAG (TAG_ID, TAG_NAME) VALUES (TAG_SEQ.NEXTVAL, ?)";
    public static final String SQL_DELETE_TAG = "DELETE FROM MIKHAIL.TAG WHERE TAG_ID = ?";
    public static final String SQL_GET_TAG = "SELECT TAG_ID, TAG_NAME FROM MIKHAIL.TAG WHERE TAG_ID = ?";
    public static final String SQL_GET_TAGS_BY_NEWS = "SELECT TAG.TAG_ID, TAG.TAG_NAME FROM MIKHAIL.TAG JOIN MIKHAIL.NEWS_TAG ON MIKHAIL.TAG.TAG_ID = MIKHAIL.NEWS_TAG.TAG_ID";
    public static final String SQL_UPDATE_TAG = "UPDATE MIKHAIL.TAG SET TAG_NAME = ? WHERE TAG_ID = ?";
    public static final String SQL_INSERT_NEWS_TAG = "INSERT INTO MIKHAIL.NEWS_TAG (NEWS_ID, TAG_ID) VALUES (?, ?)";
    public static final String SQL_DELETE_NEWS_TAG = "DELETE FROM MIKHAIL.NEWS_TAG WHERE TAG_ID = ?";
    public static final String SQL_DELETE_NEWS_TAG_BY_NEWS = "DELETE FROM MIKHAIL.NEWS_TAG WHERE NEWS_ID = ?";
}
