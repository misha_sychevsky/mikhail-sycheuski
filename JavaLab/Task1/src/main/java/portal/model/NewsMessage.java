package portal.model;

import java.io.Serializable;
import java.util.List;

/**
 * News message class.
 * This class represents composite entity which contains the news with it's author, tags and comments.
 */
public class NewsMessage implements Serializable {

    private static final long serialVersionUID = 20150831L;

    private News news;
    private Author author;
    private List<Tag> tags;
    private List<Comment> comments;

    public News getNews() {

        return news;
    }

    public void setNews(News news) {

        this.news = news;
    }

    public Author getAuthor() {

        return author;
    }

    public void setAuthor(Author author) {

        this.author = author;
    }

    public List<Comment> getComments() {

        return comments;
    }

    public void setComments(List<Comment> comments) {

        this.comments = comments;
    }

    public List<Tag> getTags() {

        return tags;
    }

    public void setTags(List<Tag> tags) {

        this.tags = tags;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        NewsMessage that = (NewsMessage) o;

        if (news != null ? !news.equals(that.news) : that.news != null) {
            return false;
        }
        if (author != null ? !author.equals(that.author) : that.author != null) {
            return false;
        }
        if (tags != null ? !tags.equals(that.tags) : that.tags != null) {
            return false;
        }
        if (comments != null ? !comments.equals(that.comments) : that.comments != null) {
            return false;
        }

        return true;

    }

    @Override
    public int hashCode() {

        int result = 17;
        result = 31 * result + (news != null ? news.hashCode() : 0);
        result = 31 * result + (author != null ? author.hashCode() : 0);
        result = 31 * result + (tags != null ? tags.hashCode() : 0);
        result = 31 * result + (comments != null ? comments.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {

        return "NewsMessage[" +
                "news=" + news +
                ", author=" + author +
                ", tagsCount=" + tags.size() +
                ", commentsCount=" + comments.size() +
                ']';
    }
}
