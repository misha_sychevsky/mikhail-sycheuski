package portal.model;

import java.io.Serializable;

/**
 * Tag entity transfer object class.
 */
public class Tag implements Serializable {

    private static final long serialVersionUID = 20150831L;

    private Long tagId;
    private String name;

    public Long getTagId() {

        return tagId;
    }

    public void setTagId(Long tagId) {

        this.tagId = tagId;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Tag tag = (Tag) o;

        if (tagId != null ? !tagId.equals(tag.tagId) : tag.tagId != null) {
            return false;
        }
        return !(name != null ? !name.equals(tag.name) : tag.name != null);

    }

    @Override
    public int hashCode() {

        int result = 17;
        result = 31 * result + (tagId != null ? tagId.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {

        return "Tag[" +
                "id=" + tagId +
                ", name=" + name +
                ']';
    }
}
