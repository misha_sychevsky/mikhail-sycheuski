package portal.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;
import portal.dao.AuthorDAO;
import portal.dao.CommentDAO;
import portal.dao.NewsDAO;
import portal.dao.TagDAO;
import portal.model.News;
import portal.model.SearchCriteria;
import portal.model.Tag;
import portal.service.implementation.NewsServiceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * News service Mockito test class.
 * Class <code>NewsServiceTest</code> contains Mockito unit tests for all <code>NewsService</code> operations.
 */
@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(locations = {"/Spring-Context.xml"})
public class NewsServiceTest {

    /**
     * Mock object, which will be inject into NewsServiceImpl class.
     */
    @Mock
    NewsDAO newsDAO;
    /**
     * Mock object, which will be inject into NewsServiceImpl class.
     */
    @Mock
    TagDAO tagDAO;
    /**
     * Mock object, which will be inject into NewsServiceImpl class.
     */
    @Mock
    AuthorDAO authorDAO;
    /**
     * Mock object, which will be inject into NewsServiceImpl class.
     */
    @Mock
    CommentDAO commentDAO;
    /**
     * Service layer object, which we will testing.
     */
    @InjectMocks
    NewsServiceImpl newsService;

    @Test
    public void addTag() throws Exception {

        News news = makeNews();
        when(newsDAO.insert(news)).thenReturn(1L);

        Long newsId = newsService.addNews(news);
        verify(newsDAO, times(1)).insert(news);
        assertEquals(newsId, Long.valueOf(1L));
    }

    @Test
    public void deleteNews() throws Exception {

        News news = makeNews();
        Long newsId = news.getNewsId();

        newsService.deleteNews(news);
        verify(authorDAO, times(1)).deleteAssociationNewsWithAuthor(newsId);
        verify(tagDAO, times(1)).deleteAssociationsNewsWithTagsByNews(newsId);
        verify(commentDAO, times(1)).deleteCommentsByNews(newsId);
        verify(newsDAO, times(1)).delete(news);
    }

    @Test
    public void editNews() throws Exception {

        News news = makeNews();

        newsService.editNews(news);
        verify(newsDAO, times(1)).update(news);
    }

    @Test
    public void getNewsById() throws Exception {

        News news = makeNews();
        Long newsId = news.getNewsId();
        String title = news.getTitle();
        String shortText = news.getShortText();
        String fullText = news.getFullText();
        Date creationDate = news.getCreationDate();
        Date modificationDate = news.getModificationDate();


        when(newsDAO.get(newsId)).thenReturn(news);
        news = newsService.getNewsById(newsId);

        verify(newsDAO, times(1)).get(newsId);
        assertEquals(newsId, news.getNewsId());
        assertEquals(title, news.getTitle());
        assertEquals(shortText, news.getShortText());
        assertEquals(fullText, news.getFullText());
        assertEquals(creationDate, news.getCreationDate());
        assertEquals(modificationDate, news.getModificationDate());
    }

    @Test
    public void countAllNews() throws Exception {

        int expectedNewsCount = 3;

        when(newsDAO.countNews()).thenReturn(3);
        int newsCount = newsService.countAllNews();

        verify(newsDAO, times(1)).countNews();
        assertEquals(expectedNewsCount, newsCount);
    }

    @Test
    public void searchNewsBySearchCriteria() throws Exception {

        SearchCriteria searchCriteria = new SearchCriteria();

        when(newsDAO.getNewsBySearchCriteria(searchCriteria)).thenReturn(new ArrayList<News>());
        List<News> news = newsService.searchNewsBySearchCriteria(searchCriteria);

        verify(newsDAO, times(1)).getNewsBySearchCriteria(searchCriteria);
        assertNotNull(news);
    }

    @Test
    public void addTagToNews() throws Exception {

        Long tagId = 1L;
        Long newsId = 2L;

        newsService.addTagToNews(tagId, newsId);

        verify(tagDAO, times(1)).associateNewsWithTag(tagId, newsId);
    }

    @Test
    public void addTagsToNews() throws Exception {

        final Tag tag1 = new Tag();
        tag1.setTagId(1L);
        final Tag tag2 = new Tag();
        tag2.setTagId(2L);
        Long newsId = 2L;

        List<Tag> tags = new ArrayList<Tag>() {{
            add(tag1);
            add(tag2);
        }};

        newsService.addTagsToNews(tags, newsId);

        verify(tagDAO, times(1)).associateNewsWithTags(tags, newsId);
    }

    @Test
    public void deleteAuthorFromAllNews() throws Exception {

        Long authorId = 1L;

        newsService.deleteAuthorFromAllNews(authorId);

        verify(authorDAO, times(1)).deleteAllAssociationsNewsWithAuthor(authorId);
    }

    @Test
    public void addAuthorToNews() throws Exception {

        Long authorId = 1L;
        Long newsId = 1L;

        newsService.addAuthorToNews(newsId, authorId);

        verify(authorDAO, times(1)).associateNewsWithAuthor(newsId, authorId);
    }

    private News makeNews() {

        News news = new News();
        news.setNewsId(1L);
        news.setTitle("Title");
        news.setShortText("Short Text");
        news.setFullText("Full Text");
        news.setCreationDate(new Date());
        news.setModificationDate(new Date());
        return news;
    }
}
