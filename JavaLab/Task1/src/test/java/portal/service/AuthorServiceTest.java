package portal.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;
import portal.dao.AuthorDAO;
import portal.model.Author;
import portal.service.implementation.AuthorServiceImpl;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * News service Mockito test class.
 * Class <code>AuthorServiceTest</code> contains Mockito unit tests for all <code>AuthorService</code> operations.
 */
@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration(locations = {"/Spring-Context.xml"})
public class AuthorServiceTest {

    /**
     * Mock object, which will be inject into AuthorServiceImpl class.
     */
    @Mock
    AuthorDAO authorDAO;
    /**
     * Service layer object, which we will testing.
     */
    @InjectMocks
    AuthorServiceImpl authorService;

    @Test
    public void addAuthor() throws Exception {

        Author author = makeAuthor();
        when(authorDAO.insert(author)).thenReturn(1L);

        Long authorId = authorService.addAuthor(author);
        verify(authorDAO, times(1)).insert(author);
        assertEquals(authorId, Long.valueOf(1L));
    }

    @Test
    public void editAuthor() throws Exception {

        Author author = makeAuthor();

        authorService.editAuthor(author);
        verify(authorDAO, times(1)).update(author);
    }

    @Test
    public void deleteAuthor() throws Exception {

        Author author = makeAuthor();
        author.setAuthorId(1L);

        authorService.deleteAuthor(author);
        verify(authorDAO, times(1)).delete(author);
    }

    @Test
    public void getAuthorById() throws Exception {

        Author author = makeAuthor();
        Long authorId = author.getAuthorId();
        String authorName = author.getName();

        when(authorDAO.get(authorId)).thenReturn(author);
        author = authorService.getAuthorById(authorId);

        verify(authorDAO, times(1)).get(authorId);
        assertEquals(authorId, author.getAuthorId());
        assertEquals(authorName, author.getName());
    }

    @Test
    public void viewTheListOfAuthors() throws Exception {

        when(authorDAO.getAll()).thenReturn(new ArrayList<Author>());

        List<Author> authors = authorService.viewTheListOfAuthors();
        verify(authorDAO, times(1)).getAll();
        assertNotNull(authors);
    }

    @Test
    public void getAuthorByNews() throws Exception {

        Long newsId = 2L;
        Author author = makeAuthor();
        Long authorId = author.getAuthorId();
        String authorName = author.getName();

        when(authorDAO.getAuthorByNews(newsId)).thenReturn(author);
        author = authorService.getAuthorByNews(newsId);

        verify(authorDAO, times(1)).getAuthorByNews(newsId);
        assertEquals(authorId, author.getAuthorId());
        assertEquals(authorName, author.getName());
    }

    @Test
    public void setAuthorExpired() throws Exception {

        Long authorId = 1L;

        authorService.setAuthorExpired(authorId);
        verify(authorDAO, times(1)).setAuthorExpired(authorId);
    }

    private Author makeAuthor() {

        Author author = new Author();
        author.setAuthorId(1L);
        author.setName("name");
        return author;
    }
}
