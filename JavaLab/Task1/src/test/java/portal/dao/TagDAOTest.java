package portal.dao;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import portal.model.Tag;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;

/**
 * Tag DAO tests class.
 * Class <code>TagDAOTest</code> contains unit tests for all <code>TagDAO</code> operations.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class, TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class})
@ContextConfiguration(locations = {"/Spring-Context.xml"})
@DatabaseSetup(value = "/database/TestDataSet.xml")
public class TagDAOTest {

    public static final Logger LOGGER = Logger.getLogger(AuthorDAOTest.class);

    /**
     * Reference on TagDAO class, to use it all public methods.
     */
    @Autowired
    private TagDAO tagDAO;

    @Test
    public void create() throws Exception {

        Long tagId;
        String tagName = "New Tag";

        Tag tag = new Tag();
        tag.setName(tagName);

        tagId = tagDAO.insert(tag);
        tag = tagDAO.get(tagId);

        assertEquals(tag.getTagId(), tagId);
        assertEquals(tag.getName(), tagName);

        LOGGER.info("The tag was successful created.");
    }

    @Test
    public void update() throws Exception {

        Long tagId = 1L;
        String tagName = "Updated Tag";

        Tag tag = new Tag();
        tag.setTagId(tagId);
        tag.setName(tagName);

        tagDAO.update(tag);
        tag = tagDAO.get(tagId);

        assertEquals(tag.getTagId(), tagId);
        assertEquals(tag.getName(), tagName);

        LOGGER.info("The tag was successful updated.");
    }

    @Test
    public void read() throws Exception {

        Long tagId = 1L;
        String tagName = "Tag 1";

        Tag tag = tagDAO.get(tagId);

        assertEquals(tag.getTagId(), tagId);
        assertEquals(tag.getName(), tagName);

        LOGGER.info("The tag was successful read.");
    }

    @Test
    public void readAll() throws Exception {

        int expectedSize = 3;
        ArrayList<String> names = new ArrayList<String>() {{
            add("Tag 3");
            add("Tag 1");
            add("Tag 2");
        }};
        ;

        List<Tag> tagList = tagDAO.getAll();

        assertEquals(tagList.size(), expectedSize);
        for (int count = 0; count < expectedSize; count++) {

            assertEquals(tagList.get(count).getName(), names.get(count));
        }

        LOGGER.info("The tags was successful read.");
    }

    @Test
    public void delete() throws Exception {

        Long tagId = 2L;

        Tag tag = tagDAO.get(tagId);
        assertNotNull(tag);

        tagDAO.deleteAssociationsNewsWithTags(tagId);
        tagDAO.delete(tag);
        tag = tagDAO.get(tagId);
        assertNull(tag);

        LOGGER.info("The tag was successful deleted.");
    }
}
