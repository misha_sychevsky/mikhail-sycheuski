package portal.dao;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import portal.model.SearchCriteria;
import portal.model.News;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;

/**
 * News DAO tests class.
 * Class <code>NewsDAOTest</code> contains unit tests for all <code>NewsDAO</code> operations.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class, TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class})
@ContextConfiguration(locations = {"/Spring-Context.xml"})
@DatabaseSetup(value = "/database/TestDataSet.xml")
public class NewsDAOTest {

    public static final Logger LOGGER = Logger.getLogger(AuthorDAOTest.class);

    /**
     * Reference on NewsDAO class, to use it all public methods.
     */
    @Autowired
    private NewsDAO newsDAO;
    /**
     * Reference on TagDAO class, to use it all public methods.
     */
    @Autowired
    private TagDAO tagDAO;
    /**
     * Reference on AuthorDAO class, to use it all public methods.
     */
    @Autowired
    private AuthorDAO authorDAO;
    /**
     * Reference on CommentDAO class, to use it all public methods.
     */
    @Autowired
    private CommentDAO commentDAO;

    @Test
    public void create() throws Exception {

        Long newsId;
        String title = "New news";
        String shortText = "asfhasopifpasifpa";
        String fullText = "aklsfndlkasnflasnfliasfnil";
        Timestamp creationDate = new Timestamp(new Date().getTime());
        java.sql.Date modificationDate = new java.sql.Date(new Date().getTime());

        News news = new News();
        news.setTitle(title);
        news.setShortText(shortText);
        news.setFullText(fullText);
        news.setCreationDate(creationDate);
        news.setModificationDate(modificationDate);

        newsId = newsDAO.insert(news);
        news = newsDAO.get(newsId);

        assertEquals(news.getNewsId(), newsId);
        assertEquals(news.getTitle(), title);
        assertEquals(news.getShortText(), shortText);
        assertEquals(news.getFullText(), fullText);
        assertEquals(news.getCreationDate(), creationDate);

        LOGGER.info("The news was successful created.");
    }

    @Test
    public void update() throws Exception {

        Long newsId = 2L;
        String title = "Updated news";
        String shortText = "asfhasopifpasifpa";
        String fullText = "aklsfndlkasnflasnfliasfnil";
        Timestamp creationDate = new Timestamp(new Date().getTime());
        java.sql.Date modificationDate = new java.sql.Date(new Date().getTime());

        News news = new News();
        news.setNewsId(newsId);
        news.setTitle(title);
        news.setShortText(shortText);
        news.setFullText(fullText);
        news.setCreationDate(creationDate);
        news.setModificationDate(modificationDate);

        newsDAO.update(news);
        news = newsDAO.get(newsId);

        assertEquals(news.getNewsId(), newsId);
        assertEquals(news.getTitle(), title);
        assertEquals(news.getShortText(), shortText);
        assertEquals(news.getFullText(), fullText);
        assertEquals(news.getCreationDate(), creationDate);

        LOGGER.info("The news was successful updated.");
    }

    @Test
    public void read() throws Exception {

        Long newsId = 1L;
        String title = "T_N";
        String shortText = "Tohny News";
        String fullText = "News1";

        News news = newsDAO.get(newsId);

        assertEquals(news.getNewsId(), newsId);
        assertEquals(news.getTitle(), title);
        assertEquals(news.getShortText(), shortText);
        assertEquals(news.getFullText(), fullText);

        LOGGER.info("The news was successful read.");
    }

    @Test
    public void delete() throws Exception {

        Long newsId = 2L;

        News news = newsDAO.get(newsId);
        assertNotNull(news);

        authorDAO.deleteAssociationNewsWithAuthor(news.getNewsId());
        tagDAO.deleteAssociationsNewsWithTagsByNews(news.getNewsId());
        commentDAO.deleteCommentsByNews(news.getNewsId());
        newsDAO.delete(news);
        news = newsDAO.get(newsId);
        assertNull(news);

        LOGGER.info("The news was successful deleted.");
    }

    @Test
    public void countNews() throws Exception {

        int expectedCount = 3;
        int count = newsDAO.countNews();
        assertEquals(expectedCount, count);

        LOGGER.info("The news was successful counted.");
    }


    @Test
    public void getNewsBySearchCriteria() throws Exception {

        final Long tagId1 = 1L;
        final Long tagId2 = 2L;
        final Long tagId3 = 3L;
        final Long authorId = 3L;
        final Long expectedNewsId = 3L;

        List<Long> tagIds = new ArrayList<Long>() {{
            add(tagId1);
            add(tagId2);
            add(tagId3);
        }};

        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setAuthorId(authorId);
        searchCriteria.setTagIds(tagIds);

        List<News> newsList = newsDAO.getNewsBySearchCriteria(searchCriteria);
        assertEquals(expectedNewsId, newsList.get(0).getNewsId());

        LOGGER.info("The news was successful read by search criteria.");
    }
}
