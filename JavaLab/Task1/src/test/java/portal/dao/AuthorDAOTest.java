package portal.dao;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import portal.dao.exception.DAOException;
import portal.model.Author;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;

/**
 * Author DAO tests class.
 * Class <code>AuthorDAOTest</code> contains unit tests for all <code>AuthorDAO</code> operations.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class, TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class})
@ContextConfiguration(locations = {"/Spring-Context.xml"})
@DatabaseSetup(value = "/database/TestDataSet.xml")
public class AuthorDAOTest {

    public static final Logger LOGGER = Logger.getLogger(AuthorDAOTest.class);

    /**
     * Reference on AuthorDAO class, to use it all public methods.
     */
    @Autowired
    AuthorDAO authorDAO;

    @Test
    public void create() throws Exception {

        String authorName = "New user";

        Author author = new Author();
        author.setName(authorName);
        author.setExpired(new Date());
        Long authorId = authorDAO.insert(author);
        author = authorDAO.get(authorId);

        assertEquals(author.getName(), authorName);
        assertEquals(author.getAuthorId(), authorId);

        LOGGER.info("The author was successful created.");
    }

    @Test
    public void update() throws Exception {

        Long authorId = 2L;
        String authorName = "Tay";

        Author author = new Author();
        author.setName(authorName);
        author.setAuthorId(authorId);

        authorDAO.update(author);
        author = authorDAO.get(authorId);

        assertEquals(author.getAuthorId(), authorId);
        assertEquals(author.getName(), authorName);

        LOGGER.info("The author was successful updated.");
    }

    @Test
    public void read() throws Exception {

        Long authorId = 1L;
        String authorName = "Tohny";

        Author author = authorDAO.get(authorId);

        assertEquals(author.getName(), authorName);
        assertEquals(author.getAuthorId(), authorId);

        LOGGER.info("The author was successful read.");
    }

    @Test
    public void readAll() throws Exception {

        int expectedSize = 3;
        ArrayList<String> names = new ArrayList<String>() {{
            add("Kate");
            add("Tohny");
            add("Maria");
        }};
        ;

        List<Author> authorList = authorDAO.getAll();

        assertEquals(authorList.size(), expectedSize);
        for (int count = 0; count < expectedSize; count++) {

            assertEquals(authorList.get(count).getName(), names.get(count));
        }

        LOGGER.info("The authors was successful read.");
    }

    @Test
    public void delete() throws Exception {

        Long authorId = 2L;

        Author author = authorDAO.get(authorId);
        assertNotNull(author);

        authorDAO.deleteAllAssociationsNewsWithAuthor(authorId);
        authorDAO.delete(author);
        author = authorDAO.get(authorId);
        assertNull(author);

        LOGGER.info("The author was successful deleted.");
    }

    @Test
    public void setExpired() throws Exception {

        Long authorId = 2L;

        authorDAO.setAuthorExpired(authorId);
        Author author = authorDAO.get(authorId);

        assertEquals(author.getAuthorId(), authorId);
        assertNotNull(author.getExpired());

        LOGGER.info("The author was successful set expired.");
    }

    @Test(expected = DAOException.class)
    public void updateWithException() throws DAOException {

        Long authorId = 2L;
        String authorName = "Tayaddddddddddddddddddddddd" +
                "ddsssssssssssssssssssssssssssssssssssssssssssaaaaaaaaa" +
                "asssssssssssssssssssssssssssssssssssssssssssssssssssss" +
                "ssssssssssssssssssssssssssssssssssssssssssssssssssssss";

        Author author = new Author();
        author.setName(authorName);
        author.setAuthorId(authorId);

        authorDAO.update(author);
        author = authorDAO.get(authorId);

        assertEquals(author.getAuthorId(), authorId);
        assertEquals(author.getName(), authorName);

        LOGGER.info("DAO exception was occurred.");
    }
}
