package portal.dao;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import portal.dao.exception.DAOException;
import portal.model.Comment;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;

/**
 * Comment DAO tests class.
 * Class <code>CommentDAOTest</code> contains unit tests for all <code>CommentDAO</code> operations.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class, TransactionalTestExecutionListener.class, DbUnitTestExecutionListener.class})
@ContextConfiguration(locations = {"/Spring-Context.xml"})
@DatabaseSetup(value = "/database/TestDataSet.xml")
public class CommentDAOTest {

    public static final Logger LOGGER = Logger.getLogger(AuthorDAOTest.class);

    /**
     * Reference on CommentDAO class, to use it all public methods.
     */
    @Autowired
    CommentDAO commentDAO;

    @Test
    public void create() throws Exception {

        Long commentId;
        Long newsId = 3L;
        String commentText = "Comment for new News";
        Timestamp commentDate = new Timestamp(new Date().getTime());

        Comment comment = new Comment();
        comment.setNewsId(newsId);
        comment.setCommentText(commentText);
        comment.setCreationDate(commentDate);

        commentId = commentDAO.insert(comment);
        comment = commentDAO.get(commentId);

        assertEquals(comment.getCommentId(), commentId);
        assertEquals(comment.getNewsId(), newsId);
        assertEquals(comment.getCommentText(), commentText);
        assertEquals(comment.getCreationDate(), commentDate);

        LOGGER.info("The comment was successful created.");
    }

    @Test(expected = DAOException.class)
    public void createWithException() throws DAOException {

        Long commentId;
        Long newsId = 4L;
        String commentText = "Comment for new News";
        Timestamp commentDate = new Timestamp(new Date().getTime());

        Comment comment = new Comment();
        comment.setNewsId(newsId);
        comment.setCommentText(commentText);
        comment.setCreationDate(commentDate);

        commentId = commentDAO.insert(comment);
        comment = commentDAO.get(commentId);

        assertEquals(comment.getCommentId(), commentId);
        assertEquals(comment.getNewsId(), newsId);
        assertEquals(comment.getCommentText(), commentText);
        assertEquals(comment.getCreationDate(), commentDate);

        LOGGER.info("DAO exception occurred.");
    }

    @Test
    public void update() throws Exception {

        Long commentId = 1L;
        Long newsId = 1L;
        String commentText = "Updated Comment";
        Timestamp commentDate = new Timestamp(new Date().getTime());

        Comment comment = new Comment();
        comment.setCommentId(commentId);
        comment.setNewsId(newsId);
        comment.setCommentText(commentText);
        comment.setCreationDate(commentDate);

        commentDAO.update(comment);
        comment = commentDAO.get(commentId);

        assertEquals(comment.getCommentId(), commentId);
        assertEquals(comment.getNewsId(), newsId);
        assertEquals(comment.getCommentText(), commentText);
        assertEquals(comment.getCreationDate(), commentDate);

        LOGGER.info("The comment was successful updated.");
    }

    @Test
    public void read() throws Exception {

        Long commentId = 1L;
        Long newsId = 1L;
        String commentText = "Comment for News 1";

        Comment comment = commentDAO.get(commentId);

        assertEquals(comment.getCommentId(), commentId);
        assertEquals(comment.getNewsId(), newsId);
        assertEquals(comment.getCommentText(), commentText);

        LOGGER.info("The comment was successful read.");
    }

    @Test
    public void readAll() throws Exception {

        int expectedSize = 6;
        Long newsId = 3L;
        String commentText = "Comment for News 3";

        List<Comment> commentList = commentDAO.getAll();

        assertEquals(commentList.size(), expectedSize);
        assertEquals(commentList.get(2).getNewsId(), newsId);
        assertEquals(commentList.get(2).getCommentText(), commentText);

        LOGGER.info("The comments was successful read.");
    }

    @Test
    public void delete() throws Exception {

        long commentId = 2;

        Comment comment = commentDAO.get(commentId);
        assertNotNull(comment);

        commentDAO.delete(comment);
        comment = commentDAO.get(commentId);
        assertNull(comment);

        LOGGER.info("The comment was successful deleted.");
    }

    @Test
    public void insertAll() throws Exception {

        int expectedSize = 3;

        Comment comment1 = new Comment();
        comment1.setNewsId(1L);
        comment1.setCreationDate(new Date());
        comment1.setCommentText("comment text1");
        Comment comment2 = new Comment();
        comment2.setNewsId(1L);
        comment2.setCreationDate(new Date());
        comment2.setCommentText("comment text2");
        List<Comment> comments = new ArrayList<>();
        comments.add(comment1);
        comments.add(comment2);

        commentDAO.insertAll(comments);
        comments = commentDAO.getCommentsByNews(1L);
        assertEquals(expectedSize, comments.size());

        LOGGER.info("The comments was successful created.");
    }
}
