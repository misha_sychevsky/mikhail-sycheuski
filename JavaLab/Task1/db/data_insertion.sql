/* INSERT SCRIPT FOR TABLE MIKHAIL.AUTHOR*/
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (1, 'Jimmy');
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (2, 'Arny');
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (3, 'Mathew');
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (4, 'Harrison');
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (5, 'Hernandez');
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (6, 'Clark');
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (7, 'James');
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (8, 'Willis');
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (9, 'James');
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (10, 'Williamson');
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (11, 'Cooper');
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (12, 'Armstrong');
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (13, 'Arnold');
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (14, 'Richards');
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (15, 'Mcdonald');
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (16, 'Warren');
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (17, 'Frazier');
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (18, 'Wagner');
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (19, 'Peterson');
INSERT INTO AUTHOR (AUTHOR_ID, AUTHOR_NAME) VALUES (20, 'Reyes');



/*INSERT SCRIPT FOR TABLE MIKHAIL.TAG */
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (1, 'Architecture');
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (2, 'Science');
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (3, 'Fashion');
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (4, 'Cars');
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (5, 'News');
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (6, 'Party');
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (7, 'World');
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (8, 'Space');
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (9, 'Analytics');
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (10, 'Predictions');
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (11, 'Food');
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (12, 'Greenhouse effect');
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (13, 'Discoveries');
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (14, 'Psychology');
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (15, 'Civilization');
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (16, 'World War III');
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (17, 'Security');
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (18, 'Web- technology');
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (19, 'Interviews');
INSERT INTO TAG (TAG_ID, TAG_NAME) VALUES (20, 'Conference');


/*INSERT SCRIPT FOR TABLE MIKHAIL.NEWS */
INSERT INTO NEWS (NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE, MODIFICATION_DATE) 
VALUES (1, 'quis augue luctus tincidunt nulla mollis molestie lorem quisque ut erat curabitur', 
'sed sagittis nam congue risus semper porta volutpat quam pede lobortis ligula sit amet eleifend 
pede libero quis orci nullam molestie nibh in lectus pellentesque at nulla suspendisse potenti 
cras in purus eu magna vulputate luctus cum sociis natoque penatibus et magnis dis parturient 
montes nascetur ridiculus mus', 'Nurse',timestamp '2013-01-15 22:11:35', date '2014-04-13');

INSERT INTO NEWS (NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE, MODIFICATION_DATE)
VALUES (2, 'sit amet sapien dignissim vestibulum vestibulum ante ipsum primis in faucibus orci',
'felis sed lacus morbi sem mauris laoreet ut rhoncus aliquet pulvinar sed nisl nunc rhoncus dui 
vel sem sed sagittis nam congue risus semper porta volutpat quam pede lobortis ligula sit amet 
eleifend pede libero quis orci nullam molestie nibh in lectus pellentesque', 'Recruiter',
timestamp '2013-01-15 22:11:35', date '2014-04-13');

INSERT INTO NEWS (NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE, MODIFICATION_DATE) 
VALUES (3, 'luctus et ultrices posuere cubilia curae donec pharetra magna vestibulum aliquet 
ultrices', 'justo sit amet sapien dignissim vestibulum vestibulum ante ipsum primis in faucibus
 orci luctus et ultrices posuere cubilia curae nulla dapibus dolor vel est donec odio justo 
sollicitudin ut suscipit a feugiat et eros vestibulum ac est lacinia nisi venenatis', 
'Geologist I',timestamp '2013-01-15 22:11:35', date '2014-04-13');

INSERT INTO NEWS (NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE, MODIFICATION_DATE) 
VALUES (4, 'pretium iaculis diam erat fermentum justo nec condimentum neque sapien placerat',
'a nibh in quis justo maecenas rhoncus aliquam lacus morbi quis tortor id nulla ultrices aliquet
 maecenas leo odio condimentum id luctus nec molestie sed justo pellentesque viverra pede ac diam
 cras pellentesque', 'Recruiting Manager',timestamp '2013-01-15 22:11:35', date '2014-04-13');
 
INSERT INTO NEWS (NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE, MODIFICATION_DATE) 
VALUES (5, 'mattis odio donec vitae nisi nam ultrices libero non mattis pulvinar', 'id ornare 
imperdiet sapien urna pretium nisl ut volutpat sapien arcu sed augue aliquam erat volutpat in 
congue etiam justo etiam pretium iaculis justo in hac habitasse platea dictumst etiam', 
'Associate Professor',timestamp '2013-01-15 22:11:35', date '2014-04-13');

INSERT INTO NEWS (NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE, MODIFICATION_DATE) 
VALUES (6, 'porta volutpat erat quisque erat eros viverra eget congue eget semper rutrum',
'amet lobortis sapien sapien non mi integer ac neque duis bibendum morbi non quam nec dui luctus
 rutrum nulla tellus in sagittis dui vel nisl duis ac nibh fusce lacus purus', 'Environmental 
Tech',timestamp '2013-01-15 22:11:35', date '2014-04-13');

INSERT INTO NEWS (NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE, MODIFICATION_DATE)
VALUES (7, 'rhoncus dui vel sem sed sagittis nam congue risus semper', 'phasellus in felis
 donec semper sapien a libero nam dui proin leo odio porttitor id consequat in consequat ut
 nulla sed accumsan felis ut at dolor quis odio consequat varius integer ac leo pellentesque',
 'Payment Adjustment Coordinator',timestamp '2013-01-15 22:11:35', date '2014-04-13');
 
INSERT INTO NEWS (NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE, MODIFICATION_DATE)
VALUES (8, 'vivamus vestibulum sagittis sapien cum sociis natoque penatibus et magnis', 
'rutrum nulla tellus in sagittis dui vel nisl duis ac nibh fusce lacus purus aliquet at feugiat
 non pretium quis lectus suspendisse potenti in eleifend quam a odio in hac habitasse platea 
dictumst maecenas ut massa quis augue luctus tincidunt nulla mollis molestie lorem quisque', 
'Speech Pathologist',timestamp '2013-01-15 22:11:35', date '2014-04-13');

INSERT INTO NEWS (NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE, MODIFICATION_DATE)
VALUES (9, 'dignissim vestibulum vestibulum ante ipsum primis in faucibus orci luctus et 
ultrices', 'dapibus duis at velit eu est congue elementum in hac habitasse platea dictumst morbi
 vestibulum velit id pretium iaculis diam erat fermentum justo nec condimentum neque sapien
 placerat ante nulla justo', 'Geological Engineer',timestamp '2013-01-15 22:11:35',
 date '2014-04-13');
 
INSERT INTO NEWS (NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE, MODIFICATION_DATE) 
VALUES (10, 'nunc rhoncus dui vel sem sed sagittis nam congue risus semper', 'augue aliquam 
erat volutpat in congue etiam justo etiam pretium iaculis justo in hac habitasse platea dictumst
 etiam faucibus cursus urna ut tellus nulla ut erat id mauris vulputate elementum nullam', 
 'Recruiter',timestamp '2013-01-15 22:11:35', date '2014-04-13');
 
INSERT INTO NEWS (NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE, MODIFICATION_DATE)
VALUES (11, 'congue etiam justo etiam pretium iaculis justo in hac habitasse platea', 'maecenas
 ut massa quis augue luctus tincidunt nulla mollis molestie lorem quisque ut erat curabitur 
gravida nisi at nibh in hac habitasse platea dictumst aliquam augue quam sollicitudin vitae
 consectetuer eget rutrum at lorem integer tincidunt ante vel ipsum praesent blandit lacinia',
 'Programmer Analyst III',timestamp '2013-01-15 22:11:35', date '2014-04-13');
 
INSERT INTO NEWS (NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE, MODIFICATION_DATE) 
VALUES (12, 'pede morbi porttitor lorem id ligula suspendisse ornare consequat lectus in est',
'ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae mauris viverra 
diam vitae quam suspendisse potenti nullam porttitor lacus at turpis donec posuere metus vitae
 ipsum aliquam non mauris morbi non lectus aliquam sit amet diam', 'Senior Developer',
 timestamp '2013-01-15 22:11:35', date '2014-04-13');
 
INSERT INTO NEWS (NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE, MODIFICATION_DATE) 
VALUES (13, 'sapien arcu sed augue aliquam erat volutpat in congue etiam justo etiam', 
'etiam faucibus cursus urna ut tellus nulla ut erat id mauris vulputate elementum nullam varius
 nulla facilisi cras non velit nec nisi vulputate nonummy maecenas tincidunt lacus at velit
 vivamus vel nulla eget eros elementum pellentesque', 'Recruiter',
 timestamp '2013-01-15 22:11:35', date '2014-04-13');
 
INSERT INTO NEWS (NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE, MODIFICATION_DATE)
VALUES (14, 'porta volutpat erat quisque erat eros viverra eget congue eget semper rutrum',
'rhoncus dui vel sem sed sagittis nam congue risus semper porta volutpat quam pede lobortis
 ligula sit amet eleifend pede libero quis orci nullam molestie nibh in lectus pellentesque
 at nulla suspendisse potenti cras in purus eu magna vulputate luctus cum sociis natoque
 penatibus et magnis dis', 'Junior Executive',timestamp '2013-01-15 22:11:35', date '2014-04-13');
 
INSERT INTO NEWS (NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE, MODIFICATION_DATE)
VALUES (15, 'a suscipit nulla elit ac nulla sed vel enim sit amet', 'sit amet turpis elementum
 ligula vehicula consequat morbi a ipsum integer a nibh in quis justo maecenas rhoncus aliquam
 lacus morbi quis tortor id nulla ultrices aliquet maecenas leo odio condimentum id luctus nec
 molestie sed justo pellentesque viverra pede ac diam cras pellentesque volutpat dui maecenas 
tristique', 'General Manager',timestamp '2013-01-15 22:11:35', date '2014-04-13');

INSERT INTO NEWS (NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE, MODIFICATION_DATE) 
VALUES (16, 'at feugiat non pretium quis lectus suspendisse potenti in eleifend', 'ultrices 
mattis odio donec vitae nisi nam ultrices libero non mattis pulvinar nulla pede ullamcorper 
augue a suscipit nulla elit ac nulla sed vel enim sit amet nunc viverra dapibus nulla suscipit 
ligula in lacus curabitur at ipsum ac tellus semper interdum mauris ullamcorper purus sit amet',
'Chief Design Engineer',timestamp '2013-01-15 22:11:35', date '2014-04-13');

INSERT INTO NEWS (NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE, MODIFICATION_DATE)
VALUES (17, 'tempus sit amet sem fusce consequat nulla nisl nunc nisl duis bibendum', 'vitae
 nisl aenean lectus pellentesque eget nunc donec quis orci eget orci vehicula condimentum 
curabitur in libero ut massa volutpat convallis morbi odio odio elementum eu interdum eu 
tincidunt in leo maecenas pulvinar lobortis est phasellus sit amet', 'Environmental Tech',
timestamp '2013-01-15 22:11:35', date '2014-04-13');

INSERT INTO NEWS (NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE, MODIFICATION_DATE) 
VALUES (18, 'tempus vel pede morbi porttitor lorem id ligula suspendisse ornare', 
'magna vestibulum aliquet ultrices erat tortor sollicitudin mi sit amet lobortis sapien sapien
 non mi integer ac neque duis bibendum morbi non quam nec dui luctus rutrum nulla tellus in 
sagittis dui vel nisl duis ac nibh fusce lacus purus', 'Account Representative III',
timestamp '2013-01-15 22:11:35', date '2014-04-13');

INSERT INTO NEWS (NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE, MODIFICATION_DATE) 
VALUES (19, 'non mi integer ac neque duis bibendum morbi non quam nec dui', 'est quam pharetra
 magna ac consequat metus sapien ut nunc vestibulum ante ipsum primis in faucibus orci luctus 
et ultrices posuere cubilia curae mauris viverra diam vitae quam suspendisse potenti nullam 
porttitor lacus at turpis donec posuere metus vitae ipsum', 'Web Developer II',
timestamp '2013-01-15 22:11:35', date '2014-04-13');

INSERT INTO NEWS (NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE, MODIFICATION_DATE) 
VALUES (20, 'pede malesuada in imperdiet et commodo vulputate justo in blandit ultrices', 
'nisi eu orci mauris lacinia sapien quis libero nullam sit amet turpis elementum ligula vehicula
 consequat morbi a ipsum integer a nibh in quis justo maecenas rhoncus aliquam lacus morbi quis
 tortor id', 'Community Outreach Specialist',timestamp '2013-01-15 22:11:35', date '2014-04-13');
 
 
 
/*INSERT SCRIPT FOR TABLE KIRYL_SPR.NEWS_AUTHOR */
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (17, 16);
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (2, 8);
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (16, 1);
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (16, 9);
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (1, 13);
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (20, 13);
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (15, 4);
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (18, 20);
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (19, 14);
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (12, 19);
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (17, 1);
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (8, 4);
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (6, 16);
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (12, 19);
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (13, 11);
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (9, 18);
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (8, 5);
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (19, 10);
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (17, 14);
INSERT INTO NEWS_AUTHOR (NEWS_ID, AUTHOR_ID) VALUES (1, 19);


/*INSERT SCRIPT FOR TABLE MIKHAIL.COMMENTS */
INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT, CREATION_DATE, NEWS_ID) 
VALUES (1, 'amet erat nulla tempus vivamus', TIMESTAMP '2013-01-15 22:11:35', 8);

INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT, CREATION_DATE, NEWS_ID) 
VALUES (2, 'interdum mauris non',TIMESTAMP '2013-01-15 22:11:35', 10);

INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT,CREATION_DATE, NEWS_ID)
VALUES (3, 'congue etiam',TIMESTAMP '2013-01-15 22:11:35', 7);

INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT,CREATION_DATE, NEWS_ID) 
VALUES (4, 'malesuada in', TIMESTAMP '2013-01-15 22:11:35',13);

INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT,CREATION_DATE, NEWS_ID) 
VALUES (5, 'mus vivamus vestibulum sagittis sapien cum', TIMESTAMP '2013-01-15 22:11:35',15);

INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT, CREATION_DATE,NEWS_ID) 
VALUES (6, 'at ipsum ac', TIMESTAMP '2013-01-15 22:11:35',11);

INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT,CREATION_DATE, NEWS_ID) 
VALUES (7, 'pretium nisl ut volutpat', TIMESTAMP '2013-01-15 22:11:35',15);

INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT,CREATION_DATE, NEWS_ID)
VALUES (8, 'habitasse platea dictumst', TIMESTAMP '2013-01-15 22:11:35',3);

INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT, CREATION_DATE,NEWS_ID)
VALUES (9, 'arcu sed',TIMESTAMP '2013-01-15 22:11:35', 8);

INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT,CREATION_DATE, NEWS_ID) 
VALUES (10, 'curae mauris viverra', TIMESTAMP '2013-01-15 22:11:35',1);

INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT, CREATION_DATE,NEWS_ID)
VALUES (11, 'tortor id', TIMESTAMP '2013-01-15 22:11:35',18);

INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT,CREATION_DATE, NEWS_ID) 
VALUES (12, 'proin eu mi nulla ac', TIMESTAMP '2013-01-15 22:11:35',15);

INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT,CREATION_DATE, NEWS_ID)
VALUES (13, 'sagittis dui vel nisl', TIMESTAMP '2013-01-15 22:11:35',2);

INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT, CREATION_DATE,NEWS_ID)
VALUES (14, 'eget', TIMESTAMP '2013-01-15 22:11:35',16);

INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT, CREATION_DATE,NEWS_ID)
VALUES (15, 'ut mauris',TIMESTAMP '2013-01-15 22:11:35', 1);

INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT, CREATION_DATE,NEWS_ID)
VALUES (16, 'amet diam in magna', TIMESTAMP '2013-01-15 22:11:35',16);

INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT, CREATION_DATE,NEWS_ID)
VALUES (17, 'ac',TIMESTAMP '2013-01-15 22:11:35', 1);

INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT,CREATION_DATE, NEWS_ID)
VALUES (18, 'fermentum justo nec condimentum', TIMESTAMP '2013-01-15 22:11:35',9);

INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT,CREATION_DATE, NEWS_ID)
VALUES (19, 'magna', TIMESTAMP '2013-01-15 22:11:35',16);

INSERT INTO COMMENTS (COMMENT_ID, COMMENT_TEXT, CREATION_DATE,NEWS_ID)
VALUES (20, 'semper interdum mauris ullamcorper purus', TIMESTAMP '2013-01-15 22:11:35',6);



/*INSERT SCRIPT FOR TABLE MIKHAIL.NEWS_TAG */
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (20, 20);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (11, 3);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (1, 10);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (7, 9);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (7, 5);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (7, 20);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (5, 18);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (16, 4);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (19, 15);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (1, 1);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (12, 9);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (18, 6);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (7, 15);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (5, 6);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (3, 14);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (19, 3);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (9, 7);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (3, 4);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (4, 17);
INSERT INTO NEWS_TAG ( NEWS_ID, TAG_ID) VALUES (19, 16);